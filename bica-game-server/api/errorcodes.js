var fs = require('fs');

try {
  var stats = fs.statSync("../extendederrorcodes.json");
}
catch (e) {
     fs.writeFileSync('../extendederrorcodes.json', '{}'); // Please customize this new file!
}

var errors = module.exports = {
    
    errorcodes: {

        NoError: 0,
        GeneralError: 1,
        InvalidGame: 2,
        Timeout: 3,
        InvalidRequest: 4,
        DoNotTrack: 5

		// Add game specific messages below this line
	},

    descriptions: {
        // General Errors
        "0": "No error",
        "1": "General error, this typically means the player is unable to connect",
        "2": "Invalid game credentials. Make sure you use the keys you set up in your database",
        "3": "Request timed out",
        "4": "Invalid request",
		
		// Add game specific messages below this line
    }
};

var extendedCodes = JSON.parse(fs.readFileSync('../extendederrorcodes.json', 'utf8'));
for(var i in extendedCodes.errorcodes)
{
	errors.errorcodes[i] = extendedCodes.errorcodes;
}
for(var i in extendedCodes.descriptions)
{
	errors.descriptions[i] = extendedCodes.descriptions;
}