var mongodb = require("mongodb"),
    games = require(__dirname + "/games.js"),
    config = require(__dirname + "/config.js"),
    db = require(__dirname + "/database.js"),
    varlist = {};

var gamevars = module.exports = {

    // for tests
    ready: false,
    data: varlist,

    /**
     * Loads all the GameVars for the game
     * @param publickey
     */
    list: function(publickey) 
    {
        return varlist || {};
    },

    /**
     * Loads a single GameVar for a game
     * @param publickey
     */
    single: function(publickey) 
    {
        return varlist[publickey] || {};
    }
};

// Data cache
(function() {
    var lastupdated = 0;

  
    function refresh() {

        if(!games.ready) {
            return setTimeout(refresh, 1000);
        }

        db.bicaserver.gamevars.get({cache: true}, function(error, vars)
        {
	    
            if(error) {
                if(callback) {
                    callback(error);
                }

                console.log("GAMEVARS failed to retrieve results from mongodb: " + error);
                return setTimeout(refresh, 1000);
            }

		
            for(var i=0; i<vars.length; i++) {
				
        		var publickey = vars[i].publickey;
        		
        		if(!publickey) {
        			console.log("GAMEVARS warning you have gamevars configured that don't have a publickey");
        			continue;
        		}

                var gamevar = vars[i];

                if(!varlist[publickey]) {
                    varlist[publickey] = {};
                }

                if(vars[i].lastupdated > lastupdated) {
                    lastupdated = vars[i].lastupdated;
                }

                varlist[publickey][gamevar.name] = gamevar.value;
            }

            gamevars.ready = true;
            return setTimeout(refresh, 30000);
        });
    }

    refresh();
})();
