var config = require(__dirname + "/config.js"),
datetime = require(__dirname + "/datetime.js"),
configJson = JSON.parse(require('fs').readFileSync('../servervars.json', 'utf8'));

// setup
var mongodb = require("mongodb");


// Add more collections as needed
var collections = configJson.database.collections;

var thisDb = null;
const client = new mongodb.MongoClient(config.mongo.bicaserver.fullUrl);


async function timedConnect()
{
	await client.connect();
	thisDb = client.db(config.mongo.bicaserver.name);
	/*mongodb.MongoClient.connect(config.mongo.bicaserver.fullUrl, {}, function(error, db1)
	{
		if(error) {
			console.log("authentication failed: " + error);
			setTimeout(function(){timedConnect()}, 1000);
			return;
		}
		
		//var db = new mongodb.Db(config.mongo.bicaserver.name, new mongodb.Server(config.mongo.bicaserver.address, config.mongo.bicaserver.port, {slave_ok: true}));
		
	});*/
	return "connected";
}

function timedDbOpen()
{
	if(thisDb === null)
	{
		setTimeout(function(){timedDbOpen()}, 1000);
		return;
	}
		
	function setup() {
		
		var jobs = [],
			collectionadd = 0,
			collectionclean = 0;
		
		// creating collections
		/*collections.forEach(function(collection) {
			jobs.push(function() { 
				console.log("handling " + collection);
				thisDb.createCollection(collection, {}).catch((err) => next(err));
			});
		});*/
		
		// create indexes
		// Add more jobs as needed with more collections
		for(var i in configJson.indexes)
		{
			jobs.push(function() { 
				thisDb.collection(i).ensureIndex(configJson.indexes[i], next);
			});
		}
		
		function next(error) {
			if(error) {
				console.log(error);
			}
			
			if(jobs.length == 0) {
				console.log("Close db");
				db.ready = true;
				client.close();
			} else {
				jobs.shift()();
			}
		}
		
		next();
	}
	
	
	
	if(config.mongo.bicaserver.username && config.mongo.bicaserver.password) {
		// cnn.authenticate(config.mongo.bicaserver.username, config.mongo.bicaserver.password, function(error) {

			// if(error) {
				// console.log("authentication failed: " + error);
				// setTimeout(function(){timedConnect()}, 1000);
				// return;
			// }

			// setup();
		// });
		
		// mongodb.mongoclient.connect(config.mongo.bicaserver.fullurl, {}, function(error, db)
		// {
			// if(error)
			// {
				// console.log("authentication failed: " + error);
				// settimeout(function(){timedconnect()}, 1000);
				// return;
			// }
			// console.log(db);
			// setup();
		// });
		
		setup();
	} else {
		setup();
	}
}

timedConnect().then(console.log).catch(console.error).finally(() => {timedDbOpen();});
//timedDbOpen();

// configuration
var db = require(__dirname + "/mongo-wrapper.js");
db.poolEnabled = true;
db.poolSize = 20;
db.killEnabled = true;
db.setDatabases(config.mongo);
db.bicaserver.collections(collections);
db.ready = false;

module.exports = db;