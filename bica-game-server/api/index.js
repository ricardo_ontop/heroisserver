var fs = require('fs');

try {
  var stats = fs.statSync("../servervars.json");
}
catch (e) {
     fs.writeFileSync('../servervars.json', fs.readFileSync('initservervars.json')); // Please customize this new file!
}

var api = module.exports = {
    config: require(__dirname + "/config.js"),
    games: require(__dirname + "/games.js"),
    gamevars: require(__dirname + "/gamevars.js"),
    documents: require(__dirname + "/documents.js"),
    database: require(__dirname + "/database.js"),
    utils: require(__dirname + "/utils.js"),
    md5: require(__dirname + "/md5.js"),
	gameutils: require(__dirname + "/gameutils.js"),
    errorcodes: require(__dirname + "/errorcodes.js").errorcodes,
    errormessages: require(__dirname + "/errorcodes.js").descriptions,
    datetime: require(__dirname + "/datetime.js"),
	requestauth: require(__dirname + "/requestauth.js")
	
	// Keep adding more requires as the game's collections increases in number
};

var configJson = JSON.parse(require('fs').readFileSync('../servervars.json', 'utf8'));
for(var i in configJson.index.requires)
{
	api[i] = require(__dirname + "/" + configJson.index.requires[i]);
}