var configJson = JSON.parse(require('fs').readFileSync('../servervars.json', 'utf8'));

var db = require(__dirname + "/database.js"),
    gamelist = {};

var games = module.exports = {

    /**
     * Returns a game if it exists
     */
    load: function(publickey) {
        return gamelist[publickey] || null;
    },

    /**
     * Returns all the loaded data for testing
     */
    data: function(callback) {
        return gamelist;
    },

    ready: false
};

// Data cache
(function() {
    var lastupdated = 0;

    function refresh() {
        db.bicaserver.games.get({cache: false}, function(error, credentials)
        {
            if(error) {
                // if(callback) {
                    // callback(error);
                // }

                console.log("GAMES failed to retrieve results from mongodb: " + error);
                return setTimeout(refresh, 1000);
            }
			
			var keys = configJson.games.keys; // Add more as needed with more collections

            for(var i=0; i<credentials.length; i++) {
                var publickey = credentials[i].publickey;
                gamelist[publickey] = credentials[i];
				
				for(var j=0; j<keys.length; j++) {				
					if(!credentials[i].hasOwnProperty(keys[j])) {
						credentials[i][keys[j]] = true;
					}
				}
            }

            games.ready = true;
            return setTimeout(refresh, 30000);
        });
    }

    refresh();
})();