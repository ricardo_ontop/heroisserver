var mongodb = require("mongodb"),
    localcache = {},
    connections = {},
    allconnections = [],
    debug = process.env.debug || false;

/**
 * Database configurations, this way your api calls are more simple
 * like:  db.get("local", "mystuff", ....
 */
var databases = {
    test: {
        address: "127.0.0.1",
        port: 27017,
        name: "test"
    },
    local2: {
        address: "127.0.0.1",
        port: 27017,
        name: "local2"
    }
};

/**
 * Helper functions
 */
function trace(message) {

    if(!debug) {
        return;
    }

    console.log("mongowrapper: " + message);
}

function promiseCallback(cnn, error, callback, results) {
	killConnection(cnn, error);
	if(callback) 
	{
		callback(error, results);
	}

	if(error) 
	{
		trace("update error: " + error);
	}
}

/**
 * Terminates a connection.
 * @param connection The collection name
 * @param db The database client
 */
function killConnection(cnn, error) {

    cnn.inuse = false;

    // disposed of after we terminated it
    if(!cnn.connection || !cnn.db) {
        cnn = null;
        return;
    }

    if(!error && module.exports.poolEnabled && cnn.db && cnn.connection) {

        var database = databases[cnn.databasename];

        if(!database.pool) {
            cnn.expire = 30;
            database.pool = [cnn];
            return;
        }

        if(database.pool.length < module.exports.poolSize) {
            cnn.expire = 30;
            database.pool.push(cnn);
            return;
        }
    }

    cnn.connection.close();
    //cnn.db.close();
    cnn.connection = null;
    cnn.db = null;
    delete(cnn.connection);
    delete(cnn.db);
    cnn = null;
    return;
}

/**
 * Pool cleaning
 */
setInterval(function() {

    if(!module.exports.poolEnabled && !module.exports.killEnabled)
        return;

    for(var i=allconnections.length - 1; i>-1; i--) {

        var cnn = allconnections[i];
        cnn.expire--;

        if(cnn.expire > 0) {
            continue;
        }

        if(cnn.inuse) {
            cnn.inuse = false;
            cnn.expire = module.exports.expireConnection;
            continue;
        }

        // remove from global pool
        allconnections.splice(i, 1);

        // remove from database pool
        var db = databases[cnn.databasename];

        if(db.pool && db.pool.length) {
            var dbp = db.pool.indexOf(cnn);

            if(dbp > -1) {
                db.pool.splice(dbp, 1);
            }
        }

        // close it
        if(cnn.connection && cnn.connection.close) {
            cnn.connection.close();
            cnn.connection = null;
            delete(cnn.connection);
        }

        if(cnn.db && cnn.db.close) {
            cnn.db.close();
            cnn.db = null;
            delete(cnn.db);
        }

        cnn = null;
    }
}, 1000);

/**
 * Nice and simple persistant connections.  If your application runs on
 * a PaaS or multiple instances each worker/whatever will have its own
 * connection pool.
 *
 * @param databasename Database configuration name
 * @param collectionname The collection name
 * @param operation The api operation
 * @param callback The callback function
 */
async function getConnection(databasename, collectionname, operation, callback) {


    var database = databases[databasename];

    if(module.exports.poolEnabled && database.pool && database.pool.length) {

        var cnn = database.pool.pop();

        if(cnn && cnn.connection && cnn.connection.state == "connected") {
            cnn.expire = module.exports.expireConnection;
            cnn.inuse = true;
            callback(null, collectionname ? new mongodb.Collection(cnn.connection, cnn.connection.topology, cnn.connection.databaseName, cnn.collectionname, cnn.connection.pkFactory, cnn.connection.options) : null, cnn);
            return;
        } else {
           killConnection(cnn, false);
        }
    }

    var options = {
        slave_ok: true
    };

	var connection = new mongodb.MongoClient(database.fullUrl);
	await connection.connect();
	
	db2 = connection.db(database.name);

	var cnn = {connection: connection, db: db2, databasename: databasename, expire: module.exports.expireConnection, inuse: true};

	if(module.exports.killEnabled) {
		allconnections.push(cnn);
	}

	if(!database.username && !database.password) {

		callback(null, collectionname ? new mongodb.Collection(db2, collectionname, connection.options) : null, cnn);
		return;
	}
	
	callback(null, collectionname ? new mongodb.Collection(db2, collectionname, connection.options) : null, cnn);
}

function showError(error)
{
	if(error) {
		trace("connection failed to database: " + error);
		//getConnection(databasename, collectionname, operation, callback).catch(showError);
		//killConnection(cnn, error);
		return;
	}
}

module.exports = db = {

    /**
     * Configuration settings
     * poolEnabled stores connections to be reused
     * poolLimit the maximum number of connections to store
     * cacheEnabled stores data from get, getAndCount and queries
     * defaultCacheTime seconds to store cache data
     * killEnabled destroys connections after up to 2x expireConnection depending on in use or not
     * expireConnection how long to keep connections
     */
    poolEnabled: true,
    cacheEnabled: true,
    killEnabled: true,
    defaultCacheTime: 60,
    poolLimit: 20,
    expireConnection: 30,

    /**
     * Import your own database collection
     *
     * @param dblist Your databases:  { db1: { address: "", port: , name: "db1" }, ... }
     */
    setDatabases:function(dblist) {
        databases = dblist;
        configureDatabases();
    },

    /**
     * Inserts an object into a collection.
     *
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { doc: {}, safe: false }
     * @param callback Your callback method(error, item)
     */
    insert: function(database, collectionname, options, callback) {

        getConnection(database, collectionname, "insert", function(error, collection, cnn) {

            collection.insertOne(options.doc, {safe: options.safe}).then((res) =>
            {
                promiseCallback(cnn, null, callback, res);
            }).catch((error) => 
			{
				promiseCallback(cnn, error, callback, null);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },

    /**
     * Updates / Upserts an object into a collection.
     *
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { filter: {}, doc: {}, safe: false, upsert: true, multi: false }
     * @param callback Your callback method(error)
     */
    update: function(database, collectionname, options, callback) 
    {
        getConnection(database, collectionname, "update", function(error, collection, cnn) 
        {
            collection.updateMany(options.filter, options.doc, {safe: options.safe || false, upsert: options.upsert !== undefined ? options.upsert : true, multi: options.multi || false}).then((res) =>
            {
                promiseCallback(cnn, null, callback, res);
            }).catch((error) => 
			{
				promiseCallback(cnn, error, callback, null);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },

    /**
     * Saves an objects into a collection.
     *
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { docs: {}, safe: false}
     * @param callback Your callback method(error)
     */
    save: function(database, collectionname, options, callback) 
    {
        getConnection(database, collectionname, "save", function(error, collection, cnn) 
        {
            collection.save(options.doc, {safe: options.safe || false}).then((res) =>
            {
                promiseCallback(cnn, null, callback, res);
            }).catch((error) => 
			{
				promiseCallback(cnn, error, callback, null);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },

    /**
     * Updates / Remove / Upserts an object into a collection and selects one or more items
     *
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { filter: {}, sort: {}, new: false, fields: {}, remove: {}, update {}, upsert: false }
     * @param callback Your callback method(error, items)
     */
    updateOrRemoveAndGet: function(database, collectionname, options, callback) 
    {
        getConnection(database, collectionname, "findAndModify", function(error, collection, cnn) 
        {
            collection.findAndModify({ query: options.filter || {}, sort: options.sort || {}, remove: options.remove || false, update: options.update || {}, new: options.new || false, fields: options.fields || {}, upsert: options.upster || false }).then((res) =>
            {
                promiseCallback(cnn, null, callback, res);
            }).catch((error) => 
			{
				promiseCallback(cnn, error, callback, null);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },

    /**
     * Selects one or more items
     *
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { filter: {}, limit: 0, skip: 0, sort: {}, cache: false, cachetime: 60, updateCache: false }
     * @param callback Your callback method(error, items)
     */
    get: function(database, collectionname, options, callback) {

        if(options.cache && !options.updateCache) {
            var cached = cache.get(database, collectionname, "get", options);

            if(cached) {
                callback(null, cached);
                return;
            }
        }

        getConnection(database, collectionname, "get", function(error, collection, cnn) {
            collection.find(options.filter || {}, options.fields || {}).limit(options.limit || 0).skip(options.skip || 0).sort(options.sort || {}).toArray().then((res) =>
            {
                promiseCallback(cnn, null, callback, res);
            }).catch((error) => 
			{
				promiseCallback(cnn, error, callback, null);
			});

        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },

    /**
     * Selects a single item or inserts it
     *
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { filter: {}, doc: {}, safe: true or false }
     * @param callback Your callback method(error, item)
     */
    getOrInsert: function(database, collectionname, options, callback) {

        getConnection(database, collectionname, "getOrInsert", function(error, collection, cnn) {

            collection.find(options.filter).limit(1).toArray().then((items) => {

                // get it
                if(items.length > 0) {
					killConnection(cnn, error);
					if(options.multi)
					{
						callback(null, items);
					}
					else
					{
						callback(null, items[0]);
					}
                    return;
                }

                // insert it
                collection.insert(options.doc, {safe: options.safe || false}).then((item) => {
					
					killConnection(cnn, error);
					if(callback) {
						if(options.multi)
						{
							 callback(null, item);
						}
						else
						{
							callback(null, item[0]);
						}
                    }
					
				}).catch((error) => {
					promiseCallback(cnn, error, callback, null);
				});
            }).catch((error) => 
			{
				promiseCallback(cnn, null, callback, res);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },

    /**
     * Selects a subset of items and returns the total number
     *
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { filter: {}, limit: 0, skip: 0, sort: {}, cache: false, cachetime: 60 }
     * @param callback Your callback method(error, items, numitems)
     */
    getAndCount: function(database, collectionname, options, callback) {

        if(options.cache) {
            var cached = cache.get(database, collectionname, "getAndCount", options);

            if(cached) {
                callback(null, cached.items, cached.numitems);
                return;
            }
        }

        getConnection(database, collectionname, "getAndCount", function(error, collection, cnn) {

            collection.find(options.filter || {}).limit(options.limit || 0).skip(options.skip || 0).sort(options.sort || {}).toArray().then((items) => {

                // note we could use the api here but it would potentially
                // establish a second connection and change the cache key
                collection.count(options.filter).then((numitems) => {

                    killConnection(cnn, error);

                    if(options.cache) {
                        cache.set(database, collectionname, "getAndCount", options, {items: items, numitems: numitems});
                    }

                    if(callback) {
                        callback(null, items, numitems);
                    }
                }).catch((error) => {
					promiseCallback(cnn, error, callback, null);
				});
            }).catch((error) => {
				promiseCallback(cnn, error, callback, null);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },
	
    /**
     * Aggregates a collection
     *
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { aggregate: [pipeline], cache: false, cachetime: 60 }
     * @param callback Your callback method(error, items, numitems)
     */
    aggregate: function(database, collectionname, options, callback) {

        if(options.cache) {
            var cached = cache.get(database, collectionname, "aggregate", options);

            if(cached) {
                callback(null, cached.items);
                return;
            }
        }

        getConnection(database, collectionname, "aggregate", function(error, collection, cnn) {
            collection.aggregate(options.aggregate).toArray().then((res) =>
            {
                promiseCallback(cnn, null, callback, res);
            }).catch((error) => 
			{
				promiseCallback(cnn, error, callback, null);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },
	
    /**
     * Aggregates and counts the total number of aggregated reuslts
     *
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { aggregate: [pipeline], count: [pipeline], cache: false, cachetime: 60 }
     * @param callback Your callback method(error, items, numitems)
     */
    aggregateAndCount: function(database, collectionname, options, callback) {

        if(options.cache) {
            var cached = cache.get(database, collectionname, "aggregateAndCount", options);

            if(cached) {
                callback(null, cached.items, cached.numitems);
                return;
            }
        }

        getConnection(database, collectionname, "aggregateAndCount", function(error, collection, cnn) {

            collection.aggregate(options.aggregate, function (error, items) {

                collection.aggregate(options.count).then((numitems) => {

                    killConnection(cnn, error);
					
					numitems = numitems[0].count;

                    if(options.cache) {
                        cache.set(database, collectionname, "aggregateAndCount", options, {items: items, numitems: numitems});
                    }
					
                    if(callback) {
                        callback(null, items, numitems);
                    }
                }).catch((error) => {
					promiseCallback(cnn, error, callback, null);
				});
            }).catch((error) => {
				promiseCallback(cnn, error, callback, null);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },

    /**
     * Counts the number of items matching a query
     *
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { filter: {}, cache: false, cachetime: 60 }
     * @param callback Your callback method(error, numitems)
     */
    count: function(database, collectionname, options, callback) {

        if(options.cache) {
            var cached = cache.get(database, collectionname, "count", options);

            if(cached) {
                callback(null, cached);
                return;
            }
        }

        getConnection(database, collectionname, "count", function(error, collection, cnn) {

            collection.count(options.filter).then((numitems) => {

                killConnection(cnn, error);

                if(options.cache) {
                    cache.set(database, collectionname, "count", numitems);
                }

                if(callback) {
                    callback(null, numitems);
                }
            }).catch((error) => {
				promiseCallback(cnn, error, callback, null);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },

    /**
     * Moves a document from one collection to another
     * @param database Database config name
     * @param collection1name The source collection name
     * @param collection2name The destination collection name
     * @param options { doc: {... }, overwrite: true, safe: false, }
     * @param callback Your callback method(error, success)
     */
    move: function(database, collection1name, collection2name, options, callback) {

        getConnection(database, collection1name, "move", function(error, collection1, cnn1) {

            if(error) {

                if(callback) {
                    callback(error);
                }

                trace("move error: " + error);
                killConnection(cnn1, error);
                return;
            }

            getConnection(database, collection2name, "move", function(error, collection2, cnn2) {

                collection2.updateMany(options.doc, options.doc, {safe: options.safe || false, upsert: options.upsert || options.overwrite}).then((res) => {

                    collection1.deleteMany(options.doc).then((res2) => {

                        killConnection(cnn1, error);
                        killConnection(cnn2);

                        if(callback) {
                            callback(null);
                        }
                    }).catch((error) => {
						promiseCallback(cnn, error, callback, null);
					});
                }).catch((error) => {
					promiseCallback(cnn, error, callback, null);
				});
            }).catch((error) => {
				promiseCallback(cnn, error, callback, null);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    },

    /**
     * Removes one or more documents from a collection
     * @param database Database config name
     * @param collectionname The collection name
     * @param options { filter: {} }
     * @param callback Your callback method(error, success)
     */
    remove: function(database, collectionname, options, callback) {

        getConnection(database, collectionname, "remove", function(error, collection, cnn) {
            collection.deleteMany(options.filter).then((res) =>
            {
                promiseCallback(cnn, null, callback, res);
            }).catch((error) => 
			{
				promiseCallback(cnn, error, callback, null);
			});
        }).catch((error) => {
			callback(error);
			showError(error);
		});
    }
};


/**
 * A very simple, self cleaning, local cache.  If your app runs on multiple threads
 * or a PaaS like Heroku each dyno / worker / whatever will have its own copy
 */
var cache = {

    get: function(databasename, collectionname, operation, options) {

        if(!db.cacheEnabled) {
            return null;
        }

        var database = databases[databasename];
        var key = database.name + ":" + database.collectionname + ":" + operation + ":" + JSON.stringify(options);
        return localcache[key] ? JSON.parse(localcache[key].data) : null;
    },

    set: function(databasename, collectionname, operation, options, obj) {

        if(!db.cacheEnabled) {
            return;
        }

        var database = databases[databasename];
        var key = database.name + ":" + database.collectionname + ":" + operation + ":" + JSON.stringify(options);
        localcache[key] = { data: JSON.stringify(obj), time: options.cachetime || db.defaultCacheTime};
    }
}

setInterval(function() {

    for(var key in localcache) {

        localcache[key].time--;

        if(localcache[key].time > 0) {
            continue;
        }

        localcache[key] = null;
        delete localcache[key];
    }

}, 1000);

/*
 * Creates the shorthand references to databases and provides methods
 * for including shorthand collection paths too.  You don't need to call
 * this manually, it will automatically apply to the locally defined
 * list of databases, or run again if you pass your own configuration.
 */
function configureDatabases() {
    for(var databasename in databases) {
		configureDatabase(databasename);
	}
}

configureDatabases();

function configureDatabase(databasename) {
	
	var alias = databasename;
	
	if(databases[databasename].alias) {
		alias = databases[databasename].alias;
	}
    
	db[alias] = databases[databasename];
	db[alias].databasename = databasename;

    /**
     * Initializes a collection's shorthand methods for accessing
	 * via db.databasename.collectionname.method(..)
     * @param collectionname the collection name
     */
    db[alias].collection = function(collectionname) {

        var databasename = this.databasename;
		
        db[databasename][collectionname] = {
			get: function(options, callback) { 
				db.get(databasename, collectionname, options, callback); 
			},
			getOrInsert: function(options, callback) { 
				db.getOrInsert(databasename, collectionname, options, callback); 
			},
			getAndCount: function(options, callback) { 
				db.getAndCount(databasename, collectionname, options, callback); 
			},
			count: function(options, callback) { 
				db.count(databasename, collectionname, options, callback); 
			},
			move: function(collection2name, options, callback) { 
				db.move(databasename, collectionname, collection2name, options, callback); 
			},
			save: function(options, callback) { 
				db.save(databasename, collectionname, options, callback); 
			},
			update: function(options, callback) { 
				db.update(databasename, collectionname, options, callback); 
			},
			updateOrRemoveAndGet: function(options, callback) { 
				db.updateOrRemoveAndGet(databasename, collectionname, options, callback); 
			},
			insert: function(options, callback) { 
				db.insert(databasename, collectionname, options, callback); 
			},
			remove: function(options, callback) { 
				db.remove(databasename, collectionname, options, callback); 
			},
			aggregate: function(options, callback) { 
				db.aggregate(databasename, collectionname, options, callback); 
			},
			aggregateAndCount: function(options, callback) { 
				db.aggregateAndCount(databasename, collectionname, options, callback); 
			}
		}
    };

    /**
     * Initializes the collection shorthand on a database
     * @param opt either an array of collection names or a callback method(error) for
     * loading directly from the db
     */
    db[alias].collections = function(opt) {

        var callback;
		var databasename = this.databasename;

		// received an array of collections
        if(opt) {

            if(typeof opt === 'function') {
                callback = opt;
            } else {
                for(var i=0; i<opt.length; i++) {
                    db[databasename].collection(opt[i]);
                }

                return;
            }
        }

		// look up the collections
        getConnection(databasename, "", "", function(error, collection, connection) {

            if(error) {
                callback(error);
                return;
            }

            connection.collectionNames({namesOnly: true}, function(error, names) {

                if(error) {
                    callback(error);
                    return;
                }

                for(var i=0; i<names.length; i++) {

                    var name = names[i];

                    if(name.indexOf(databasename + ".system.") == 0)
                        continue;

                    var collectionname = name.substring(databasename.length + 1);
                    db[databasename].collection(collectionname);
                }

                connection.close();
                connection = null;
                callback(null);
            });
        });
    }
}