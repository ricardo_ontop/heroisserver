var configJson = JSON.parse(require('fs').readFileSync('../servervars.json', 'utf8'));

var config = module.exports = {
    mongo: {
        bicaserver: parseCredentials(process.env.bicaserver || (process.env.NODE_ENV === "development" ? configJson.config.bicaserver_dev : configJson.config.bicaserver)) 
    }
};

function parseCredentials(connstring) 
{
    var username = connstring.split("://")[1];
    username = username.substring(0, username.indexOf(":"));
    var password = connstring.split("://")[1];
    password = password.substring(password.indexOf(":") + 1);
    password = password.substring(0, password.indexOf("@"));
    var host = connstring.split("@")[1];
    host = host.substring(0, host.indexOf(":"));
    var port = connstring.split("@")[1];
    port = port.substring(port.indexOf(":") + 1);

    if(port.indexOf("/") > -1) {
        port = port.substring(0, port.indexOf("/"));
    }

    port = parseInt(port);

    // optional database
    var database = connstring.split("/");
    database = database[database.length - 1];
	database = database.split("?")[0];

	// console.log("Username: " + username);
	// console.log("Password: " + password);
	// console.log("Host: " + host);
	// console.log("Port: " + port);
	// console.log("Database: " + database);

    return {
        alias: "bicaserver",
        username: username,
        password: password,
        address: host,
        port: 27017,
        name: database,
		fullUrl: connstring
    }
}