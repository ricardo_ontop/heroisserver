var config = require(__dirname + "/config.js"),
    db = require(__dirname + "/database.js"),
    mongodb = require("mongodb"),
    md5 = require(__dirname + "/md5.js"),
    utils = require(__dirname + "/utils.js"),
    date = utils.fromTimestamp,
    datetime = require(__dirname + "/datetime.js"),
    errorcodes = require(__dirname + "/errorcodes.js").errorcodes;

var documents = module.exports = {

    delete: function(options, callback) {

        if(!options.collectionname) {
            callback("missing collectionname (api.documents.delete:17)", errorcodes.NoCollection);
            return ;
        }
        
        if(!db.bicaserver[options.collectionname]) {
            callback("wrong collectionname '" + options.collectionname + "' (api.documents.delete:22)", errorcodes.WrongCollection);
            return ;
        }
        
        db.bicaserver[options.collectionname].remove(options, function(error, success) {
            
            if (error) {
            	callback("error: '" + error + "' while removing document (api.documents.delete:29)", errorcodes.ErrorRemoving);
            	return;
            }

            if(!success) {
                callback("error removing document (api.documents.delete:34)", errorcodes.ErrorRemoving);
                return;
            } 

            callback(null, errorcodes.NoError);
        });
    }
};