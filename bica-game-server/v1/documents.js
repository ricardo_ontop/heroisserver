var api = require(__dirname + "/../api"),
    output = require(__dirname + "/output.js"),
    errorcodes= api.errorcodes,
	testing = process.env.testing || false;

module.exports = {
	sectionCode: 700,
	
    delete: function(payload, request, response, testcallback) {

	    api.documents.delete(payload, function(error, errorcode){

	        if(error) {
	            if(testcallback) {
	                testcallback(error);
	            }

	            return output.terminate(payload, response, errorcode, error);
	        }

        	var r = output.end(payload, response, {}, errorcode);

	        if(testing && testcallback) {
	            testcallback(null, r);
	        }
	    });
	}
};