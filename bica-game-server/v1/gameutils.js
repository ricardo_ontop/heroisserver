var output = require(__dirname + "/output.js"),
    api = require(__dirname + "/../api"),
	datetime = require(__dirname + "/../api/datetime.js"),
    errorcodes = api.errorcodes,
	testing = process.env.testing || false;

module.exports = 
{
	sectionCode: 300,

    time: function(payload, request, response, testcallback) 
    {
        var r = output.end(payload, response, datetime.now, errorcodes.NoError);

        if(testcallback) 
        {
            testcallback(null, r);
        }
    }
	
	// Complete this class with more methods as necessary, depending on the game
};