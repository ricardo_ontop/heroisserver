var output = require(__dirname + "/output.js"),
    api = require(__dirname + "/../api"),
    errorcodes = api.errorcodes,
	testing = process.env.testing || false;

module.exports = 
{
	sectionCode: 300,

    list: function(payload, request, response, testcallback) 
    {
        var gv = api.gamevars.list();
        var r = output.end(payload, response, gv, errorcodes.NoError);

        if(testcallback) 
        {
            testcallback(null, r);
        }
    },

    single: function(payload, request, response, testcallback) 
    {
        var gv = api.gamevars.single(payload.publickey);

        var single = {};
        single[payload.name] = gv[payload.name];

        var r = output.end(payload, response, single, errorcodes.NoError);

        if(testing && testcallback) 
        {
            testcallback(null, r);
        }
    }
};