var forever = require('forever-monitor');
var nodemailer = require('nodemailer');

// var child = new (forever.Monitor)('web.js', {
var child = new (forever.Monitor)('web.js', {
	max:0,
	silent: false,
	watch: true,
	watchDirectory: (process.env.local ? 'C:/Users/Asus/Desktop/ServerRepos/VodafoneRockInRio/' : '/home/ec2-user/VodafoneRockout/vodafonerockinrio/'),
	args: [],
	killTree: true
});

var transporter = nodemailer.createTransport("Direct", {debug:true});
	
// Message object
var message = {

    // sender info
    from: 'Paredes de Coura Server <paredes@coura.com>',

    // Comma separated list of recipients
    to: '"Vítor Oliveira" <vitor.alvdj@gmail.com>',

    // Subject of the message
    subject: 'Server error/crash'
};

var jsonEmailMessage = {

    // sender info
    from: 'Paredes de Coura Server <paredes@coura.com>',

    // Comma separated list of recipients
    to: '"Vítor Oliveira" <vitor.alvdj@gmail.com>',

    // Subject of the message
    subject: 'Changed JSON file'
};

var mailsCached = 0;
var exitEmailSent = false;

child.on('watch:restart', function(info) {
	console.log(info, "");
	
	var msg = 'Restaring script because ' + info.stat + ' changed';
	message.text = msg;
	jsonEmailMessage.text = info.stat;
	
	console.error(msg);
	
	if(process.env.local) return;
	
	transporter.sendMail(info.stat.includes(".json") ? jsonEmailMessage : message, function(error, response){
		if(error){
			console.log('Error occured from child exit');
			console.log(error.message);
			return;
		}else{
			console.log(response);
			console.log('Message sent successfully from child exit!');
		}
	});
});

child.on('restart', function() {
	console.error('Forever restarting script for ' + child.times + ' time');
});

child.on('exit:code', function(code) {
	var errorMessage = 'Forever detected script exited with code ' + code;
	console.error(errorMessage);
	message.text = errorMessage;
	
	if(process.env.local) return;
	
	if(mailsCached === 0)
	{	
		console.log("No mails cached??");
		transporter.sendMail(message, function(error, response){
			if(error){
				console.log('Error occured from child exit');
				console.log(error.message);
				return;
			}else{
				console.log(response);
				console.log('Message sent successfully from child exit!');
			}
		});
	}
});


function exitHandler(options, err) {
	var errorMessage = 'Forever forcefully exited';
	if(err)
	{
		errorMessage += "\n" + err.stack;
		console.log(err.stack);
	}
	
	if(process.env.local)
	{
		child.stop();
		process.exit();
		return;
	}
	
	console.log(errorMessage);
		
	if(mailsCached === 0 && !exitEmailSent)
	{
		message.text = errorMessage;
		
		mailsCached++;
		
		console.log("Mails cached: " + mailsCached);
		
		exitEmailSent = true;
		
		transporter.sendMail(message, function(error, response){
			if(error){
				console.log('Error occured from force exit');
				//console.log(error.message);
				
				setTimeout(function() {
					mailsCached--;
					console.log("Responde mails cached: " + mailsCached);
					if(mailsCached === 0)
					{
						child.stop();
						process.exit();
					}
				}, 1000);
				return;
			}else{
				//console.log(response);
				console.log('Message sent successfully from force exit!');
				
				setTimeout(function() {
					mailsCached--;
					console.log("Responde mails cached: " + mailsCached);
					if(mailsCached === 0)
					{
						child.stop();
						process.exit();
					}
				}, 1000);
			}
		});
	}
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));


child.start();