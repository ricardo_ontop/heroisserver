var basefolder = __dirname + "/../../../bica-game-server/api/",
    config = require(basefolder + "/config.js"),
    db = require(basefolder + "/database.js"),
    mongodb = require(basefolder + "../node_modules/mongodb"),
    md5 = require(basefolder + "/md5.js"),
    utils = require(basefolder + "/utils.js"),
    date = utils.fromTimestamp,
    datetime = require(basefolder + "/datetime.js"),
    fs = require('fs'),
    api = require(basefolder + "/../api"),
    errorcodes = require(__dirname + "/errorcodes.js").errorcodes,
    readline = require('readline');


var redeems = module.exports = {
    add_player: function(options, callback) {

        var query = {
            safe: true
        };

        db.bicaserver.redeems.aggregate(query, function(error, results) {

            if (error) {
                callback("error aggregating players from database (api.player.add_player:28)", errorcodes.GeneralError);
                return;
            }

            if (results.length === 0) {
                options.id = 0;
            } else {
                options.id = results[0].id + 1;
            }
            options.timestamp = datetime.now;

            var newPlayer = {};
            newPlayer.id = options.id;
            newPlayer.name = options.name;
            newPlayer.score = options.score;
            newPlayer.gender = options.gender;
            newPlayer.birthdayDay = options.birthdayDay;
            newPlayer.birthdayMonth = options.birthdayMonth;
            newPlayer.birthdayYear = options.birthdayYear;
            newPlayer.districtName = options.districtName;
            newPlayer.regionName = options.regionName;
            newPlayer.schoolName = options.schoolName;
            newPlayer.classIsParticipating = options.classIsParticipating;
            newPlayer.lastYearParticipated = options.lastYearParticipated;

            if (options.cards === undefined || options.cards === null)
                newPlayer.cards = [];
            else
                newPlayer.cards = options.cards;

            if (options.cards === undefined || options.cards === null)
                newPlayer.markers = [];
            else
                newPlayer.markers = options.markers;


            var query2 = {
                doc: newPlayer,
                safe: true
            };

            db.bicaserver.players.insert(query2, function(error2, items) {

                if (error2) {
                    callback("error inserting player into database (api.player.add_player:43)", errorcodes.GeneralError);
                    return;
                }

                callback(null, errorcodes.NoError, newPlayer);
            });
        });
    },

}