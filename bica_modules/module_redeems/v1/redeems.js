var basefolder = __dirname + "/../../../bica-game-server/v1/",
    output = require(basefolder + "/output.js"),
    api = require(basefolder + "/../api"),
    datetime = require(basefolder + "/../api/datetime.js"),
    errorcodes = api.errorcodes,
    testing = process.env.testing || false,
    fs = require("fs"),
    path = require("path");

module.exports = {
    sectionCode: 300,

    add_redeem: function(payload, request, response, testcallback) {
        api.redeems.add_redeem(payload, function(error, errorcode, data) {
            if (error) {
                if (testcallback) {
                    testcallback(error);
                }

                return output.terminate(payload, response, errorcode, error);
            }

            var r = output.end(payload, response, { "players": data }, errorcode);

            if (testing && testcallback) {
                testcallback(null, r);
            }
        });
    },
};