var basefolder = __dirname + "/../../../bica-game-server/v1/",
    output = require(basefolder + "/output.js"),
    api = require(basefolder + "/../api"),
    datetime = require(basefolder + "/../api/datetime.js"),
    errorcodes = api.errorcodes,
    testing = process.env.testing || false,
    fs = require("fs"),
    path = require("path");

module.exports = {
    sectionCode: 300,

    get_prize_missions: function(payload, request, response, testcallback) {
        api.prizeCodes.get_prize_missions(payload, function(error, errorcode, data) {
            if (error) {
                if (testcallback) {
                    testcallback(error);
                }

                return output.terminate(payload, response, errorcode, error);
            }

            var r = output.end(payload, response, { "players": data }, errorcode);

            if (testing && testcallback) {
                testcallback(null, r);
            }
        });
    },

    check_prize_code: function(payload, request, response, testcallback) {
        api.prizeCodes.check_prize_code(payload, function(error, errorcode, data) {
            if (error) {
                if (testcallback) {
                    testcallback(error);
                }

                return output.terminate(payload, response, errorcode, error);
            }

            var r = output.end(payload, response, { "players": data }, errorcode);

            if (testing && testcallback) {
                testcallback(null, r);
            }
        });
    }
};