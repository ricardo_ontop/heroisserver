var basefolder = __dirname + "/../../../bica-game-server/api/",
    config = require(basefolder + "/config.js"),
    db = require(basefolder + "/database.js"),
    mongodb = require(basefolder + "../node_modules/mongodb"),
    md5 = require(basefolder + "/md5.js"),
    utils = require(basefolder + "/utils.js"),
    date = utils.fromTimestamp,
    datetime = require(basefolder + "/datetime.js"),
    fs = require('fs'),
    api = require(basefolder + "/../api"),
    errorcodes = require(__dirname + "/errorcodes.js").errorcodes,
    readline = require('readline');

var prizeCodes = module.exports = {
    get_prize_missions: function(options, callback) {

        var query = {
            filter:
            {
                $or:[
                     {active: {$exists: false}},
                     {active: {$eq:true}}
                ]
            },
            safe: true
        };

        db.bicaserver.prizeCodes.get(query, function(error, results) {

            if (error) {
                callback("error getting prize codes from database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                return;
            }

            var filteredResults = [];
            for (var i = 0; i < results.length; i++) {
                var individualResult = results[i];
                var newResult = {};
                newResult["missionId"] = individualResult["missionId"];
                newResult["prizeByEmail"] = individualResult["byEmail"] !== undefined;
                newResult["hasQuantity"] = individualResult["totalQuantity"] !== undefined;

                var totalQuantity = individualResult["totalQuantity"];

                var currentQuantity
                if (individualResult["currentQuantity"] === undefined)
                    currentQuantity = 0;
                else
                    currentQuantity = individualResult["currentQuantity"];

				newResult["email"] = individualResult["byEmail"];
                newResult["currentQuantity"] = totalQuantity - currentQuantity;
                newResult["totalQuantity"] = totalQuantity;

                filteredResults.push(newResult);
            }

            callback(null, errorcodes.NoError, filteredResults);
        });
    },

    check_prize_code: function(options, callback) {

        var query = {
            filter: {
                "missionId": options.missionId
            },
            safe: true
        };

        db.bicaserver.prizeCodes.get(query, function(error, results) {

            if (error) {
                callback("error getting prize codes from database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                return;
            }

            if (results.length === 0) {
                callback("missionId not found (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                return;
            }

            var targetCode = results[0];
            if(options.email === undefined || options.email === null || options.email === false)
            {
                var foundCode = false;
                for (var i = 0; i < targetCode.codes.length; i++) {
                    var code = targetCode.codes[i];
                    if (code == options.code) {
                        foundCode = true;
                        break;
                    }
                }
    
                if (!foundCode) {
                    callback("code incorrect (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                    return;
                }
                
                if(targetCode.totalQuantity !== undefined && targetCode.currentQuantity !== undefined && (targetCode.totalQuantity <= targetCode.currentQuantity || targetCode.totalQuantity === 0))
                {
                    callback("maximum number of code redeems reached (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                    return;
                }
            }

            // check redeems
            var query2 = {
                filter: {
                    "missionId": options.missionId
                },
                safe: true
            };

            db.bicaserver.redeems.get(query2, function(error2, results2) {

                if (error2) {
                    console.log(error2);
                    callback("error getting redeems data from database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                    return;
                }

                if (results2.length === 0) {
                    var newRedeemData = {};
                    newRedeemData["missionId"] = options.missionId;


                    var redeemInnerData = [];
                    var currentRedeemData = {};

                    currentRedeemData["playerId"] = options.playerId;

                    if(options.email != undefined && options.email != null && options.email == true)
                        currentRedeemData["email"] = options.code;

                    currentRedeemData["geoLocation"] = options.geoLocation;
                    currentRedeemData["leaderboardMonth"] = options.leaderboardMonth;
                    currentRedeemData["leaderboardYear"] = options.leaderboardYear;
                    currentRedeemData["promoterName"] = options.promoterName;
                    currentRedeemData["timestamp"] = datetime.now;

                    redeemInnerData.push(currentRedeemData);

                    newRedeemData["redeemData"] = redeemInnerData;

                    var insertQuery = {
                        doc: newRedeemData,
                        safe: true
                    };

                    db.bicaserver.redeems.insert(insertQuery, function(error3, items) {
                        if (error3) {
                            callback("error inserting redeems data on database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                            return;
                        }

                        if (targetCode.currentQuantity === undefined)
                            targetCode.currentQuantity = 1;
                        else
                            targetCode.currentQuantity += 1;

                        var doc = {
                            "$set": {
                                currentQuantity: targetCode.currentQuantity
                            }
                        };
                        
                        var addQuery = {
                            filter: {
                                "missionId": options.missionId
                            },
                            doc: doc,
                            safe: true
                        };

                        db.bicaserver.prizeCodes.update(addQuery, function(error4, items) {
                            if (error4) {
                                console.log(error4);
                                callback("error updating redeems data on database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                                return;
                            }

                            callback(null, errorcodes.NoError, true);
                        });
                    });
                } else {
                    var codeRedeems = results2[0];
                    for (var i = 0; i < codeRedeems.redeemData.length; i++) {
                        var currentUser = codeRedeems.redeemData[i];
                        if (currentUser.playerId === options.playerId) {
                            callback("error player already redeemed (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                            return;
                        }
                    }

                    var currentRedeemData = {};

                    currentRedeemData["playerId"] = options.playerId;

                    if(options.email != undefined && options.email != null && options.email == true)
                        currentRedeemData["email"] = options.code;

                    currentRedeemData["geoLocation"] = options.geoLocation;
                    currentRedeemData["leaderboardMonth"] = options.leaderboardMonth;
                    currentRedeemData["leaderboardYear"] = options.leaderboardYear;
                    currentRedeemData["promoterName"] = options.promoterName;
                    currentRedeemData["timestamp"] = datetime.now;

                    if(options.email != undefined && options.email != null && options.email == true)
                        currentRedeemData["email"] = options.code;
                    
                    codeRedeems["redeemData"].push(currentRedeemData);

                    var doc = {
                        "$set":{
                            "redeemData": codeRedeems["redeemData"]
                        }
                    };
                    
                    var updateQuery = {
                        filter: {
                            "missionId": options.missionId
                        },
                        doc: doc,
                        safe: true
                    };

                    db.bicaserver.redeems.update(updateQuery, function(error4, items) {
                        if (error4) {
                            console.log(error4);
                            callback("error updating redeems data on database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                            return;
                        }

                        if (targetCode.currentQuantity === undefined)
                            targetCode.currentQuantity = 1;
                        else
                            targetCode.currentQuantity += 1;

                        var doc = {
                            "$set":{
                                "currentQuantity":  targetCode.currentQuantity
                            }
                        };
                            
                        var addQuery = {
                            filter: {
                                "missionId": options.missionId
                            },
                            doc: doc,
                            safe: true
                        };

                        db.bicaserver.prizeCodes.update(addQuery, function(error4, items) {
                            if (error4) {
                                console.log(error4);
                                callback("error updating redeems data on database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                                return;
                            }

                            callback(null, errorcodes.NoError, true);
                        });
                    });
                }
            });
        });
    }
}