var errors = module.exports = {
    
    errorcodes: {

        NoError: 0,
        GeneralError: 1,
        NoUsername: 2,
		NoCoords: 3,
		NoInstrument: 4,
		TargetDoesntExist: 5,
		GpsThresholdExceeded: 6,
		InvalidTime: 7,
		ScoreAlreadyExists: 8,
		LeaderboardUnchanged: 9,
		ScoreDoesntExist: 10,
		ScoreAlreadyShared: 11,
		EventStillOn: 12
	},

    descriptions: {
        // General Errors
        "0": "No error",
        "1": "General error, this typically means the player is unable to connect",
        "2": "Username not defined",
        "3": "GPS coords not defined",
        "4": "No instrument defined",
		"5": "Target ID doesn't exist",
		"6": "Exceeded GPS threshold distance",
		"7": "Invalid time to scan this object",
		"8": "Score already exists on the database",
		"9": "Leaderboard hasn't changed",
		"10": "Score doesn't exist on the database",
		"11": "Score was already shared by the user",
		"12": "Event is still running"
    }
};