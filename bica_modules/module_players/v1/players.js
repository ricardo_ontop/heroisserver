var basefolder = __dirname + "/../../../bica-game-server/v1/",
    output = require(basefolder + "/output.js"),
    api = require(basefolder + "/../api"),
    datetime = require(basefolder + "/../api/datetime.js"),
    errorcodes = api.errorcodes,
    testing = process.env.testing || false,
    fs = require("fs"),
    path = require("path");

module.exports = {
    sectionCode: 300,

    get_version: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            testcallback(0);
        }
        else
        {
            api.players2.get_version(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        
    },

    add_player: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.add_player(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.add_player(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        
    },

    get_player: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.get_player(payload, function(error2, errorcode2, player) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": player }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.get_player(payload, function(error2, errorcode2, player) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": player }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },

    get_all_players: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.get_all_players(payload, function(error2, errorcode2, players) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": players }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.get_all_players(payload, function(error2, errorcode2, players) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": players }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },
	
	get_random_nickname: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.get_random_nickname(payload, function(error2, errorcode2, players) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": players }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.get_random_nickname(payload, function(error2, errorcode2, players) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": players }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },

    update_player: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.update_player(payload, function(error2, errorcode2, player) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": player}, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.update_player(payload, function(error2, errorcode2, player) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": player}, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },

    validate_marker: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.validate_marker(payload, function(error2, errorcode2, pack) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": pack }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.validate_marker(payload, function(error2, errorcode2, pack) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": pack }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },

    add_marker: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.add_marker(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.add_marker(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },


	add_score: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.add_score(payload, function(error2, errorcode2, data) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.add_score(payload, function(error2, errorcode2, data) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
	},

    add_score_mission: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.add_score_mission(payload, function(error2, errorcode2, mission) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": mission }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.add_score_mission(payload, function(error2, errorcode2, mission) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": mission }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },

    add_card: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.add_card(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.add_card(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },

    get_leaderboard: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.get_leaderboard(payload, function(error2, errorcode2, players) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": players }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.get_leaderboard(payload, function(error2, errorcode2, players) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": players }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },

    get_leaderboard_district: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.get_leaderboard_district(payload, function(error2, errorcode2, players) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": players }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.get_leaderboard_district(payload, function(error2, errorcode2, players) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": players }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },

    get_leaderboard_region: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.get_leaderboard_region(payload, function(error2, errorcode2, players) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": players }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.get_leaderboard_region(payload, function(error2, errorcode2, players) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": players }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },

    get_markers: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.get_markers(payload, function(error2, errorcode2, markers) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": markers }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.get_markers(payload, function(error2, errorcode2, markers) {
                if (error2) {
                    if (testcallback) {
                        testcallback(error2);
                    }
    
                    return output.terminate(payload, response, errorcode2, error2);
                }
    
                var r = output.end(payload, response, { "players": markers }, errorcode2);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
    },
	
	check_leaderboard_prizes: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.check_leaderboard_prizes(payload, function(error, errorcode, prize_validations) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": prize_validations }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.check_leaderboard_prizes(payload, function(error, errorcode, prize_validations) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": prize_validations }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
	},
	
	redeem_leaderboard_prize: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.redeem_leaderboard_prize(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.redeem_leaderboard_prize(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
	},
	
	get_daily_chest: function(payload, request, response, testcallback) {
        if(payload.version === undefined)
        {
            console.log("RUNNING CODE ONE API1");
            api.players.get_daily_chest(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
        else
        {
            api.players2.get_daily_chest(payload, function(error, errorcode, data) {
                if (error) {
                    if (testcallback) {
                        testcallback(error);
                    }
    
                    return output.terminate(payload, response, errorcode, error);
                }
    
                var r = output.end(payload, response, { "players": data }, errorcode);
    
                if (testing && testcallback) {
                    testcallback(null, r);
                }
            });
        }
	},

    get_locations: function(payload, request, response, testcallback)
    {   
        api.players2.get_locations(payload, function(error2, errorcode2, markers) {
            if (error2) {
                if (testcallback) {
                    testcallback(error2);
                }

                return output.terminate(payload, response, errorcode2, error2);
            }

            var r = output.end(payload, response, { "players": markers }, errorcode2);

            if (testing && testcallback) {
                testcallback(null, r);
            }
        });
    },

	check_campaign_prizes: function(payload, request, response, testcallback) {
        api.players2.check_campaign_prizes(payload, function(error, errorcode, prize_validations) {
            if (error) {
                if (testcallback) {
                    testcallback(error);
                }

                return output.terminate(payload, response, errorcode, error);
            }

            var r = output.end(payload, response, { "players": prize_validations }, errorcode);

            if (testing && testcallback) {
                testcallback(null, r);
            }
        });
	},

	get_campaign: function(payload, request, response, testcallback) {
        api.players2.get_campaign(payload, function(error, errorcode, prize_validations) {
            if (error) {
                if (testcallback) {
                    testcallback(error);
                }

                return output.terminate(payload, response, errorcode, error);
            }

            var r = output.end(payload, response, { "players": prize_validations }, errorcode);

            if (testing && testcallback) {
                testcallback(null, r);
            }
        });
	},
};