var basefolder = __dirname + "/../../../bica-game-server/api/",
    config = require(basefolder + "/config.js"),
    db = require(basefolder + "/database.js"),
    mongodb = require(basefolder + "../node_modules/mongodb"),
    md5 = require(basefolder + "/md5.js"),
    utils = require(basefolder + "/utils.js"),
    date = utils.fromTimestamp,
    datetime = require(basefolder + "/datetime.js"),
    fs = require('fs'),
    api = require(basefolder + "/../api"),
    errorcodes = require(__dirname + "/errorcodes.js").errorcodes,
    readline = require('readline'),
    seedrandom = require('seedrandom'),
    nodemailer = require('nodemailer'),
    { google } = require('googleapis'),
    keys = require("./client_secret.json"),
    schedule = require('node-schedule');



// Google Spreadsheets
const TOKEN_PATH = 'token.json';
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const EXCEL_UPDATE_INTERVAL = 43200000;

// Run at server startup
//authorize(keys, saveUserData);
/*setInterval(function() {
    authorize(keys, saveUserData);
}, EXCEL_UPDATE_INTERVAL);*/

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oauth2client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getNewToken(oauth2client, callback);
        oauth2client.setCredentials(JSON.parse(token));
        callback(oauth2client);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error while trying to retrieve access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

/**
 * Prints the names and majors of students in a sample spreadsheet:
 * @see https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function saveUserData(auth) {
    const sheets = google.sheets({ version: 'v4', auth });
    // Update Stats
    players.get_all_players_statistics(null, function(error, errorcodes, results) {
        var avgAge = 0;
        var avgAgeM = 0;
        var avgAgeF = 0;
        var pctGenderM = 0;
        var pctGenderF = 0;
        var totalM = 0;
        var totalF = 0;
        var totalIsParticipating = 0;
        var totalHasParticipatedLastYear = 0;
        var avgIsParticipating = 0;
        var avgHasParticipatedLastYear = 0;

        var users = [
            ["ID", "Nome", "Género", "Idade", "Distrito", "Região", "Escola", "Turma a participar", "Turma já participou", "Pontuação"]
        ];

        for (var i = 0; i < results.length; i++) {
            var currentUser = results[i];

            if (currentUser.debug_user === undefined || currentUser.debug_user === true) continue;

            if (currentUser.birthdayYear !== undefined) {
                var userBirthdayDate = new Date(results[i].birthdayYear, results[i].birthdayMonth - 1, results[i].birthdayDay);
                var today = new Date(datetime.utcnow);
                var age = today.getFullYear() - userBirthdayDate.getFullYear();

                var m = today.getMonth() - userBirthdayDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < userBirthdayDate.getDate())) {
                    age--;
                }

                if (age < 0) {
                    age = 0;
                }

                avgAge += age;
                currentUser["age"] = age;
            }

            if (currentUser.gender !== undefined) {
                if (currentUser.gender === 1) {
                    // M
                    totalM++;
                    avgAgeM += currentUser["age"];
                    currentUser["gender"] = "M";
                } else if (currentUser.gender === 2) {
                    // F
                    totalF++;
                    avgAgeF += currentUser["age"];
                    currentUser["gender"] = "F";
                }
            }

            // Failsafes
            if (currentUser["id"] === undefined) {
                currentUser["id"] = -1;
            }

            if (currentUser["name"] === undefined) {
                currentUser["name"] = "";
            }

            if (currentUser["gender"] === undefined) {
                currentUser["gender"] = "";
            }

            if (currentUser["age"] === undefined) {
                currentUser["age"] = -1;
            }

            if (currentUser["districtName"] === undefined) {
                currentUser["districtName"] = "";
            }

            if (currentUser["regionName"] === undefined) {
                currentUser["regionName"] = "";
            }

            if (currentUser["schoolName"] === undefined) {
                currentUser["schoolName"] = "";
            }

            if (currentUser["classIsParticipating"] === undefined) {
                currentUser["classIsParticipating"] = false;
            } else {
                if (currentUser["classIsParticipating"] === true) {
                    totalIsParticipating++;
                }
                currentUser["classIsParticipating"] = currentUser["classIsParticipating"] === true ? "Sim" : "Não";

            }

            if (currentUser["lastYearParticipated"] === undefined) {
                currentUser["lastYearParticipated"] = false;
            } else {
                if (currentUser["lastYearParticipated"] === true) {
                    totalHasParticipatedLastYear++;
                }
                currentUser["lastYearParticipated"] = currentUser["lastYearParticipated"] === true ? "Sim" : "Não";

            }

            if (currentUser["score"] === undefined) {
                currentUser["score"] = -1;
            }

            var newField = [currentUser["id"], currentUser["name"], currentUser["gender"], currentUser["age"], currentUser["districtName"], currentUser["regionName"], currentUser["schoolName"], currentUser["classIsParticipating"], currentUser["lastYearParticipated"], currentUser["score"]];
            users.push(newField);
        }

        avgAge /= results.length;
        avgAge = avgAge.toFixed(2);

        avgAgeM /= totalM;
        avgAgeM = avgAgeM.toFixed(2);

        avgAgeF /= totalF;
        avgAgeF = avgAgeF.toFixed(2);

        pctGenderM = totalM / results.length;
        pctGenderM *= 100;
        pctGenderM = pctGenderM.toFixed(2);
        pctGenderF = totalF / results.length;
        pctGenderF *= 100;
        pctGenderF = pctGenderF.toFixed(2);

        avgIsParticipating = parseFloat(totalIsParticipating) / results.length * 100;
        avgIsParticipating = avgIsParticipating.toFixed(2);

        avgHasParticipatedLastYear = parseFloat(totalHasParticipatedLastYear) / results.length * 100;
        avgHasParticipatedLastYear = avgHasParticipatedLastYear.toFixed(2);

        sheets.spreadsheets.values.clear({
            spreadsheetId: '1WCSFulVReUZ1Bs0RTJYh5CdVW5q-M_Hyq2AjwGrxQY0',
            range: 'Jogadores!A:J',
		range: 'Jogadores!A:J', 
            range: 'Jogadores!A:J',
        }, (err, res) => {
            if (err) return console.log('The API returned an error: ' + err);
            console.log("Users cleared!");


            // Update User List
            sheets.spreadsheets.values.update({
                spreadsheetId: '1WCSFulVReUZ1Bs0RTJYh5CdVW5q-M_Hyq2AjwGrxQY0',
                range: 'Jogadores!A:J',
                valueInputOption: 'USER_ENTERED',
                resource: {
                    values: users
                },
            }, (err2, res2) => {
                if (err2) return console.log('The API returned an error: ' + err2);
                console.log("Saved users successfully!");
            });
        });


        // Update statistics
        sheets.spreadsheets.values.update({
            spreadsheetId: '1WCSFulVReUZ1Bs0RTJYh5CdVW5q-M_Hyq2AjwGrxQY0',
            range: 'Estatísticas!A:B',
            valueInputOption: 'USER_ENTERED',
            resource: {
                values: [
                    ["Total jogadores", results.length],
                    ["Média Idades", avgAge.toString()],
                    ["Média Idades M", avgAgeM.toString()],
                    ["Média Idades F", avgAgeF.toString()],
                    ["Percentagem Jogadores M", pctGenderM.toString()],
                    ["Percentagem Jogadores F", pctGenderF.toString()],
                    ["Percentagem Turma A Participar", avgIsParticipating.toString()],
                    ["Percentagem Participou Ano PAssado", avgHasParticipatedLastYear.toString()]
                ]
            },
        }, (err, res) => {
            if (err) return console.log('The API returned an error: ' + err);
            console.log("Saved statistics successfully!");
        });

    });
}






// ----------------------------


// Consts
const _MAX_SCORE_PER_REQUEST = 100000;
const leaderboardMissionIds = [101, 102]; //101 - first place || 102 - 1-100th place
const _NUM_CHESTS = 5;
const _NICKNAME_THRESHOLD_FOR_EMAIL = 0.3;

//Markers
var markers = new Array();
var aldi = new Array();
var c111 = new Array();
var c222 = new Array();
var c333 = new Array();
var c444 = new Array();
var c555 = new Array();
var boyNicknames = new Array();
var girlNicknames = new Array();
var totalBoyNicknamesCount = 0;
var totalGirlNicknamesCount = 0;


//Bi-Monthly Leaderboard
var leaderboardDebug = new Array();
var leaderboard = new Array();
var leaderboardRange = 15; //half of the amount of players to send (excluding top 3 and player)

const { debug } = require('console');
// Emails
var nodemailer = require('nodemailer');
var sentEmailToday = false;

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'development.ontop@gmail.com',
        pass: 'lzalemgivzxwxlbh'
    }
});

var mailOptions = {
    from: 'development.ontop@gmail.com',
    to: 'development.ontop@gmail.com',
    subject: '[HeroisDaFruta] WARNING: Nicknames available under 30%',
    text: 'Hi, one of the lists of boys/girl available nicknames is below 30%. Please consider adding more nickname variants to the CSV file and restart the server.'
};

function getDateMonthYearString() {
    var utc = new Date(datetime.utcnow.getTime());
    return (utc.getMonth() + 1) + "-" + utc.getFullYear();
}

function getPreviousDateMonthYearString() {
    var utc = new Date(datetime.utcnow.getTime());
    return (utc.getMonth()) + "-" + utc.getFullYear();
}

function getPreviousLeaderboardDateStrings() {
    var utc = new Date(datetime.utcnow.getTime());
    var date1;
    var date2;
    if (getDateMonthString() % 2 == 0)
    {
        utc.setMonth(utc.getMonth()-2);
        date1 = (utc.getMonth()) + "-" + utc.getFullYear();
        date2 = (utc.getMonth()+1) + "-" + utc.getFullYear();
        
        // console.log(date1);
        // console.log(date2);
    }
    else
    {
        utc.setMonth(utc.getMonth()-1);
        date1 = (utc.getMonth()) + "-" + utc.getFullYear();
        date2 = (utc.getMonth()+1) + "-" + utc.getFullYear();
        
        // console.log(date1);
        // console.log(date2);
    }

    var dates = {};
    dates.date1 = date1
    dates.date2 = date2;
    return dates;
}

function getCurrentLeaderboardDateStrings() {
    var utc = new Date(datetime.utcnow.getTime());
    var date1;
    var date2;
    if (getDateMonthString() % 2 == 0)
    {
        date1 = (utc.getMonth()) + "-" + utc.getFullYear();
        date2 = (utc.getMonth()+1) + "-" + utc.getFullYear();
    }
    else
    {
        date2 = (utc.getMonth()+1) + "-" + utc.getFullYear();

        date1 = null;
        // console.log(date2);
    }

    var dates = {};
    dates.date1 = date1
    dates.date2 = date2;
    return dates;
}

function getDateMonthString() {
    var utc = new Date(datetime.utcnow.getTime());
    return (utc.getMonth() + 1);
}

function reorderLeaderboard() {
    var dateMonthYear = getDateMonthYearString();

    leaderboard.sort(function(a, b) {
        var aScore = a.score;
        var bScore = b.score;
        return bScore - aScore;
    });
    
    var position = 0;

    leaderboard.forEach(playerLB => {
        position++;
        playerLB.position = position
    });

    leaderboardDebug.sort(function(a, b) {
        var aScore = a.score;
        var bScore = b.score;
        return bScore - aScore;
    });

    position = 0;
    
    leaderboardDebug.forEach(playerLB => {
        position++;
        playerLB.position = position
    });
}



var players = module.exports = {

    // To be used internally only!
    update_leaderboard: function(options, callback) {

        // var sortDate = "scores." + getDateMonthYearString();
        var sortDate = "score";

        var sort = {};
        sort[sortDate] = -1;

        var query = {
            safe: true,
            filter: {"score":{"exists": true}},
            sort: { score: -1 }
        };

        db.bicaserver.players.get(query, function(error, results) {
            if (error) {
                callback("error aggregating players from database", errorcodes.GeneralError);
                return;
            }

            callback(null, errorcodes.NoError, results);
        });
    },

    get_all_players: function(options, callback) {
        var query = {
            filter: {
                "id": { "$exists": true }
            },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, results) {
            if (error) {
                callback("error aggregating players from database (api.player.add_player:28)", errorcodes.GeneralError);
                return;
            }

            callback(null, errorcodes.NoError, results);
        });
    },

    get_all_players_statistics: function(options, callback) {
        var query = {
            filter: {
                "id": { "$exists": true },
                $and: [
                    { "debug_user": { "$exists": true } },
                    { "debug_user": false }
                ]
            },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, results) {
            if (error) {
                callback("error aggregating players from database (api.player.add_player:28)", errorcodes.GeneralError);
                return;
            }

            callback(null, errorcodes.NoError, results);
        });
    },

    get_all_players_debug: function(options, callback) {
        var query = {
            filter: {
                "id": { "$exists": true },
                $and: [
                    { "debug_user": { "$exists": true } },
                    { "debug_user": true }
                ]
            },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, results) {
            if (error) {
                callback("error aggregating players from database (api.player.add_player:28)", errorcodes.GeneralError);
                return;
            }

            callback(null, errorcodes.NoError, results);
        });
    },

    get_random_nickname: function(options, callback) {

        if (options.gender === undefined) {
            callback("error: no gender variable  (api.players.get_random_nickname:98)", errorcodes.GeneralError);
            return;
        }

        var player = {};

        var randomIndex = -1;
        var randomNickname = "";
        var foundNickname = false;

        if (options.gender === 1) {
            if (boyNicknames.length === 0) {
                callback("error: no more nicknames available for gender " + options.gender + "  (api.players.get_random_nickname:98)", errorcodes.GeneralError, false);
                return;
            }

            randomIndex = Math.floor(Math.random() * boyNicknames.length);
            randomNickname = boyNicknames[randomIndex];
            foundNickname = true;
        } else if (options.gender === 2) {
            if (girlNicknames.length === 0) {
                callback("error: no more nicknames available for gender " + options.gender + "  (api.players.get_random_nickname:98)", errorcodes.GeneralError, false);
                return;
            }

            randomIndex = Math.floor(Math.random() * girlNicknames.length);
            randomNickname = girlNicknames[randomIndex];
            foundNickname = true;
        } else {
            callback("error: no valid gender specified  (api.players.get_random_nickname:98)", errorcodes.GeneralError, false);
            return;
        }

        if (foundNickname && randomIndex > -1) {
            player["name"] = randomNickname;
            callback(null, errorcodes.NoError, player);
        } else {
            callback("error: no nickname found  (api.players.get_random_nickname:98)", errorcodes.GeneralError, false);
        }
    },

    add_player: function(options, callback) {

        var query = {
            aggregate: [{
                    $sort: { id: -1 }
                },
                {
                    $limit: 1
                }
            ]
        };

        db.bicaserver.players.aggregate(query, function(error, results) {

            if (error) {
                callback("error aggregating players from database (api.player.add_player:28)", errorcodes.GeneralError);
                return;
            }

            if (results.length === 0) {
                options.id = 0;
            } else {
                options.id = results[0].id + 1;
            }
            options.timestamp = datetime.now;

            var newPlayer = {};
            newPlayer.id = options.id;
            newPlayer.name = options.name;
            newPlayer.score = options.score;
            newPlayer.scores = {};
            newPlayer.scores[getDateMonthYearString()] = options.score;
            newPlayer.gender = options.gender;
            newPlayer.birthdayDay = options.birthdayDay;
            newPlayer.birthdayMonth = options.birthdayMonth;
            newPlayer.birthdayYear = options.birthdayYear;
            newPlayer.districtName = options.districtName;
            newPlayer.regionName = options.regionName;
            newPlayer.schoolName = options.schoolName;
            newPlayer.classIsParticipating = options.classIsParticipating;
            newPlayer.lastYearParticipated = options.lastYearParticipated;

            var utc = new Date(datetime.utcnow.getTime());
            var dateTime = new Object();
            dateTime.year = utc.getFullYear();
            dateTime.month = utc.getMonth() + 1;
            dateTime.day = utc.getDate();
            dateTime.hour = 0;
            dateTime.minutes = 0;
            dateTime.seconds = 0;
            dateTime.miliseconds = 0;
            newPlayer.date = dateTime;

            if (options.debug_user != undefined && options.debug_user != null)
                newPlayer.debug_user = options.debug_user;

            if (options.cards === undefined || options.cards === null)
                newPlayer.cards = [];
            else
                newPlayer.cards = options.cards;

            if (options.cards === undefined || options.cards === null)
                newPlayer.markers = [];
            else
                newPlayer.markers = options.markers;


            var query2 = {
                doc: newPlayer,
                safe: true
            };

            db.bicaserver.players.insert(query2, function(error2, items) {

                if (error2) {
                    callback("error inserting player into database (api.player.add_player:43)", errorcodes.GeneralError);
                    return;
                }

                var boyNamesIndex = boyNicknames.indexOf(newPlayer.name);
                if (boyNamesIndex > -1) {
                    boyNicknames.splice(boyNamesIndex, 1);
                }

                var girlNamesIndex = girlNicknames.indexOf(newPlayer.name);
                if (girlNamesIndex > -1) {
                    girlNicknames.splice(girlNamesIndex, 1);
                }

                executeUpdate();
                callback(null, errorcodes.NoError, newPlayer);

                if (sentEmailToday === true) return;

                var girlNicknamesPercentRemaining = girlNicknames.length / totalGirlNicknamesCount;
                var boyNicknamesPercentRemaining = boyNicknames.length / totalBoyNicknamesCount;

                if (girlNicknamesPercentRemaining <= _NICKNAME_THRESHOLD_FOR_EMAIL || boyNicknamesPercentRemaining <= _NICKNAME_THRESHOLD_FOR_EMAIL) {
                    console.log("ALERT: One of nicknames array is below treshold of " + (_NICKNAME_THRESHOLD_FOR_EMAIL * 100) + "%");

                    transporter.sendMail(mailOptions, function(errorm, info) {
                        if (errorm) {
                            console.log(errorm);
                        } else {
                            console.log('Email sent: ' + info.response);
                            sentEmailToday = true;
                        }
                    });
                }
            });
        });

    },

    get_player: function(player, callback) {

        if (player.id === null || player.id === undefined) {
            callback("error: no player id specified  (api.players.get_player:68)", errorcodes.GeneralError);
            return;
        }

        var query = {
            filter: { id: player.id },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players) {

            if (error) {
                callback("error getting player from database (api.players.get_player:28)", errorcodes.GeneralError);
                return;
            }

            var player = players[0];
            var utc = new Date(datetime.utcnow.getTime());
            var dateTime = new Object();
            dateTime.year = utc.getFullYear();
            dateTime.month = utc.getMonth() + 1;
            dateTime.day = utc.getDate();
            dateTime.hour = 0;
            dateTime.minutes = 0;
            dateTime.seconds = 0;
            dateTime.miliseconds = 0;
            player.date = dateTime;
            callback(null, errorcodes.NoError, player);

            var updatedPlayer = players[0];
            updatedPlayer["lastlogin"] = datetime.now;

            var query2 = {
                filter: { id: players[0].id },
                doc: updatedPlayer,
                safe: true
            }

            db.bicaserver.players.update(query2, function(error2, players2) {
                if (error2) {
                    console.log("error updatating player login date (api.players.get_player:28)", errorcodes.GeneralError);
                }
            });
        });
    },

    update_player: function(player, callback) {

        if (player.id === null || player.id === undefined) {
            callback("error: no player id specified  (api.players.update_player:68)", errorcodes.GeneralError);
            return;
        }

        var query = {
            filter: { id: player.id },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players) {

            if (error) {
                callback("error getting player from database (api.players.update_player:28)", errorcodes.GeneralError);
                return;
            }

            var filteredPlayer = player;
            delete filteredPlayer["action"];
            delete filteredPlayer["section"];
            delete filteredPlayer["publickey"];
            delete filteredPlayer["ip"];
            delete filteredPlayer["baseurl"];
            delete filteredPlayer["url"];

            var utc = new Date(datetime.utcnow.getTime());
            var dateTime = new Object();
            dateTime.year = utc.getFullYear();
            dateTime.month = utc.getMonth() + 1;
            dateTime.day = utc.getDate();
            dateTime.hour = 0;
            dateTime.minutes = 0;
            dateTime.seconds = 0;
            dateTime.miliseconds = 0;
            filteredPlayer.date = dateTime;

            var query2 = {
                doc: filteredPlayer,
                filter: { id: player.id },
                safe: true
            };

            db.bicaserver.players.update(query2, function(error, players2) {

                if (error) {
                    callback("error updating player from database (api.players.update_player:28)", errorcodes.GeneralError);
                    return;
                }

                callback(null, errorcodes.NoError, filteredPlayer);
            });
        });
    },

    //validate_marker?
    validate_marker: function(marker, callback)
	{
		console.log("validate_marker");
		
        if (marker.playerId === null || marker.playerId === undefined)
		{
            callback("error: no player id specified  (api.players.validate_marker:362)", errorcodes.NoUsername);
            return;
        }

        var query =
		{
            filter: { id: marker.playerId },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players)
		{
            if (error)
			{
                callback("error getting player from database (api.players.validate_marker:374)", errorcodes.NoUsername);
                return;
            }

            var filteredPlayer = players[0];

            //check if pack is being scanned on the same day
            var utc = new Date(datetime.utcnow.getTime());
			
			// calculate week number:
			var oneJan = new Date(utc.getFullYear(), 0, 1);
			var numberOfDays = Math.floor((utc - oneJan) / (24 * 60 * 60 * 1000));
			var utcWeekNumber = Math.ceil((utc.getDay() - 1 + numberOfDays) / 7);
			//console.log(`The week number of the current date (${utc}) is ${utcWeekNumber}.`);
			
            var Check24h = true;
            var isUniqueMarker = false;
			var isRegionMarker = false;
			var isMonthly = false;
			//var regionId = 0;
			var geoLocation = 0;

			var isMarkerTimeValidated = false;
			var isMarkerDateValidated = false;
			var isMarkerRegionValidated = false;
			
			for(var i = 0; i < markers.length; i++)
			{
				if(markers[i] !== undefined && markers[i].id !== undefined && markers[i].id === marker.markerId)
				{
					var serverMarker = markers[i];
                    if(serverMarker.unique === true)
                    {
                        isUniqueMarker = true;
                    }
                    if(serverMarker.monthly === true)
                    {
                        isMonthly = true;
                    }
					// Check if marker is a region marker
					if(serverMarker.geoLocation === true && serverMarker.normal === true)
					{
						var check = CheckMarkerRegionGPSDistance(marker.markerId, marker.location.x, marker.location.y);
						isRegionMarker = check.success;
						//regionId = check.regionId;
						geoLocation = check.geoLocation;
						break;
					}
					else
					{
						isRegionMarker = false;
						//regionId = 0;
						geoLocation = 0;
						break;
					}
				}
			}

            var m = new Array();
            filteredPlayer.markers.forEach(element => {
                if (element.markerId === marker.markerId)
                    m.push(element);
            });
			
			if(marker.markerId == "c111")
				isRegionMarker = true;

			console.log("checking 24h");
            if (Array.isArray(m))
			{
                if(isUniqueMarker && m.length > 0)
                {
                    console.log("Unique FAIL");
                    callback("error validating marker: marker " + marker.markerId + " is unique and has already been redeemed (api.players.validate_marker:28)", errorcodes.MarkerScannedUnique);
                    return;
                }
                // check if found any
                if (m.length === 0)
				{
                    Check24h = true;
				}
                else
				{
                    // if any marker already on the player is in the same day
                    // as the current date then it can't be scanned
                    Check24h = true;
                    m.forEach(e =>
					{
						if(isRegionMarker)
						{
							var e_date = new Date(e.date.year, e.date.month - 1, e.date.day);
							var e_oneJan = new Date(e_date.getFullYear(), 0, 1);
							var e_numberOfDays = Math.floor((e_date - e_oneJan) / (24 * 60 * 60 * 1000));
							var e_weekNumber = Math.ceil((e_date.getDay() - 1 + e_numberOfDays) / 7);
							//console.log(`The week number of the marker date (${e_date}) is ${e_weekNumber}.`);
							
	                        if (e.date.year === utc.getFullYear() &&
	                            e.date.month === utc.getMonth() + 1 &&
								e_weekNumber == utcWeekNumber)
							{
								//if(e.locationId !== undefined && e.locationId === regionId)
								if(e.geoLocation !== undefined && e.geoLocation === geoLocation)
								{
	                            	Check24h = false;
								}
							}
						}
						else if(isMonthly) // one time only
						{
	                        if (e.date.year === utc.getFullYear() &&
	                            e.date.month === utc.getMonth() + 1)
							{
								Check24h = false;
							}
						}
						else
						{
	                        if (e.date.year === utc.getFullYear() &&
	                            e.date.month === utc.getMonth() + 1 &&
								e.date.day === utc.getDate())
							{
								Check24h = false;
	                        }
						}
                    });
                }
            }
			else Check24h = true;
			
            if (Check24h)
			{
                var dateTime = new Object();
                dateTime.year = utc.getFullYear();
                dateTime.month = utc.getMonth() + 1;
                dateTime.day = utc.getDate();
                dateTime.hour = 0;
                dateTime.minutes = 0;
                dateTime.seconds = 0;
                dateTime.miliseconds = 0;

                marker.date = dateTime;
            }
			else
			{
				if(isRegionMarker)
					callback("error validating marker: marker " + marker.markerId + " already scanned today (api.players.validate_marker:422)", errorcodes.MarkerScannedRepeatRegion);
				else if(isMonthly)
                    callback("error validating marker: marker " + marker.markerId + " already scanned this month (api.players.validate_marker:422)", errorcodes.MarkerScannedMonthly);
                else
					callback("error validating marker: marker " + marker.markerId + " already scanned today (api.players.validate_marker:422)", errorcodes.MarkerAlreadyScanned);
				
                return;
			}
			
			var foundMarker = false;
			
			// Find scanned marker in array
			for(var i = 0; i < markers.length; i++)
			{				
				if(markers[i] !== undefined && markers[i].id !== undefined && markers[i].id === marker.markerId)
				{
					foundMarker = true;
					var serverMarker = markers[i];
					
					// Check if marker is a region marker
					if(serverMarker.geoLocation === true && serverMarker.normal === true)
					{
						isMarkerRegionValidated = CheckMarkerRegionGPSDistance(marker.markerId, marker.location.x, marker.location.y).success;
					}
					// Check if marker is active all day
					else if(serverMarker.dated === true)
					{
						if(serverMarker.dateTime.day === utc.getDate() &&
							serverMarker.dateTime.month === utc.getMonth() + 1 &&
							serverMarker.dateTime.year === utc.getFullYear())
							{
								isMarkerDateValidated = true;
							}
					}
					// Check if Marker is active only at a certain time of the same day
					else if(serverMarker.timed === true)
					{
						if(serverMarker.dateTime.day === utc.getDate() &&
							serverMarker.dateTime.month === utc.getMonth() + 1 &&
							serverMarker.dateTime.year === utc.getFullYear())
							{
								var startTime = new Date(serverMarker.dateTime.year, serverMarker.dateTime.month - 1, serverMarker.dateTime.day, serverMarker.dateTime.hour, serverMarker.dateTime.minutes, serverMarker.dateTime.seconds);
								
								var endTime = new Date(startTime.getTime());
								endTime.setMinutes(endTime.getMinutes() + serverMarker.minutes);
								
								if(utc >= startTime && utc <= endTime)
								{
									isMarkerTimeValidated = true;
								}
							}
					}
					else
					{
						isMarkerTimeValidated = true;
						isMarkerDateValidated = true;
						isMarkerRegionValidated = true;
					}
					
					break;
				}
				else if(markers[i] === undefined || markers[i].id === undefined)
				{
					console.log("MARKER OF INDEX " + i + " UNDEFINED??");
					for(var j = 0; j < markers.length; j++)
					{
						console.log(markers[j], "");
					}
				}
			}
			
			if(!foundMarker)
			{
                console.log("NOT FOUND");
                callback("error validating marker: marker " + marker.markerId + " doesn't exist (api.players.validate_marker:28)", errorcodes.GeneralError);
                return;
            }

            if (!isMarkerRegionValidated && serverMarker.geoLocation === true && serverMarker.normal === true) {
                console.log("Location FAIL");
                callback("error validating marker: marker " + marker.markerId + " not on correct location (api.players.validate_marker:28)", errorcodes.MarkerScannedWrongLocation);
                return;
            }

            if (!isMarkerDateValidated && serverMarker.dated === true) {
                console.log("DATE FAIL");
                callback("error validating marker: marker " + marker.markerId + " not on correct date (api.players.validate_marker:28)", errorcodes.MarkerScannedWrongDate);
                return;
            }

            if (!isMarkerTimeValidated && serverMarker.timed === true) {
                console.log("TIMED FAIL");
                callback("error validating marker: marker " + marker.markerId + " not on correct time interval (api.players.validate_marker:28)", errorcodes.MarkerScannedWrongTime);
                return;
            }

            callback(null, errorcodes.NoError, marker);

        });
    },

    add_marker: function(marker, callback)
	{
        if (marker.playerId === null || marker.playerId === undefined)
		{
            callback("error: no player id specified  (api.players.update_player:68)", errorcodes.GeneralError);
            return;
        }

        var query =
		{
            filter: { id: marker.playerId },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players)
		{
            if (error)
			{
                callback("error getting player from database (api.players.update_player:28)", errorcodes.GeneralError);
                return;
            }

            var filteredPlayer = players[0];

            //check if pack is being scanned on the same day
            var utc = new Date(datetime.utcnow.getTime());

            var dateTime = new Object();
            dateTime.year = utc.getFullYear();
            dateTime.month = utc.getMonth() + 1;
            dateTime.day = utc.getDate();
            dateTime.hour = 0;
            dateTime.minutes = 0;
            dateTime.seconds = 0;
            dateTime.miliseconds = 0;

            var markerAdd = new Object();
            markerAdd.markerId = marker.markerId;
            markerAdd.date = dateTime;

			for(var i = 0; i < markers.length; i++)
			{				
				if(markers[i] !== undefined && markers[i].id !== undefined && markers[i].id === marker.markerId)
				{
					foundMarker = true;
					var serverMarker = markers[i];

					// Check if marker is a region marker
					if(serverMarker.geoLocation === true && serverMarker.normal === true)
					{
						var foundMarker = CheckMarkerRegionGPSDistance(marker.markerId, marker.location.x, marker.location.y);
						//markerAdd.locationId = foundMarker.regionId;
						markerAdd.geoLocation = foundMarker.geoLocation;
						console.log("------------------LOCATION ID: ", markerAdd.locationId);
						break;
					}
					else
					{
						markerAdd.locationId = 0;
						markerAdd.geoLocation = 0;
						break;
					}
				}
			}

            console.log(markerAdd, "");

            filteredPlayer.markers.push(markerAdd);

            marker.date = dateTime;

            var query2 =
			{
                doc: filteredPlayer,
                filter: { id: marker.playerId },
                safe: true
            };

            db.bicaserver.players.update(query2, function(error2, players2)
			{
                if (error2)
				{
                    callback("error updating player from database (api.players.update_player:28)", errorcodes.GeneralError);
                    return;
                }

                callback(null, errorcodes.NoError, marker);
            });

        });
    },

    add_score: function(options, callback)
	{
        if (options.playerId === null || options.playerId === undefined)
		{
            callback("error: no player id specified  (api.players.add_score:68)", errorcodes.GeneralError, false);
            return;
        }

        if (options.score === undefined || options.score > _MAX_SCORE_PER_REQUEST)
		{
            callback("error: no score provided or score over maximum allowed (api.players.add_score:68)", errorcodes.GeneralError, false);
            return;
        }

        var query =
		{
            filter: { id: options.playerId },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players)
		{
            if (error)
			{
                callback("error getting player from database (api.players.add_score:28)", errorcodes.GeneralError, false);
                return;
            }

            var filteredPlayer = players[0];
            var dateMonthYearString = getDateMonthYearString();
            var prevDateMonthYearString = getPreviousDateMonthYearString();

            if (filteredPlayer.scores === undefined)
                filteredPlayer.scores = {};

            if (filteredPlayer.scores[dateMonthYearString] === undefined)
                filteredPlayer.scores[dateMonthYearString] = 0;
			
            filteredPlayer.scores[dateMonthYearString] += options.score;

            if (getDateMonthString() % 2 == 0)
            {
                var currentMonthScore = filteredPlayer.scores[dateMonthYearString];

                if (filteredPlayer.scores[prevDateMonthYearString] === undefined)
                    filteredPlayer.scores[prevDateMonthYearString] = 0;
				
                var previousMonthScore = filteredPlayer.scores[prevDateMonthYearString];
                filteredPlayer.score = currentMonthScore + previousMonthScore;
            }
            else
            {
                filteredPlayer.score = filteredPlayer.scores[dateMonthYearString];
            }
			
            var lbPlayer;
            if (filteredPlayer.debug_user === undefined || filteredPlayer.debug_user)
                lbPlayer = leaderboardDebug.find(element => element.id === options.playerId);
            else
                lbPlayer = leaderboard.find(element => element.id === options.playerId);
			
            lbPlayer.score = filteredPlayer.score;
            lbPlayer.scores = filteredPlayer.scores;
            reorderLeaderboard();

            var query2 =
			{
                doc: filteredPlayer,
                filter: { id: options.playerId },
                safe: true
            };

            db.bicaserver.players.update(query2, function(error2, players2)
			{
                if (error2)
				{
                    callback("error updating player from database (api.players.add_score:28)", errorcodes.GeneralError, false);
                    return;
                }

                callback(null, errorcodes.NoError, true);
            });
        });
    },

    add_score_mission: function(mission, callback) {

        if (mission.playerId === null || mission.playerId === undefined) {
            callback("error: no player id specified  (api.players.update_player:68)", errorcodes.GeneralError);
            return;
        }

        var query = {
            filter: { id: mission.playerId },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players) {

            if (error) {
                callback("error getting player from database (api.players.update_player:28)", errorcodes.GeneralError);
                return;
            }

            var filteredPlayer = players[0];
            filteredPlayer.score += mission.score;
            if (filteredPlayer.scores === undefined) {
                filteredPlayer.scores = {};
            }

            var dateMonthYearString = getDateMonthYearString();

            if (filteredPlayer.scores[dateMonthYearString] === undefined) {
                filteredPlayer.scores[dateMonthYearString] = 0;
            }

            filteredPlayer.scores[dateMonthYearString] += options.score;

            var lbPlayer;
            if (filteredPlayer.debug_user)
                lbPlayer = leaderboardDebug.find(element => element.id === options.playerId);
            else
                lbPlayer = leaderboard.find(element => element.id === options.playerId);
            lbPlayer.score = filteredPlayer.score;
            lbPlayer.scores = filteredPlayer.scores;
            reorderLeaderboard();

            var query2 = {
                doc: filteredPlayer,
                filter: { id: mission.playerId },
                safe: true
            };

            db.bicaserver.players.update(query2, function(error2, players2) {

                if (error2) {
                    callback("error updating player from database (api.players.update_player:28)", errorcodes.GeneralError);
                    return;
                }

                callback(null, errorcodes.NoError, mission);
            });
        });
    },

    add_card: function(card, callback) {

        if (card.playerId === null || card.playerId === undefined) {
            callback("error: no player id specified  (api.players.update_player:68)", errorcodes.GeneralError);
            return;
        }

        var query = {
            filter: { id: card.playerId },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players) {

            if (error) {
                callback("error getting player from database (api.players.update_player:28)", errorcodes.GeneralError);
                return;
            }

            var filteredPlayer = players[0];
            var cardFound = filteredPlayer.cards.find(element => element.cardId === card.cardId);
            if (cardFound === null || cardFound === undefined) {
                var c = { cardId: card.cardId, numberCollected: 1 };
                filteredPlayer.cards.push(c);
                // console.log(c.numberCollected);
            } else {
                var objIndex = filteredPlayer.cards.findIndex((obj => obj.cardId == card.cardId));
                filteredPlayer.cards[objIndex].numberCollected += 1;
                card.numberCollected = filteredPlayer.cards[objIndex].numberCollected;
                // console.log("2");
            }

            var query2 = {
                doc: filteredPlayer,
                filter: { id: card.playerId },
                safe: true
            };

            db.bicaserver.players.update(query2, function(error, players2) {

                if (error) {
                    callback("error adding card to database (api.players.add_card)", errorcodes.GeneralError);
                    return;
                }

                callback(null, errorcodes.NoError, card);
            });
        });
    },

    get_leaderboard: function(player, callback) {

        if (leaderboard.length === 0 && leaderboardDebug.length === 0) {
            callback("error: no players in Leaderboard", errorcodes.GeneralError);
            return;
        }

        if (player.id === null || player.id === undefined) {
            callback("error: no player id specified", errorcodes.GeneralError);
            return;
        }

        var lb;
        if (player.debug_user)
            lb = leaderboardDebug;
        else
            lb = leaderboard;

        var temp = new Array();

        var playerPosition = lb.find(element => element.id === player.id).position;

        lb.forEach(playerLB => {
            if (playerLB.position < 4 || playerLB.id === player.id ||
                (playerLB.position <= (playerPosition + leaderboardRange) && playerLB.position >= (playerPosition - leaderboardRange))) {
                var tempPlayer = new Object();
                tempPlayer.id = playerLB.id;
                tempPlayer.name = playerLB.name;
                tempPlayer.score = playerLB.score;
                tempPlayer.position = playerLB.position;

                temp.push(tempPlayer);
            }
        });

        console.log(temp);
        callback(null, errorcodes.NoError, temp);
    },

    get_leaderboard_district: function(player, callback) {

        if (leaderboard.length === 0 && leaderboardDebug.length === 0) {
            callback("error: no players in Leaderboard", errorcodes.GeneralError);
            return;
        }

        if (player.id === null || player.id === undefined) {
            callback("error: no player id specified", errorcodes.GeneralError);
            return;
        }

        var temp = new Array();
        var districtLB = new Array();

        var lb;
        if (player.debug_user)
            lb = leaderboardDebug;
        else
            lb = leaderboard;

        var position = 0;
        lb.forEach(playerLB => {
            if (playerLB.district === player.filter) {
                position++;
                var tempPlayer = new Object();
                tempPlayer.id = playerLB.id;
                tempPlayer.name = playerLB.name;
                tempPlayer.score = playerLB.score;
                tempPlayer.position = position;

                districtLB.push(tempPlayer);
            }
        });

        var playerPosition = districtLB.find(element => element.id === player.id).position;
        districtLB.forEach(playerLB => {
            if (playerLB.position < 4 || playerLB.id === player.id ||
                (playerLB.position <= (playerPosition + leaderboardRange) && playerLB.position >= (playerPosition - leaderboardRange))) {
                temp.push(playerLB);
            }
        });

        console.log(temp);
        callback(null, errorcodes.NoError, temp);
    },

    get_leaderboard_region: function(player, callback) {

        if (leaderboard.length === 0 && leaderboardDebug.length === 0) {
            callback("error: no players in Leaderboard", errorcodes.GeneralError);
            console.log("HAHAHAHA")
            return;
        }

        if (player.id === null || player.id === undefined) {
            callback("error: no player id specified", errorcodes.GeneralError);
            console.log("HEHEHEHE")
            return;
        }

        var temp = new Array();
        var regionLB = new Array();

        var lb;
        if (player.debug_user)
            lb = leaderboardDebug;
        else
            lb = leaderboard;

        var position = 0;
        lb.forEach(playerLB => {
            if (playerLB.region === player.filter) {
                position++;
                var tempPlayer = new Object();
                tempPlayer.id = playerLB.id;
                tempPlayer.name = playerLB.name;
                tempPlayer.score = playerLB.score;
                tempPlayer.position = position;

                regionLB.push(tempPlayer);
            }
        });

        var playerPosition = regionLB.find(element => element.id === player.id).position;
        regionLB.forEach(playerLB => {
            if (playerLB.position < 4 || playerLB.id === player.id ||
                (playerLB.position <= (playerPosition + leaderboardRange) && playerLB.position >= (playerPosition - leaderboardRange))) {
                temp.push(playerLB);
            }
        });

        callback(null, errorcodes.NoError, temp);
    },

    get_leaderboard_lastmonth_top3: function(player, callback) {

        var utc = new Date(datetime.utcnow.getTime());
        utc.setMonth(utc.getMonth() - 1);
        var utcstring = (utc.getMonth() + 1) + "-" + utc.getFullYear();

        var scoreString = "scores." + utcstring;
        var sortScore = {};
        sortScore[scoreString] = -1;

        var matchStr = {};
        matchStr[scoreString] = { $ne: null };

        var query = {
            aggregate: [{
                    $sort: sortScore
                },
                {
                    $limit: 3
                },
                {
                    $match: matchStr
                }
            ]
        };

        db.bicaserver.players.aggregate(query, function(error, results) {

            if (error) {
                callback("error getting last month leaderboard (api.players.get_leaderboard_lastmonth_top3)", errorcodes.GeneralError);
                return;
            }

            callback(null, errorcodes.NoError, results);
        });
    },

    get_markers: function(player, callback) {

        if (markers.length === 0) {
            callback("error: no markers in list", errorcodes.GeneralError);
            return;
        }

        // var temp = new Array();
        // markers.forEach(marker => {
        //     temp.push(marker);
        // });
        console.log(markers[1]);

        callback(null, errorcodes.NoError, markers);
    },

    check_leaderboard_prizes: function(options, callback) {

        console.log("HERE _______________________");
        if (options.playerId === undefined) {
            callback("no player id found (api.players.check_leaderboard_prizes:25)", errorcodes.GeneralError, false);
            return;
        }

        if (options.debug_user === undefined) {
            callback("debug_user variable undefined (api.players.check_leaderboard_prizes:25)", errorcodes.GeneralError, false);
            return;
        }

        if(options.debug_user === true)
        {
            players.get_all_players_debug(options, (error, errorcode, results) => {
                if (error) {
                    callback("error getting last month leaderboard (api.players.check_leaderboard_prizes)", errorcodes.GeneralError);
                    return;
                }

                var missionConfirmValues = [];
                for (var i = 0; i < leaderboardMissionIds.length; i++)
                {
                    missionConfirmValues[leaderboardMissionIds[i]] = false;
                }
    
                var missionCheckId = -1;

                var months = getPreviousLeaderboardDateStrings();
                    
                results.sort(function(a, b) {
                    if(a.scores === undefined) a.scores = {};
                    if(a.scores[months.date1] === undefined) a.scores[months.date1] = -1;
                    if(a.scores[months.date2] === undefined) a.scores[months.date2] = -1;
                    if(b.scores === undefined) b.scores = {};
                    if(b.scores[months.date1] === undefined) b.scores[months.date1] = -1;
                    if(b.scores[months.date2] === undefined) b.scores[months.date2] = -1;

                    var aScore = a.scores[months.date1] + a.scores[months.date2];
                    var bScore = b.scores[months.date1] + b.scores[months.date2];

                    if(a.id == options.id) console.log(aScore);

                    return bScore - aScore;
                });
    
                // for (var i = 0; i < 100; i++) {
                //     missionConfirmValues[leaderboardMissionIds[i]] = filtered[i].id === options.playerId;
                //     if (filtered[i].id === options.playerId) {
                //         missionCheckId = leaderboardMissionIds[i];
                //     }
                // }
    
                //first place mission
                if(results[0].id === options.playerId && (results[0].scores[months.date1] + results[0].scores[months.date2]) >= 0)
                    missionConfirmValues[leaderboardMissionIds[0]] = true;

                
    
                //top 100 mission
                for (var i = 0; i < results.length && i < 100; i++)
                {
                    if((results[i].scores[months.date1] + results[i].scores[months.date2]) >= 0)
                    {
                        missionConfirmValues[leaderboardMissionIds[1]] = results[i].id === options.playerId;
                        if (results[i].id === options.playerId) {
                            missionCheckId = leaderboardMissionIds[1];
                            break;
                        }
                    }                    
                    
                }
    
                var missionsArray = [];
    
                // TODO: Make dictionaries valid in client side, not just turn this into an Array
                for (var key in missionConfirmValues) {
                    var newObject = {
                        "missionId": key,
                        "isAvailable": missionConfirmValues[key]
                    };
                    missionsArray.push(newObject);
                }
    
                if (missionCheckId === -1) {
                    callback(null, errorcodes.NoError, missionsArray);
                } else {
                    // check redeems
                    var query2 = {
                        filter: {
                            "missionId": missionCheckId
                        },
                        safe: true
                    };
    
                    db.bicaserver.redeems.get(query2, function(error2, results2) {
    
                        if (error2) {
                            console.log(error2);
                            callback("error getting redeems data from database (api.players.check_leaderboard_prizes:25)", errorcodes.GeneralError, false);
                            return;
                        }
    
                        if (results2.length === 0) {
                            callback(null, errorcodes.NoError, missionsArray);
                        } else {
                            var redeems = results2[0];
                            for (var i = 0; i < redeems.redeemData.length; i++) {
                                var currentUser = redeems.redeemData[i];
    
                                var utctime = new Date(datetime.utcnow.getTime());
                                var month;
                                if (getDateMonthString() % 2 == 0)
                                {
                                    console.log(utctime.getMonth()+1);
                                    utctime.setMonth(utctime.getMonth() - 2);
                                    
                                    console.log("second month");
                                    month = utctime.getMonth()+1;
                                    console.log(month);
                                }
                                else
                                {
                                    console.log(utctime.getMonth()+1);

                                    utctime.setMonth(utctime.getMonth()-1);
                                    month = utctime.getMonth()+1;
                                    console.log(month);
                                }
                                
                                for (var j = 0; j < missionsArray.length; j++) {
                                    console.log(missionsArray[j].isAvailable);
                                }
    
                                if (currentUser.playerId === options.playerId && currentUser.leaderboardMonth === (utctime.getMonth() + 1) && currentUser.leaderboardYear === utctime.getFullYear()) {
    
                                    for (var j = 0; j < missionsArray.length; j++) {
                                        missionsArray[j].isAvailable = false;
                                    }
                                    break;
                                }
                            }
    
                            callback(null, errorcodes.NoError, missionsArray);
                        }
                    });
                }
            });
        }
        else
        {
            players.get_all_players_statistics(options, (error, errorcode, results) => {
                if (error) {
                    callback("error getting last month leaderboard (api.players.check_leaderboard_prizes)", errorcodes.GeneralError);
                    return;
                }

                var missionConfirmValues = [];
                for (var i = 0; i < leaderboardMissionIds.length; i++)
                {
                    missionConfirmValues[leaderboardMissionIds[i]] = false;
                }
    
                var missionCheckId = -1;

                var months = getPreviousLeaderboardDateStrings();
                    
                results.sort(function(a, b) {
                    if(a.scores === undefined) a.scores = {};
                    if(a.scores[months.date1] === undefined) a.scores[months.date1] = -1;
                    if(a.scores[months.date2] === undefined) a.scores[months.date2] = -1;
                    if(b.scores === undefined) b.scores = {};
                    if(b.scores[months.date1] === undefined) b.scores[months.date1] = -1;
                    if(b.scores[months.date2] === undefined) b.scores[months.date2] = -1;

                    var aScore = a.scores[months.date1] + a.scores[months.date2];
                    var bScore = b.scores[months.date1] + b.scores[months.date2];

                    if(a.id == options.id) console.log(aScore);

                    return bScore - aScore;
                });
    
                // for (var i = 0; i < 100; i++) {
                //     missionConfirmValues[leaderboardMissionIds[i]] = filtered[i].id === options.playerId;
                //     if (filtered[i].id === options.playerId) {
                //         missionCheckId = leaderboardMissionIds[i];
                //     }
                // }
    
                //first place mission
                if(results[0].id === options.playerId && (results[0].scores[months.date1] + results[0].scores[months.date2]) >= 0)
                    missionConfirmValues[leaderboardMissionIds[0]] = true;

                
    
                //top 100 mission
                for (var i = 0; i < results.length && i < 100; i++)
                {
                    if((results[i].scores[months.date1] + results[i].scores[months.date2]) >= 0)
                    {
                        missionConfirmValues[leaderboardMissionIds[1]] = results[i].id === options.playerId;
                        if (results[i].id === options.playerId) {
                            missionCheckId = leaderboardMissionIds[1];
                            break;
                        }
                    }                    
                    
                }
    
                var missionsArray = [];
    
                // TODO: Make dictionaries valid in client side, not just turn this into an Array
                for (var key in missionConfirmValues) {
                    var newObject = {
                        "missionId": key,
                        "isAvailable": missionConfirmValues[key]
                    };
                    missionsArray.push(newObject);
                }
    
                if (missionCheckId === -1) {
                    callback(null, errorcodes.NoError, missionsArray);
                } else {
                    // check redeems
                    var query2 = {
                        filter: {
                            "missionId": missionCheckId
                        },
                        safe: true
                    };
    
                    db.bicaserver.redeems.get(query2, function(error2, results2) {
    
                        if (error2) {
                            console.log(error2);
                            callback("error getting redeems data from database (api.players.check_leaderboard_prizes:25)", errorcodes.GeneralError, false);
                            return;
                        }
    
                        if (results2.length === 0) {
                            callback(null, errorcodes.NoError, missionsArray);
                        } else {
                            var redeems = results2[0];
                            for (var i = 0; i < redeems.redeemData.length; i++) {
                                var currentUser = redeems.redeemData[i];
    
                                var utctime = new Date(datetime.utcnow.getTime());
                                var month;
                                if (getDateMonthString() % 2 == 0)
                                {
                                    console.log(utctime.getMonth()+1);
                                    utctime.setMonth(utctime.getMonth() - 2);
                                    
                                    console.log("second month");
                                    month = utctime.getMonth()+1;
                                    console.log(month);
                                }
                                else
                                {
                                    console.log(utctime.getMonth()+1);

                                    utctime.setMonth(utctime.getMonth()-1);
                                    month = utctime.getMonth()+1;
                                    console.log(month);
                                }
                                
                                for (var j = 0; j < missionsArray.length; j++) {
                                    console.log(missionsArray[j].isAvailable);
                                }
    
                                if (currentUser.playerId === options.playerId && currentUser.leaderboardMonth === (utctime.getMonth() + 1) && currentUser.leaderboardYear === utctime.getFullYear()) {
    
                                    for (var j = 0; j < missionsArray.length; j++) {
                                        missionsArray[j].isAvailable = false;
                                    }
                                    break;
                                }
                            }
    
                            callback(null, errorcodes.NoError, missionsArray);
                        }
                    });
                }
            });
        }

    },

    redeem_leaderboard_prize: function(options, callback) {

        if (options.playerId === undefined) {
            callback("no player id found (api.players.redeem_leaderboard_prize:25)", errorcodes.GeneralError, false);
            return;
        }

        players.get_leaderboard_lastmonth_top3(options, (error, errorcode, results) => {
            if (error) {
                callback("error getting last month leaderboard (api.players.redeem_leaderboard_prize)", errorcodes.GeneralError, false);
                return;
            }

            var isPositionValid = false;

            var index = leaderboardMissionIds.indexOf(options.missionId);
            if (index === -1) {
                callback("missionId is not one of the leaderboard mission ids (api.players.redeem_leaderboard_prize)", errorcodes.GeneralError, false);
                return;
            }

            var placementResult = results[index];
            if (placementResult.id !== options.playerId) {
                callback("this player is not on the " + (index + 1) + " position (api.players.redeem_leaderboard_prize)", errorcodes.GeneralError, false);
                return;
            }

            // check redeems
            var query2 = {
                filter: {
                    "missionId": options.missionId
                },
                safe: true
            };

            db.bicaserver.redeems.get(query2, function(error2, results2) {

                if (error2) {
                    console.log(error2);
                    callback("error getting redeems data from database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                    return;
                }

                var utctime = new Date(datetime.utcnow.getTime());
                utctime.setMonth(utctime.getMonth() - 1);

                if (results2.length === 0) {
                    var newRedeemData = {};
                    newRedeemData["missionId"] = options.missionId;

                    var redeemInnerData = [];
                    var currentRedeemData = {};

                    currentRedeemData["playerId"] = options.playerId;
                    currentRedeemData["geoLocation"] = options.geoLocation;
                    currentRedeemData["leaderboardMonth"] = (utctime.getMonth() + 1);
                    currentRedeemData["leaderboardYear"] = utctime.getFullYear();
                    currentRedeemData["promoterName"] = options.promoterName;
                    currentRedeemData["timestamp"] = datetime.now;

                    redeemInnerData.push(currentRedeemData);

                    newRedeemData["redeemData"] = redeemInnerData;

                    var insertQuery = {
                        doc: newRedeemData,
                        safe: true
                    };

                    db.bicaserver.redeems.insert(insertQuery, function(error3, items) {
                        if (error3) {
                            callback("error inserting redeems data on database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                            return;
                        }

                        callback(null, errorcodes.NoError, true);
                    });
                } else {
                    var codeRedeems = results2[0];
                    for (var i = 0; i < codeRedeems.redeemData.length; i++) {
                        var currentUser = codeRedeems.redeemData[i];
                        if (currentUser.playerId === options.playerId && currentUser.leaderboardMonth === (utctime.getMonth() + 1) && currentUser.leaderboardYear === utctime.getFullYear()) {
                            callback("error player already redeemed (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                            return;
                        }
                    }

                    var currentRedeemData = {};

                    currentRedeemData["playerId"] = options.playerId;
                    currentRedeemData["geoLocation"] = options.geoLocation;
                    currentRedeemData["leaderboardMonth"] = (utctime.getMonth() + 1);
                    currentRedeemData["leaderboardYear"] = utctime.getFullYear();
                    currentRedeemData["promoterName"] = options.promoterName;
                    currentRedeemData["timestamp"] = datetime.now;

                    codeRedeems["redeemData"].push(currentRedeemData);

                    var updateQuery = {
                        filter: {
                            "missionId": options.missionId
                        },
                        doc: codeRedeems,
                        safe: true
                    };

                    db.bicaserver.redeems.update(updateQuery, function(error4, items) {
                        if (error4) {
                            console.log(error4);
                            callback("error updating redeems data on database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                            return;
                        }

                        callback(null, errorcodes.NoError, true);
                    });
                }
            });
        });
    },

    get_daily_chest: function(options, callback) {

        if (options.id === undefined) {
            callback("no player id found (api.players.get_daily_chest:965)", errorcodes.GeneralError, false);
            return;
        }

        date = new Date(datetime.utcnow.getTime());

        var str = "" + options.id + date.getDate() + date.getMonth() + date.getFullYear();
        var seed = parseInt(str);
        var rng = seedrandom(seed);
        var sameRandom = rng();
        var randomChest = Math.floor(sameRandom * _NUM_CHESTS);

        var callbackInfo = {};
        callbackInfo["playerId"] = options.id;
        callbackInfo["chestIndex"] = randomChest;
        callback(null, errorcodes.NoError, callbackInfo);

        return;
    }
};

function executeUpdate() {
    players.update_leaderboard(null, updateLeaderboard);
}

function updateLeaderboard(errorMessage, errorCode, results) {
    leaderboard = [];
    leaderboardDebug = [];
    if (results === undefined) {
        console.log("Error getting leaderboard from database");
        return;
    }
    if (results[0] != null && results[0] != undefined) {
        var position = 0;
        var debug_position = 0;
        
        var months = getCurrentLeaderboardDateStrings();

        results.sort(function(a, b) {
            if(a.scores === undefined) a.scores = {};
            if(a.scores[months.date1] === undefined) a.scores[months.date1] = 0;
            if(a.scores[months.date2] === undefined) a.scores[months.date2] = 0;
            if(b.scores === undefined) b.scores = {};
            if(b.scores[months.date1] === undefined) b.scores[months.date1] = 0;
            if(b.scores[months.date2] === undefined) b.scores[months.date2] = 0;

            var aScore = a.scores[months.date1] + a.scores[months.date2];
            var bScore = b.scores[months.date1] + b.scores[months.date2];

            return bScore - aScore;
        });

        results.forEach(player => {
            var playerLB = new Object();
            playerLB.id = player.id;
            playerLB.name = player.name;
            playerLB.district = player.districtName;
            playerLB.region = player.regionName;

            //score
            var months = getCurrentLeaderboardDateStrings();

            if(playerLB.scores === undefined) playerLB.scores = {};
            if(player.scores === undefined) player.scores = {};

            if (getDateMonthString() % 2 == 0)
            {
                if(playerLB.scores[months.date1] === undefined) playerLB.scores[months.date1] = 0;
                if(playerLB.scores[months.date2] === undefined) playerLB.scores[months.date2] = 0;
                playerLB.score = player.scores[months.date1] + player.scores[months.date2];
            }
            else
            {
                if(playerLB.scores[months.date2] === undefined) playerLB.scores[months.date2] = 0;
                playerLB.score = player.scores[months.date2];
            }
            
            if (player.debug_user != undefined && player.debug_user === false)
            {
                position++;
                playerLB.position = position;
                leaderboard.push(playerLB);
            }
            else
            {
                debug_position++;
                playerLB.position = debug_position;
                leaderboardDebug.push(playerLB);
            }



            // console.log(playerLB);
            // console.log("--------------");
        });

        //console.log("LEADERBOARD -------------------------------");
        //console.log(leaderboard, "");
    }
}

//30 mins * 60000
//var interval = 60 * 60000;
//var cr = setInterval(executeUpdate, interval);
//executeUpdate();

function executeMarkersUpdate() {
    markers = new Array();

    fs.readFile(__dirname + '/../../../markers.json', 'utf8', (err, jsonString) => {
        if (err) {
            console.log("File read failed:", err);
            return;
        }

        try {
            const file = JSON.parse(jsonString);
            var json = file.markers;

            json.forEach(element => {
                if (markers.some(e => e.id === element.id)) {
                    // console.log("Marker ID:" + element.id + " already present");
                } else
                    markers.push(element);
            });

            markers.forEach(function(item, index, object) {
                if (json.some(e => e.id === item.id)) {
                    // console.log("Validated " + marker.id);
                } else
                    object.splice(index, 1);
            });

            // markers.forEach(marker => {
            //     console.log(marker.id);
            // });

        } catch (err) {
            console.log('Error parsing JSON string:', err)
        }

    });

    // players.update_markers(null, updateMarkers);
}

//24 hours * 60000
// var intervalMarkers = (24 * 60) * 60000;
//var intervalMarkers = 30 * 60000;
//var cr = setInterval(executeMarkersUpdate, intervalMarkers);

//executeMarkersUpdate();

// Nicknames
function generateAllNicknames() {

    var prefixNicknames = new Array();
    var girlFunctionNicknames = new Array();
    var boyFunctionNicknames = new Array();
    var recipeNicknames = new Array();

    fs.readFile(__dirname + '/../../../nicknames.csv', 'utf8', (err, jsonString) => {
        if (err) {
            console.log("File read failed:", err);
            return;
        }

        try {

            var fileContentLines = jsonString.split(/r\n|\n/);
            // Ignore first line
            for (var i = 1; i < fileContentLines.length; i++) {
                var splitLine = fileContentLines[i].split(',');
                if (splitLine.length !== 4) {
                    console.log("WARNING: CSV file not well formatted");
                }

                if (splitLine[0] !== undefined && splitLine[0] !== "") {
                    prefixNicknames.push(splitLine[0]);
                }
                if (splitLine[1] !== undefined && splitLine[1] !== "") {
                    girlFunctionNicknames.push(splitLine[1]);
                }
                if (splitLine[2] !== undefined && splitLine[2] !== "") {
                    boyFunctionNicknames.push(splitLine[2]);
                }
                if (splitLine[3] !== undefined && splitLine[3] !== "") {
                    recipeNicknames.push(splitLine[3]);
                }
            }

            for (var i = 0; i < prefixNicknames.length; i++) {
                var currentPrefix = prefixNicknames[i];
                for (var j = 0; j < girlFunctionNicknames.length; j++) {
                    var currentFunction = girlFunctionNicknames[j];
                    for (var k = 0; k < recipeNicknames.length; k++) {
                        var currentRecipe = recipeNicknames[k];
                        girlNicknames.push(currentPrefix + " " + currentFunction + " " + currentRecipe);
                    }
                }

                for (var j = 0; j < boyFunctionNicknames.length; j++) {
                    var currentFunction = boyFunctionNicknames[j];
                    for (var k = 0; k < recipeNicknames.length; k++) {
                        var currentRecipe = recipeNicknames[k];
                        boyNicknames.push(currentPrefix + " " + currentFunction + " " + currentRecipe);
                    }
                }
            }

            totalBoyNicknamesCount = boyNicknames.length;
            totalGirlNicknamesCount = girlNicknames.length;

            var query = {
                aggregate: [{
                    $project: {
                        "name": "$name"
                    }
                }, ],
                safe: true
            };

            db.bicaserver.players.aggregate(query, function(error, results) {
                for (var i = 0; i < results.length; i++) {
                    var arrayIndex = boyNicknames.indexOf(results[i].name);
                    if (arrayIndex > -1) {
                        boyNicknames.splice(arrayIndex, 1);
                    }

                    arrayIndex = girlNicknames.indexOf(results[i].name);
                    if (arrayIndex > -1) {
                        girlNicknames.splice(arrayIndex, 1);
                    }
                }

                console.log("Girl nicknames count: " + girlNicknames.length);
                console.log("Boy nicknames count: " + boyNicknames.length);

                executeNicknamesUpdate();
            });

        } catch (err) {
            console.log('Error parsing CSV string:', err)
        }

    });
}



//24 hours = 86400 seconds
var intervalNicknames = 86400000;
var maxNoLoginTime = 86400000 * 30;

function executeNicknamesUpdate() {

    sentEmailToday = false;

    var lastMonthUtc = new Date(datetime.utcnow.getTime());
    lastMonthUtc.setMonth(lastMonthUtc.getMonth() - 3); //get the day -3 months which = 90 days

    var timestamp = lastMonthUtc.getTime() / 1000;

    var query = {
        filter: {
            "lastlogin": { "$lte": timestamp },
            "name": { "$ne": "" }
        },
        safe: true
    }
    db.bicaserver.players.get(query, function(error, results) {
        if (error) {
            console.log("Error getting players on executeNicknamesUpdate: " + error);
            return;
        }




        var allPlayerIds = new Array();
        for (var i = 0; i < results.length; i++) {
            allPlayerIds.push(results[i].id);
        }

        var query2 = {
            filter: {
                "id": { "$in": allPlayerIds }
            },
            doc: {
                "$set": { "name": "" }
            },
            multi: true,
            safe: true
        };

        db.bicaserver.players.update(query2, function(error2, results2) {
            if (error2) {
                console.log("Error setting player names on executeNicknamesUpdate: " + error2);
                return;
            }

            console.log("Player nicknames deleted successfully -- " + results.length);
            
            for (var i = 0; i < results.length; i++) {
                if (results[i].gender === 1) {
                    boyNicknames.push(results[i].name);
                } else {
                    girlNicknames.push(results[i].name);
                }
            }

            console.log("Updated local nicknames list");
        });
    });

}


//var cn = setInterval(executeNicknamesUpdate, intervalNicknames);

//generateAllNicknames();



// DELETE USERS
function deleteEmptyUsers() {
    var query = {
        filter: {
            "id": { "$exists": false }
        },
        safe: true
    };



    db.bicaserver.players.get(query, function(error, results) {
        if (error) {
            console.log("ERROR Getting players to delete: " + error);
            return;
        }

        console.log("Users to delete: " + results.length);

        for (var i = 0; i < results.length; i++) {
            console.log(results[i], "");


        }

        var query2 = {
            filter: {
                "id": { "$exists": false }
            },
            safe: true
        }

        db.bicaserver.players.remove(query2, function(error2, results2) {
            if (error2) {
                console.log("ERROR Deleting players: " + error2);
            }
        });
    });
}
//deleteEmptyUsers();

// Region Markers
function generateRegionMarkers()
{
	// Example
	// var cMarker = new Object();
	// cMarker.regionName = "Águeda";
	// cMarker.regionId = 1;
	// cMarker.latitude = 38.000;
	// cMarker.longitude = 7.000;

	c111 = [];
	
	 fs.readFile(__dirname + '/../../../regionMarkers.csv', 'utf8', (err, jsonString) =>
	 {
        if (err)
		{
            console.log("File read failed:", err);
            return;
        }

        try
		{
			var fileContentLines = jsonString.split(/r\n|\n/);
			// Ignore first line (i = 1)
			for(var i = 1; i < fileContentLines.length; i++)
			{	
				var cMarker = new Object();
				var splitLine = fileContentLines[i].split(',');
				
				if(splitLine[0] !== undefined && splitLine[0] !== "")
					cMarker.regionName = splitLine[0];
				
				if(splitLine[1] !== undefined && splitLine[1] !== "")
					cMarker.regionId = splitLine[1];
				
				for(var j = 2; j < splitLine.length; j++)
				{
					if(splitLine[j] !== undefined && splitLine[j] !== "")
					{
						var coordinates = splitLine[j].split(';');
						cMarker.latitude = parseFloat(coordinates[0]);
						cMarker.longitude = parseFloat(coordinates[1]);
						const clone1 = Object.assign({}, cMarker);
						c111.push(clone1);
					}
				}
			}
        }
		catch (err)
		{
            console.log('Error parsing CSV string:', err)
        }
    });
}

// ALDI Markers
function generateAldiMarkers()
{
	// Example
	// var cMarker = new Object();
	// cMarker.storeName = "Águeda";
	// cMarker.storeId = 1;
	// cMarker.latitude = 38.000;
	// cMarker.longitude = 7.000;

	aldi = [];
	
	 fs.readFile(__dirname + '/../../../aldi.csv', 'utf8', (err, jsonString) =>
	 {
        if (err)
		{
            console.log("File read failed:", err);
            return;
        }

        try
		{
			var fileContentLines = jsonString.split(/r\n|\n/);
			// Ignore first line (i = 1)
			for(var i = 1; i < fileContentLines.length; i++)
			{	
				var cMarker = new Object();
				var splitLine = fileContentLines[i].split(',');
				
				if(splitLine[0] !== undefined && splitLine[0] !== "")
					cMarker.storeName = splitLine[0];
				
				if(splitLine[1] !== undefined && splitLine[1] !== "")
					cMarker.storeId = splitLine[1];
				
                if(splitLine[2] !== undefined && splitLine[2] !== "")
				{
                    var coordinates = splitLine[2].split(';');
						cMarker.latitude = parseFloat(coordinates[0]);
						cMarker.longitude = parseFloat(coordinates[1]);
						const clone1 = Object.assign({}, cMarker);
						aldi.push(clone1);
                }
				
			}
        }
		catch (err)
		{
            console.log('Error parsing CSV string:', err)
        }
    });
}

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:::                                                                         :::
//:::  This routine calculates the distance between two points (given the     :::
//:::  latitude/longitude of those points). It is being used to calculate     :::
//:::  the distance between two locations using GeoDataSource (TM) prodducts  :::
//:::                                                                         :::
//:::  Definitions:                                                           :::
//:::    South latitudes are negative, east longitudes are positive           :::
//:::                                                                         :::
//:::  Passed to function:                                                    :::
//:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
//:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
//:::    unit = the unit you desire for results                               :::
//:::           where: 'M' is statute miles (default)                         :::
//:::                  'K' is kilometers                                      :::
//:::                  'N' is nautical miles                                  :::
//:::                                                                         :::
//:::  Worldwide cities and other features databases with latitude longitude  :::
//:::  are available at https://www.geodatasource.com                         :::
//:::                                                                         :::
//:::  For enquiries, please contact sales@geodatasource.com                  :::
//:::                                                                         :::
//:::  Official Web site: https://www.geodatasource.com                       :::
//:::                                                                         :::
//:::               GeoDataSource.com (C) All Rights Reserved 2022            :::
//:::                                                                         :::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

function Distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    } else {
        var radlat1 = Math.PI * lat1 / 180;
        var radlat2 = Math.PI * lat2 / 180;
        var theta = lon1 - lon2;
        var radtheta = Math.PI * theta / 180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit == "K") { dist = dist * 1.609344 }
        if (unit == "N") { dist = dist * 0.8684 }
        return dist;
    }
}

const maxRadius = 0.1; //100 meters | 0.1 Km

function CheckMarkerRegionGPSDistance(id, latitude, longitude)
{
	//console.log("Check Distance ---------------------------------------");
	var success = false;
	//var regionId = 0;
	//var storeId = 0;
	var geoLocation = 0;
    var closestDistance = 10000000000;
	switch (id)
	{
		case 'c111':
			c111.every(element => {
				var distance = Distance(element.latitude, element.longitude, latitude, longitude, 'K');
				if(distance <= maxRadius)
				{
                    console.log('Found c111 in ', element.regionName);
					//regionId = element.regionId;
					// geoLocation = element.latitude + ";" + element.longitude;
                    if(closestDistance > distance)
                    {
                        geoLocation = element.latitude + ";" + element.longitude;
                        closestDistance = distance;
                        success = true;
                    }
					// Make sure you return true. If you don't return a value, `every()` will stop.
  					// return false;
  					return true;
				}
				//else console.log("latitude: " + latitude + "; longitude: " + longitude + " != " + "element.latitude: " + element.latitude + "; element.longitude: " + element.longitude);
				return true;
			});
			// return { success, regionId, geoLocation };
			return { success, geoLocation };
        case 'aldi':
            aldi.every(element => {
                var distance = Distance(element.latitude, element.longitude, latitude, longitude, 'K');
                if(distance <= maxRadius)
				{
                    console.log('Found c111 in ', element.regionName);
					//regionId = element.regionId;
					// geoLocation = element.latitude + ";" + element.longitude;
                    if(closestDistance > distance)
                    {
                        geoLocation = element.latitude + ";" + element.longitude;
                        closestDistance = distance;
                        success = true;
                    }
					// Make sure you return true. If you don't return a value, `every()` will stop.
  					// return false;
				}
				//else console.log("latitude: " + latitude + "; longitude: " + longitude + " != " + "element.latitude: " + element.latitude + "; element.longitude: " + element.longitude);
				return true;
            });
            // return { success, regionId, geoLocation };
            return { success, geoLocation };
			
		default:
			console.log('Not close to any location, ', id);
			// return { success, regionId, geoLocation };
			return { success, geoLocation };
	}
}

//generateRegionMarkers();
//generateAldiMarkers();

function clearScore()
{
    players.get_all_players(null, updatePlayerScore);
}

function updatePlayerScore(errorMessage, errorCode, results)
{
    if (results === undefined)
	{
        console.log("Error getting players from database");
        return;
    }

    //results = get_all_players

    if (results[0] != null && results[0] != undefined)
	{
        var dateMonthYearString = getDateMonthYearString();
        var prevDateMonthYearString = getPreviousDateMonthYearString();

        if (getDateMonthString() % 2 != 0)
        {

            var allPlayerIds = new Array();
            for (var i = 0; i < results.length; i++)
			{
                allPlayerIds.push(results[i].id);
            }

            var query2 =
			{
                filter: {
                    "id": { "$in": allPlayerIds }
                },
                doc: {
                    "$set": { "score": 0 }
                },
                multi: true,
                safe: true
            };
            //Update on the database
            db.bicaserver.players.update(query2, function(error2, players2)
			{
                if (error2)
				{
                    console.log("error updatating player login date (api.players.get_player:28)", errorcodes.GeneralError);
                }
                else
                {
                    // executeUpdate(); //leaderboard
                    console.log("UPDATED_______________________________");
                }
            });
        }

        
        console.log("UPDATE LEADERBOARD_______________________________");
        executeUpdate(); //leaderboard
        
    }
}


// schedule.scheduleJob('* * * * *', function(){
//schedule.scheduleJob('@monthly', function(){
//    clearScore();
//}); // run every month
