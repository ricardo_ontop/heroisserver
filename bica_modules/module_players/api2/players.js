var basefolder = __dirname + "/../../../bica-game-server/api/",
    config = require(basefolder + "/config.js"),
    db = require(basefolder + "/database.js"),
    mongodb = require(basefolder + "../node_modules/mongodb"),
    md5 = require(basefolder + "/md5.js"),
    utils = require(basefolder + "/utils.js"),
    date = utils.fromTimestamp,
    datetime = require(basefolder + "/datetime.js"),
    fs = require('fs'),
    api = require(basefolder + "/../api"),
    errorcodes = require(__dirname + "/errorcodes.js").errorcodes,
    readline = require('readline'),
    seedrandom = require('seedrandom'),
    nodemailer = require('nodemailer'),
    { google } = require('googleapis'),
    keys = require("./client_secret.json"),
    schedule = require('node-schedule');



// Google Spreadsheets
const TOKEN_PATH = 'token.json';
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const EXCEL_UPDATE_INTERVAL = 43200000;

// Run at server startup
authorize(keys, saveUserData);
setInterval(function() {
    authorize(keys, saveUserData);
}, EXCEL_UPDATE_INTERVAL);

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oauth2client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getNewToken(oauth2client, callback);
        oauth2client.setCredentials(JSON.parse(token));
        callback(oauth2client);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error while trying to retrieve access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

/**
 * Prints the names and majors of students in a sample spreadsheet:
 * @see https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function saveUserData(auth) {
    const sheets = google.sheets({ version: 'v4', auth });
    // Update Stats
    players.get_all_players_statistics(null, function(error, errorcodes, results) {
        var avgAge = 0;
        var avgAgeM = 0;
        var avgAgeF = 0;
        var pctGenderM = 0;
        var pctGenderF = 0;
        var totalM = 0;
        var totalF = 0;
        var totalIsParticipating = 0;
        var totalHasParticipatedLastYear = 0;
        var avgIsParticipating = 0;
        var avgHasParticipatedLastYear = 0;

        var users = [
            ["ID", "Nome", "Género", "Idade", "Distrito", "Região", "Escola", "Turma a participar", "Turma já participou", "Pontuação"]
        ];

        if (results === undefined) {
            console.log("ERROR: Error getting user statistics.");
            return;
        }
        
        for (var i = 0; i < results.length; i++) {
            var currentUser = results[i];

            if (currentUser.debug_user === undefined || currentUser.debug_user === true) continue;

            if (currentUser.birthdayYear !== undefined) {
                var userBirthdayDate = new Date(results[i].birthdayYear, results[i].birthdayMonth - 1, results[i].birthdayDay);
                var today = new Date(datetime.utcnow);
                var age = today.getFullYear() - userBirthdayDate.getFullYear();

                var m = today.getMonth() - userBirthdayDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < userBirthdayDate.getDate())) {
                    age--;
                }

                if (age < 0) {
                    age = 0;
                }

                avgAge += age;
                currentUser["age"] = age;
            }

            if (currentUser.gender !== undefined) {
                if (currentUser.gender === 1) {
                    // M
                    totalM++;
                    avgAgeM += currentUser["age"];
                    currentUser["gender"] = "M";
                } else if (currentUser.gender === 2) {
                    // F
                    totalF++;
                    avgAgeF += currentUser["age"];
                    currentUser["gender"] = "F";
                }
            }

            // Failsafes
            if (currentUser["id"] === undefined) {
                currentUser["id"] = -1;
            }

            if (currentUser["name"] === undefined) {
                currentUser["name"] = "";
            }

            if (currentUser["gender"] === undefined) {
                currentUser["gender"] = "";
            }

            if (currentUser["age"] === undefined) {
                currentUser["age"] = -1;
            }

            if (currentUser["districtName"] === undefined) {
                currentUser["districtName"] = "";
            }

            if (currentUser["regionName"] === undefined) {
                currentUser["regionName"] = "";
            }

            if (currentUser["schoolName"] === undefined) {
                currentUser["schoolName"] = "";
            }

            if (currentUser["classIsParticipating"] === undefined) {
                currentUser["classIsParticipating"] = false;
            } else {
                if (currentUser["classIsParticipating"] === true) {
                    totalIsParticipating++;
                }
                currentUser["classIsParticipating"] = currentUser["classIsParticipating"] === true ? "Sim" : "Não";

            }

            if (currentUser["lastYearParticipated"] === undefined) {
                currentUser["lastYearParticipated"] = false;
            } else {
                if (currentUser["lastYearParticipated"] === true) {
                    totalHasParticipatedLastYear++;
                }
                currentUser["lastYearParticipated"] = currentUser["lastYearParticipated"] === true ? "Sim" : "Não";

            }

            if (currentUser["score"] === undefined) {
                currentUser["score"] = -1;
            }

            var newField = [currentUser["id"], currentUser["name"], currentUser["gender"], currentUser["age"], currentUser["districtName"], currentUser["regionName"], currentUser["schoolName"], currentUser["classIsParticipating"], currentUser["lastYearParticipated"], currentUser["score"]];
            users.push(newField);
        }

        avgAge /= results.length;
        avgAge = avgAge.toFixed(2);

        avgAgeM /= totalM;
        avgAgeM = avgAgeM.toFixed(2);

        avgAgeF /= totalF;
        avgAgeF = avgAgeF.toFixed(2);

        pctGenderM = totalM / results.length;
        pctGenderM *= 100;
        pctGenderM = pctGenderM.toFixed(2);
        pctGenderF = totalF / results.length;
        pctGenderF *= 100;
        pctGenderF = pctGenderF.toFixed(2);

        avgIsParticipating = parseFloat(totalIsParticipating) / results.length * 100;
        avgIsParticipating = avgIsParticipating.toFixed(2);

        avgHasParticipatedLastYear = parseFloat(totalHasParticipatedLastYear) / results.length * 100;
        avgHasParticipatedLastYear = avgHasParticipatedLastYear.toFixed(2);

        sheets.spreadsheets.values.clear({
            spreadsheetId: '1WCSFulVReUZ1Bs0RTJYh5CdVW5q-M_Hyq2AjwGrxQY0',
            range: 'Jogadores!A:J',
		range: 'Jogadores!A:J', 
            range: 'Jogadores!A:J',
        }, (err, res) => {
            if (err) return console.log('The API returned an error: ' + err);
            console.log("Users cleared!");


            // Update User List
            sheets.spreadsheets.values.update({
                spreadsheetId: '1WCSFulVReUZ1Bs0RTJYh5CdVW5q-M_Hyq2AjwGrxQY0',
                range: 'Jogadores!A:J',
                valueInputOption: 'USER_ENTERED',
                resource: {
                    values: users
                },
            }, (err2, res2) => {
                if (err2) return console.log('The API returned an error: ' + err2);
                console.log("Saved users successfully!");
            });
        });


        // Update statistics
        sheets.spreadsheets.values.update({
            spreadsheetId: '1WCSFulVReUZ1Bs0RTJYh5CdVW5q-M_Hyq2AjwGrxQY0',
            range: 'Estatísticas!A:B',
            valueInputOption: 'USER_ENTERED',
            resource: {
                values: [
                    ["Total jogadores", results.length],
                    ["Média Idades", avgAge.toString()],
                    ["Média Idades M", avgAgeM.toString()],
                    ["Média Idades F", avgAgeF.toString()],
                    ["Percentagem Jogadores M", pctGenderM.toString()],
                    ["Percentagem Jogadores F", pctGenderF.toString()],
                    ["Percentagem Turma A Participar", avgIsParticipating.toString()],
                    ["Percentagem Participou Ano PAssado", avgHasParticipatedLastYear.toString()]
                ]
            },
        }, (err, res) => {
            if (err) return console.log('The API returned an error: ' + err);
            console.log("Saved statistics successfully!");
        });

    });
}

// ----------------------------------------------------------------------------


// Consts
const _MAX_SCORE_PER_REQUEST = 100000;
const CAMPAIGN_PREFIX = 2000;
//const leaderboardMissionIds = [];
const leaderboardMissionIds = [104, 105, 106]; // 101 - first place national || 102 - 1-100th place || 104 - top3 national || 105 - top3 district || 106 - top3 regional
const campaignMissionIds = [2001, 2002]; //1 - top 3 || 2 - 4-100th place
//Campaign list
const campaigns = [
    {
        campaignId : 1,
        campaignName : "TOP Maçã",
        campaignFullName : "Maçã de Alcobaça",

        // startTime : new Date("2022-09-07T12:55:55.000Z"), //? testing purposes
        // endTime : new Date("2022-12-21T23:59:59.000Z"), //* the month is 0-indexed
        // redeemTime : new Date("2023-02-01T00:00:01.000Z"),

        startTime : new Date("2022-09-20T22:59:55.000+00:00"),
        endTime : new Date("2022-12-21T23:59:55.000+00:00"),
        redeemTime : new Date("2023-12-01T00:00:01.000+00:00")
    },
    // {
    //     campaignId : 2,
    //     campaignName : "Fake",
    //     campaignFullName : "Fake",
    //     startTime : new Date(2022, 6, 21),
    //     endTime : new Date(2023, 5, 21),
    //     redeemTime : new Date(2023, 10, 25)
    // },
];
const _NUM_CHESTS = 5;
const _NICKNAME_THRESHOLD_FOR_EMAIL = 0.3;
const VERSION = 6;

const leaderboardSettings = [
    {
        leaderboardId: 104,
        startTime : new Date("2022-07-01T00:00:05.000+00:00")
        // TODO: Also set time interval
        // TODO: Add active start time
    },
    {
        leaderboardId: 105,
        startTime : new Date("2022-07-01T00:00:05.000+00:00")
        // TODO: Also set time interval
         // TODO: Add active start time
    },
    {
        leaderboardId: 106,
        startTime : new Date("2022-07-01T00:00:05.000+00:00")
        // TODO: Also set time interval
        // TODO: Add active start time
    }
];

var campaign;
//Markers
var markers = new Array();
var aldi = new Array();
var c111 = new Array();
var boyNicknames = new Array();
var girlNicknames = new Array();
var totalBoyNicknamesCount = 0;
var totalGirlNicknamesCount = 0;


//Bi-Monthly Leaderboard
var leaderboardDebug = new Array();
var leaderboard = new Array();
var leaderboardDebugLast = new Array();
var leaderboardLast = new Array();
var leaderboardCampaign = new Array();
var leaderboardRange = 15; //half of the amount of players to send (excluding top 3 and player)
var leaderboardBots = [
    { id: -1,  name: "Super Capitã Tofu" , position: 1, score: 1000},
    { id: -2,  name: "Hiper Príncipe Risoto" , position: 2, score: 900},
    { id: -3,  name: "Mega Guerreira Ravioli" , position: 3, score: 800},
    { id: -4,  name: "Maxi Guardião Pesto" , position: 4, score: 700},
    { id: -5,  name: "Macro Tenente Gaspacho" , position: 5, score: 600},
    { id: -6,  name: "Inter Cadete Tártaro" , position: 6, score: 500},
    { id: -7,  name: "Supra Comandante Carbonara" , position: 7, score: 400},
    { id: -8,  name: "Meta Mestre Caprese" , position: 8, score: 300},
    { id: -9,  name: "Extra Ninja Ceviche" , position: 9, score: 200},
    { id: -10,  name: "Ultra Almirante Noodle" , position: 10, score: 100}
];

//Yearly leaderboard
var leaderboardStartMonth = 6; // July

const { debug, Console } = require('console');
// Emails
var nodemailer = require('nodemailer');
const { version } = require('os');
var sentEmailToday = false;

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'development.ontop@gmail.com',
        pass: 'lzalemgivzxwxlbh'
    }
});

var mailOptions = {
    from: 'development.ontop@gmail.com',
    to: 'development.ontop@gmail.com',
    subject: '[HeroisDaFruta] WARNING: Nicknames available under 30%',
    text: 'Hi, one of the lists of boys/girl available nicknames is below 30%. Please consider adding more nickname variants to the CSV file and restart the server.'
};

function getDateMonthYearString() {
    var utc = new Date(datetime.utcnow.getTime());
    return (utc.getMonth() + 1) + "-" + utc.getFullYear();
}

function getPreviousDateMonthYearString() {
    var utc = new Date(datetime.utcnow.getTime());
    return (utc.getMonth()) + "-" + utc.getFullYear();
}

function getPreviousLeaderboardDateStrings() {
    var utc;
    var dates = [];
    
    var availableDates = [
        "3-2023",
        "4-2023",
        "5-2023",
        "6-2023"
    ];
    
    for (var i = 0; i < 12; i++) {
        utc = new Date(datetime.utcnow.getTime());
        if (utc.getMonth() < leaderboardStartMonth) {
            utc.setFullYear(utc.getFullYear() - 1);
        }
        utc.setMonth(i + leaderboardStartMonth);
        var dateStr = (utc.getMonth() + 1) + "-" + (utc.getFullYear() - 1);
        
        if (availableDates.indexOf(dateStr) > -1) {
            dates.push(dateStr);
        }
        
    }
    
    return dates;
}

function getCurrentLeaderboardDateStrings() {
    var utc;
    var dates = [];
    for (var i = 0; i < 12; i++) {
        utc = new Date(datetime.utcnow.getTime());
        if (utc.getMonth() < leaderboardStartMonth) {
            utc.setFullYear(utc.getFullYear() - 1);
        }
        utc.setMonth(i + leaderboardStartMonth);
        var dateStr = (utc.getMonth() + 1) + "-" + utc.getFullYear();
        dates.push(dateStr);
    }
    
    return dates;
}

function getDateMonthString() {
    var utc = new Date(datetime.utcnow.getTime());
    return (utc.getMonth() + 1);
}

function reorderLeaderboard() {
    var dateMonthYear = getDateMonthYearString();

    leaderboard.sort(function(a, b) {
        var aScore = a.score;
        var bScore = b.score;
        // Sort by score
        // If the first item has a higher number, move it up
        // If the first item has a lower number, move it down
        if (aScore < bScore) return 1;
        if (aScore > bScore) return -1;

        // If the score number is the same between both items, sort by timestamp
        // If the first score was achieved first, move it up
        // Otherwise move it down
        if (a.lastScore < b.lastScore) return 1;
        if (a.lastScore > b.lastScore) return -1;

        // return bScore - aScore;
        return 0;
    });
    
    var position = 0;

    leaderboard.forEach(playerLB => {
        position++;
        playerLB.position = position
    });

    leaderboardDebug.sort(function(a, b) {
        var aScore = a.score;
        var bScore = b.score;
        // Sort by score
        // If the first item has a higher number, move it up
        // If the first item has a lower number, move it down
        if (aScore < bScore) return 1;
        if (aScore > bScore) return -1;

        // If the score number is the same between both items, sort by timestamp
        // If the first score was achieved first, move it up
        // Otherwise move it down
        if (a.lastScore < b.lastScore) return 1;
        if (a.lastScore > b.lastScore) return -1;

        // return bScore - aScore;
        return 0;
    });

    position = 0;
    
    leaderboardDebug.forEach(playerLB => {
        position++;
        playerLB.position = position
    });

    leaderboardCampaign.sort(function(a, b) {
        var aScore = a.score;
        var bScore = b.score;
        // Sort by score
        // If the first item has a higher number, move it up
        // If the first item has a lower number, move it down
        if (aScore < bScore) return 1;
        if (aScore > bScore) return -1;

        // If the score number is the same between both items, sort by timestamp
        // If the first score was achieved first, move it up
        // Otherwise move it down
        if (a.lastScore < b.lastScore) return 1;
        if (a.lastScore > b.lastScore) return -1;

        // return bScore - aScore;
        return 0;
    });
    
    position = 0;

    leaderboardCampaign.forEach(playerLB => {
        position++;
        playerLB.position = position
    });
}



var players = module.exports = {

    // To be used internally only!
    update_leaderboard: function(options, callback) {

        // var sortDate = "scores." + getDateMonthYearString();
        var sortDate = "score";

        var sort = {};
        sort[sortDate] = -1;

        var query = {
            safe: true,
            filter: {"id": {"$exists":true}}
        };

        console.log("BEFORE QUERY GET LEADERBOARD");
        
        db.bicaserver.leaderboard.get(query, function(error, results) {
            if (error) {
                console.log(error);
                callback("error aggregating players from database", errorcodes.GeneralError);
                return;
            }

            console.log("COMPLETED QUERY GET LEADERBOARD");
            callback(null, errorcodes.NoError, results);
        });
    },

    get_version: function(options, callback) {

        if (options.gameVersion < VERSION) {
            callback("error: wrong app version", errorcodes.WrongAppVersion);
            return;
        }

        callback(null, errorcodes.NoError, options);
    },

    get_all_players: function(options, callback) {
        var query = {
            filter: {
                "id": { "$exists": true }
            },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, results) {
            if (error) {
                callback("error aggregating players from database (api.player.add_player:28)", errorcodes.GeneralError);
                return;
            }

            callback(null, errorcodes.NoError, results);
        });
    },

    get_all_players_statistics: function(options, callback) {
        var query = {
            filter: {
                "id": { "$exists": true },
                $and: [
                    { "debug_user": { "$exists": true } },
                    { "debug_user": false }
                ]
            },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, results) {
            if (error) {
                callback("error aggregating players from database (api.player.add_player:28)", errorcodes.GeneralError);
                return;
            }

            callback(null, errorcodes.NoError, results);
        });
    },

    get_all_players_debug: function(options, callback) {
        var query = {
            filter: {
                "id": { "$exists": true },
                $and: [
                    { "debug_user": { "$exists": true } },
                    { "debug_user": true }
                ]
            },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, results) {
            if (error) {
                callback("error aggregating players from database (api.player.add_player:28)", errorcodes.GeneralError);
                return;
            }

            callback(null, errorcodes.NoError, results);
        });
    },

    get_random_nickname: function(options, callback) {

        if (options.gender === undefined) {
            callback("error: no gender variable  (api.players.get_random_nickname:98)", errorcodes.GeneralError);
            return;
        }

        var player = {};

        var randomIndex = -1;
        var randomNickname = "";
        var foundNickname = false;

        if (options.gender === 1) {
            if (boyNicknames.length === 0) {
                callback("error: no more nicknames available for gender " + options.gender + "  (api.players.get_random_nickname:98)", errorcodes.GeneralError, false);
                return;
            }

            randomIndex = Math.floor(Math.random() * boyNicknames.length);
            randomNickname = boyNicknames[randomIndex];
            foundNickname = true;
        } else if (options.gender === 2) {
            if (girlNicknames.length === 0) {
                callback("error: no more nicknames available for gender " + options.gender + "  (api.players.get_random_nickname:98)", errorcodes.GeneralError, false);
                return;
            }

            randomIndex = Math.floor(Math.random() * girlNicknames.length);
            randomNickname = girlNicknames[randomIndex];
            foundNickname = true;
        } else {
            callback("error: no valid gender specified  (api.players.get_random_nickname:98)", errorcodes.GeneralError, false);
            return;
        }

        if (foundNickname && randomIndex > -1) {
            player["name"] = randomNickname;
            callback(null, errorcodes.NoError, player);
        } else {
            callback("error: no nickname found  (api.players.get_random_nickname:98)", errorcodes.GeneralError, false);
        }
    },

    add_player: function(options, callback) {

        options.score = 0;
    
        var query = {
            aggregate: [{
                    $sort: { id: -1 }
                },
                {
                    $limit: 1
                }
            ]
        };

        db.bicaserver.players.aggregate(query, function(error, results) {

            if (error) {
                callback("error aggregating players from database (api.player.add_player:28)", errorcodes.GeneralError);
                return;
            }

            if (results.length === 0) {
                options.id = 0;
            } else {
                options.id = results[0].id + 1;
            }
            options.timestamp = datetime.now;

            var newPlayer = {};
            newPlayer.id = options.id;
            newPlayer.name = options.name;
            newPlayer.score = options.score;
            newPlayer.scores = {};
            newPlayer.scores[getDateMonthYearString()] = options.score;
            newPlayer.gender = options.gender;
            newPlayer.birthdayDay = options.birthdayDay;
            newPlayer.birthdayMonth = options.birthdayMonth;
            newPlayer.birthdayYear = options.birthdayYear;
            newPlayer.districtName = options.districtName;
            newPlayer.regionName = options.regionName;
            newPlayer.schoolName = options.schoolName;
            newPlayer.classIsParticipating = options.classIsParticipating;
            newPlayer.lastYearParticipated = options.lastYearParticipated;

            var utc = new Date(datetime.utcnow.getTime());
            var dateTime = new Object();
            dateTime.year = utc.getFullYear();
            dateTime.month = utc.getMonth() + 1;
            dateTime.day = utc.getDate();
            dateTime.hour = 0;
            dateTime.minutes = 0;
            dateTime.seconds = 0;
            dateTime.miliseconds = 0;
            newPlayer.date = dateTime;

            if (options.debug_user != undefined && options.debug_user != null)
                newPlayer.debug_user = options.debug_user;

            if (options.cards === undefined || options.cards === null)
                newPlayer.cards = [];
            else
                newPlayer.cards = options.cards;

            if (options.cards === undefined || options.cards === null)
                newPlayer.markers = [];
            else
                newPlayer.markers = options.markers;


            var query2 = {
                doc: newPlayer,
                safe: true
            };

            db.bicaserver.players.insert(query2, function(error2, items) {

                if (error2) {
                    callback("error inserting player into database (api.player.add_player:43)", errorcodes.GeneralError);
                    return;
                }

                var boyNamesIndex = boyNicknames.indexOf(newPlayer.name);
                if (boyNamesIndex > -1) {
                    boyNicknames.splice(boyNamesIndex, 1);
                }

                var girlNamesIndex = girlNicknames.indexOf(newPlayer.name);
                if (girlNamesIndex > -1) {
                    girlNicknames.splice(girlNamesIndex, 1);
                }

                console.log("FINISHED: Added player");
                
                var lbPlayer = {};
                lbPlayer.id = newPlayer.id;
                lbPlayer.regionName = newPlayer.regionName;
                lbPlayer.districtName = newPlayer.districtName;
                lbPlayer.name = newPlayer.name;
                
                for (var i in newPlayer.scores) {
                    lbPlayer[i] = newPlayer.scores[i];
                }
                
                var lbQuery = {
                      doc: lbPlayer,
                      safe: true
                };
                
                 db.bicaserver.leaderboard.insert(lbQuery, function(error3, items2) {
                    
                     if (error3) {
                        callback("error inserting player into database (api.player.add_player:43)", errorcodes.GeneralError);
                        return;
                    }
                    
                    executeUpdate();
                    callback(null, errorcodes.NoError, newPlayer);
    
                    if (sentEmailToday === true) return;
    
                    var girlNicknamesPercentRemaining = girlNicknames.length / totalGirlNicknamesCount;
                    var boyNicknamesPercentRemaining = boyNicknames.length / totalBoyNicknamesCount;
    
                    if (girlNicknamesPercentRemaining <= _NICKNAME_THRESHOLD_FOR_EMAIL || boyNicknamesPercentRemaining <= _NICKNAME_THRESHOLD_FOR_EMAIL) {
                        console.log("ALERT: One of nicknames array is below treshold of " + (_NICKNAME_THRESHOLD_FOR_EMAIL * 100) + "%");
    
                        transporter.sendMail(mailOptions, function(errorm, info) {
                            if (errorm) {
                                console.log(errorm);
                            } else {
                                console.log('Email sent: ' + info.response);
                                sentEmailToday = true;
                            }
                        });
                    }
                    
                 });
            });
        });

    },

    get_player: function(player, callback) {
        console.log("RUNNING CODE ONE API2");
        if (player.id === null || player.id === undefined) {
            callback("error: no player id specified  (api.players.get_player:68)", errorcodes.GeneralError);
            return;
        }

        var query = {
            filter: { id: player.id },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players) {

            if (error) {
                callback("error getting player from database (api.players.get_player:28)", errorcodes.GeneralError);
                return;
            }

            if (players.length === 0) {
                callback("no player found (api.players.get_player:28)", errorcodes.GeneralError);
                return;
            }
            
            var player = players[0];
            var utc = new Date(datetime.utcnow.getTime());
            var dateTime = new Object();
            dateTime.year = utc.getFullYear();
            dateTime.month = utc.getMonth() + 1;
            dateTime.day = utc.getDate();
            dateTime.hour = 0;
            dateTime.minutes = 0;
            dateTime.seconds = 0;
            dateTime.miliseconds = 0;
            player.date = dateTime;
            callback(null, errorcodes.NoError, player);

            var lb = player.debug_user !== undefined && player.debug_user === true ? leaderboardDebug : leaderboard;
            
            var normalLB = lb.find(element => element.id === player.id);
            var campaignLB = {};
            if(campaign != null)
            {
                var today = new Date(datetime.utcnow);
                if(campaign.endTime > today)
                {
                    campaignLB = leaderboardCampaign.find(element => element.id === player.id);
                }
            }

            if(normalLB === undefined || campaignLB === undefined)
                executeUpdate();

            var doc = {
                "$set":
                {
                    "lastlogin":datetime.now
                }
            };

            var query2 = {
                filter: { id: players[0].id },
                doc: doc,
                safe: true
            };

            db.bicaserver.players.update(query2, function(error2, players2) {
                if (error2) {
                    console.log("error updatating player login date (api.players.get_player:28)", errorcodes.GeneralError);
                }
            });
        });
    },

    update_player: function(player, callback) {

        if (player.id === null || player.id === undefined) {
            callback("error: no player id specified  (api.players.update_player:68)", errorcodes.GeneralError);
            return;
        }

        var query = {
            filter: { id: player.id },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players) {

            if (error) {
                console.log(error);
                callback("error getting player from database (api.players.update_player:28)", errorcodes.GeneralError);
                return;
            }

			if(players.length === 0)
			{
				callback("no players with this id", errorcodes.GeneralError);
                return;
			}			
            
            var utc = new Date(datetime.utcnow.getTime());
            var dateTime = new Object();
            dateTime.year = utc.getFullYear();
            dateTime.month = utc.getMonth() + 1;
            dateTime.day = utc.getDate();
            dateTime.hour = 0;
            dateTime.minutes = 0;
            dateTime.seconds = 0;
            dateTime.miliseconds = 0;
            
            var doc = {
                "$set":{
                    date: dateTime,
                    name: player.name
                }
            };

            var query2 = {
                doc: doc,
                filter: { id: player.id },
                safe: true
            };

            db.bicaserver.players.update(query2, function(error2, players2) {

                if (error2) {
                    console.log(error2);
                    callback("error updating player from database (api.players.update_player:28)", errorcodes.GeneralError);
                    return;
                }

                var leaderboardQuery = {
                    filter: {
                        id: player.id
                    }, 
                    doc: {
                        "$set":{"name":player.name}
                    },
                    upsert: true,
                    safe: true
                };
                
                db.bicaserver.leaderboard.update(leaderboardQuery, function(error3, players3) {
                    
                    if (error3) {
                        console.log(error3);
                        callback("error updating player from database (api.players.update_player:28)", errorcodes.GeneralError);
                        return;
                    }
                    
                    executeUpdate(null);
                    
                    var playerRet = players[0];
                    playerRet.name = player.name;
                    
                    callback(null, errorcodes.NoError, playerRet);
                });
                
                
            });
        });
    },

    validate_marker: function(marker, callback)
	{		
        if (marker.playerId === null || marker.playerId === undefined)
		{
            callback("error: no player id specified  (api.players.validate_marker:362)", errorcodes.NoUsername);
            return;
        }

        var query =
		{
            filter: { id: marker.playerId },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players)
		{
            if (error)
			{
                callback("error getting player from database (api.players.validate_marker:374)", errorcodes.NoUsername);
                return;
            }

            var filteredPlayer = players[0];

            //check if pack is being scanned on the same day
            var utc = new Date(datetime.utcnow.getTime());
			
			// calculate week number:
			var oneJan = new Date(utc.getFullYear(), 0, 1);
			var numberOfDays = Math.floor((utc - oneJan) / (24 * 60 * 60 * 1000));
            // console.log(numberOfDays);
            // console.log(oneJan.getDay());
			var utcWeekNumber = Math.ceil((oneJan.getDay() + 1 + numberOfDays) / 7);
			// console.log(`The week number of the current date (${utc}) is ${utcWeekNumber}.`);
			
            var Check24h = true;
            var isUniqueMarker = false;
			var isRegionMarker = false;
			var isMonthly = false;
			var isWeekly = false;
			//var regionId = 0;
			var geoLocation = 0;

			var isMarkerTimeValidated = false;
			var isMarkerDateValidated = false;
			var isMarkerRegionValidated = false;
			
			for(var i = 0; i < markers.length; i++)
			{
				if(markers[i] !== undefined && markers[i].id !== undefined && markers[i].id === marker.markerId)
				{
					var serverMarker = markers[i];
                    if(serverMarker.unique != undefined && serverMarker.unique != null && serverMarker.unique === true)
                    {
                        isUniqueMarker = true;
                    }
                    if(serverMarker.monthly != undefined && serverMarker.monthly != null && serverMarker.monthly === true)
                    {
                        isMonthly = true;
                    }
                    if(serverMarker.weekly != undefined && serverMarker.weekly != null && serverMarker.weekly === true)
                    {
                        isWeekly = true;
                    }
					// Check if marker is a region marker
					if(serverMarker.geoLocation != undefined && serverMarker.geoLocation != null && serverMarker.geoLocation === true && serverMarker.normal === true)
					{
						var check = CheckMarkerRegionGPSDistance(marker.markerId, marker.location.x, marker.location.y);
						isRegionMarker = check.success;
						//regionId = check.regionId;
						geoLocation = check.geoLocation;
						break;
					}
					else
					{
						isRegionMarker = false;
						//regionId = 0;
						geoLocation = 0;
						break;
					}
				}
			}

            var m = new Array();
            filteredPlayer.markers.forEach(element => {
                if (element.markerId === marker.markerId)
                    m.push(element);
            });
			
			if(marker.markerId == "c111")
				isRegionMarker = true;

			console.log("checking 24h");
            if (Array.isArray(m))
			{
                if(isUniqueMarker && m.length > 0)
                {
                    console.log("Unique FAIL");
                    callback("error validating marker: marker " + marker.markerId + " is unique and has already been redeemed (api.players.validate_marker:28)", errorcodes.MarkerScannedUnique);
                    return;
                }
                // check if found any
                if (m.length === 0)
				{
                    Check24h = true;
				}
                else
				{
                    // if any marker already on the player is in the same day
                    // as the current date then it can't be scanned
                    Check24h = true;
                    m.forEach(e =>
					{
						if(isRegionMarker)
						{
							var e_date = new Date(e.date.year, e.date.month - 1, e.date.day);
							var e_oneJan = new Date(e_date.getFullYear(), 0, 1);
							var e_numberOfDays = Math.floor((e_date - e_oneJan) / (24 * 60 * 60 * 1000));
							var e_weekNumber = Math.ceil((e_oneJan.getDay() + 1 + e_numberOfDays) / 7);
							// console.log(`The week number of the marker date (${e_date}) is ${e_weekNumber}.`);
							
	                        if (e.date.year === utc.getFullYear() &&
	                            e.date.month === utc.getMonth() + 1 &&
								//e_weekNumber == utcWeekNumber)
                                e.date.day === utc.getDate())
							{
								//if(e.locationId !== undefined && e.locationId === regionId)
								if(e.geoLocation !== undefined && e.geoLocation === geoLocation)
								{
	                            	Check24h = false;
								}
							}
						}
						else if(isMonthly) // one time only
						{
	                        if (e.date.year === utc.getFullYear() &&
	                            e.date.month === utc.getMonth() + 1)
							{
								Check24h = false;
							}
						}
						else if(isWeekly) // one time only
						{
                            console.log("WEEKLY");
	                        var e_date = new Date(e.date.year, e.date.month - 1, e.date.day);
							var e_oneJan = new Date(e_date.getFullYear(), 0, 1);
							var e_numberOfDays = Math.floor((e_date - e_oneJan) / (24 * 60 * 60 * 1000));
							var e_weekNumber = Math.ceil((e_oneJan.getDay() + 1 + e_numberOfDays) / 7);
							// console.log(`The week number of the marker date (${e_date}) is ${e_weekNumber}.`);
							
	                        if (e.date.year === utc.getFullYear() &&
	                            e.date.month === utc.getMonth() + 1 &&
								e_weekNumber == utcWeekNumber)
							{
                                Check24h = false;
							}
						}
						else
						{
	                        if (e.date.year === utc.getFullYear() &&
	                            e.date.month === utc.getMonth() + 1 &&
								e.date.day === utc.getDate())
							{
								Check24h = false;
	                        }
						}
                    });
                }
            }
			else Check24h = true;
			
            if (Check24h)
			{
                var dateTime = new Object();
                dateTime.year = utc.getFullYear();
                dateTime.month = utc.getMonth() + 1;
                dateTime.day = utc.getDate();
                dateTime.hour = utc.getHours();
                dateTime.minutes = utc.getMinutes();
                dateTime.seconds = utc.getSeconds();
                dateTime.miliseconds = 0;

                marker.date = dateTime;
            }
			else
			{
				if(isRegionMarker)
					callback("error validating marker: marker " + marker.markerId + " already scanned today (api.players.validate_marker:422)", errorcodes.MarkerScannedRepeatRegion);
				else if(isMonthly)
                    callback("error validating marker: marker " + marker.markerId + " already scanned this month (api.players.validate_marker:422)", errorcodes.MarkerScannedMonthly);
				else if(isWeekly)
                    callback("error validating marker: marker " + marker.markerId + " already scanned this week (api.players.validate_marker:422)", errorcodes.MarkerScannedWeekly);
                else
					callback("error validating marker: marker " + marker.markerId + " already scanned today (api.players.validate_marker:422)", errorcodes.MarkerAlreadyScanned);
				
                return;
			}
			
			var foundMarker = false;
			
			// Find scanned marker in array
			for(var i = 0; i < markers.length; i++)
			{				
				if(markers[i] !== undefined && markers[i].id !== undefined && markers[i].id === marker.markerId)
				{
					foundMarker = true;
					var serverMarker = markers[i];
					
					// Check if marker is a region marker
					if(serverMarker.geoLocation === true && serverMarker.normal === true)
					{
						isMarkerRegionValidated = CheckMarkerRegionGPSDistance(marker.markerId, marker.location.x, marker.location.y).success;
					}
					// Check if marker is active all day
					else if(serverMarker.dated === true)
					{
						if(serverMarker.dateTime.day === utc.getDate() &&
							serverMarker.dateTime.month === utc.getMonth() + 1 &&
							serverMarker.dateTime.year === utc.getFullYear())
							{
								isMarkerDateValidated = true;
							}
					}
					// Check if Marker is active only at a certain time of the same day
					else if(serverMarker.timed === true)
					{
						if(serverMarker.dateTime.day === utc.getDate() &&
							serverMarker.dateTime.month === utc.getMonth() + 1 &&
							serverMarker.dateTime.year === utc.getFullYear())
							{
								var startTime = new Date(serverMarker.dateTime.year, serverMarker.dateTime.month - 1, serverMarker.dateTime.day, serverMarker.dateTime.hour, serverMarker.dateTime.minutes, serverMarker.dateTime.seconds);
								
								var endTime = new Date(startTime.getTime());
								endTime.setMinutes(endTime.getMinutes() + serverMarker.minutes);
								
								if(utc >= startTime && utc <= endTime)
								{
									isMarkerTimeValidated = true;
								}
							}
					}
					else
					{
						isMarkerTimeValidated = true;
						isMarkerDateValidated = true;
						isMarkerRegionValidated = true;
					}
					
					break;
				}
				else if(markers[i] === undefined || markers[i].id === undefined)
				{
					console.log("MARKER OF INDEX " + i + " UNDEFINED??");
					for(var j = 0; j < markers.length; j++)
					{
						console.log(markers[j], "");
					}
				}
			}
			
			if(!foundMarker)
			{
                console.log("NOT FOUND");
                callback("error validating marker: marker " + marker.markerId + " doesn't exist (api.players.validate_marker:28)", errorcodes.GeneralError);
                return;
            }

            if (!isMarkerRegionValidated && serverMarker.geoLocation === true && serverMarker.normal === true) {
                console.log("Location FAIL");
                callback("error validating marker: marker " + marker.markerId + " not on correct location (api.players.validate_marker:28)", errorcodes.MarkerScannedWrongLocation);
                return;
            }

            if (!isMarkerDateValidated && serverMarker.dated === true) {
                console.log("DATE FAIL");
                callback("error validating marker: marker " + marker.markerId + " not on correct date (api.players.validate_marker:28)", errorcodes.MarkerScannedWrongDate);
                return;
            }

            if (!isMarkerTimeValidated && serverMarker.timed === true) {
                console.log("TIMED FAIL");
                callback("error validating marker: marker " + marker.markerId + " not on correct time interval (api.players.validate_marker:28)", errorcodes.MarkerScannedWrongTime);
                return;
            }

            callback(null, errorcodes.NoError, marker);

        });
    },

    add_marker: function(marker, callback)
	{
        if (marker.playerId === null || marker.playerId === undefined)
		{
            callback("error: no player id specified  (api.players.update_player:68)", errorcodes.GeneralError);
            return;
        }

        var query =
		{
            filter: { id: marker.playerId },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players)
		{
            if (error)
			{
                callback("error getting player from database (api.players.update_player:28)", errorcodes.GeneralError);
                return;
            }

            var filteredPlayer = players[0];

            //check if pack is being scanned on the same day
            var utc = new Date(datetime.utcnow.getTime());

            var dateTime = new Object();
            dateTime.year = utc.getFullYear();
            dateTime.month = utc.getMonth() + 1;
            dateTime.day = utc.getDate();
            dateTime.hour = 0;
            dateTime.minutes = 0;
            dateTime.seconds = 0;
            dateTime.miliseconds = 0;

            var markerAdd = new Object();
            markerAdd.markerId = marker.markerId;
            markerAdd.date = dateTime;

			for(var i = 0; i < markers.length; i++)
			{				
				if(markers[i] !== undefined && markers[i].id !== undefined && markers[i].id === marker.markerId)
				{
					foundMarker = true;
					var serverMarker = markers[i];

					// Check if marker is a region marker
					if(serverMarker.geoLocation === true && serverMarker.normal === true)
					{
						var foundMarker = CheckMarkerRegionGPSDistance(marker.markerId, marker.location.x, marker.location.y);
						//markerAdd.locationId = foundMarker.regionId;
						markerAdd.geoLocation = foundMarker.geoLocation;
                        console.log(foundMarker.geoLocation);
						console.log("------------------LOCATION ID: ", markerAdd.locationId);
						break;
					}
					else
					{
						markerAdd.locationId = 0;
						markerAdd.geoLocation = 0;
						break;
					}
				}
			}

            console.log(markerAdd, "");

            filteredPlayer.markers.push(markerAdd);

            marker.date = dateTime;
            
            if(markerAdd.geoLocation != 0)
            {
                var coordinates = markerAdd.geoLocation.split(', ');
                marker.location.x = parseFloat(coordinates[0]);
                marker.location.y = parseFloat(coordinates[1]);
            }
            else
            {
                marker.location.x = 0;
                marker.location.y = 0;
            }

            var doc = {
                "$set": {
                    markers: filteredPlayer.markers
                }
            };

            var query2 =
			{
                doc: doc,
                filter: { id: marker.playerId },
                safe: true
            };

            db.bicaserver.players.update(query2, function(error2, players2)
			{
                if (error2)
				{
                    callback("error updating player from database (api.players.update_player:28)", errorcodes.GeneralError);
                    return;
                }

                callback(null, errorcodes.NoError, marker);
            });

        });
    },

    add_score: async function(options, callback)
	{
        // Hack to make all scores 0 on the leaderboard
        options.score = 0;
        
        if (options.playerId === null || options.playerId === undefined)
		{
            callback("error: no player id specified  (api.players.add_score:68)", errorcodes.GeneralError, false);
            return;
        }

        if (options.score === undefined || options.score > _MAX_SCORE_PER_REQUEST)
		{
            callback("error: no score provided or score over maximum allowed (api.players.add_score:68)", errorcodes.GeneralError, false);
            return;
        }

        var query =
		{
            filter: { id: options.playerId },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players)
		{
            if (error)
			{
                callback("error getting player from database (api.players.add_score:28)", errorcodes.GeneralError, false);
                return;
            }

            if (players.length === 0) {
                callback("no player found with id " + options.playerId + " (api.players.add_score:28)", errorcodes.GeneralError, false);
                return;
            }
            
            var filteredPlayer = players[0];
            var dateMonthYearString = getDateMonthYearString();
            var prevDateMonthYearString = getPreviousDateMonthYearString();

            if (filteredPlayer.scores === undefined)
                filteredPlayer.scores = {};

            if (filteredPlayer.scores[dateMonthYearString] === undefined)
                filteredPlayer.scores[dateMonthYearString] = 0;
			
            filteredPlayer.scores[dateMonthYearString] += options.score;

            if (getDateMonthString() % 2 == 0)
            {
                var currentMonthScore = filteredPlayer.scores[dateMonthYearString];

                if (filteredPlayer.scores[prevDateMonthYearString] === undefined)
                    filteredPlayer.scores[prevDateMonthYearString] = 0;
				
                var previousMonthScore = filteredPlayer.scores[prevDateMonthYearString];
                filteredPlayer.score = currentMonthScore + previousMonthScore;
            }
            else
            {
                filteredPlayer.score = filteredPlayer.scores[dateMonthYearString];
            }

            //add score to campaign
            var today = new Date(datetime.utcnow);
            if(campaign != null)
            {
                if(campaign.endTime > today)
                {
                    var cID = CAMPAIGN_PREFIX + campaign.campaignId;
                    if (filteredPlayer.scores["c"+cID] === undefined)
                        filteredPlayer.scores["c"+cID] = 0;
                    
                    filteredPlayer.scores["c"+cID] += options.score;
                }
            }

            var lbPlayer;
            if (filteredPlayer.debug_user === undefined || filteredPlayer.debug_user)
                lbPlayer = leaderboardDebug.find(element => element.id === options.playerId);
            else
                lbPlayer = leaderboard.find(element => element.id === options.playerId);

                // ERROR HERE
            if (lbPlayer === undefined) {
                lbPlayer = {...filteredPlayer};
            }
            
            lbPlayer.score = filteredPlayer.score;
            
            for (var i in filteredPlayer.scores) {
                lbPlayer[i] = filteredPlayer.scores[i];
            }

            if (filteredPlayer.lastScore === undefined)
                filteredPlayer.lastScore = 0;
                
            filteredPlayer.lastScore = datetime.now;
            lbPlayer.lastScore = filteredPlayer.lastScore;            

            if(campaign != null)
            {
                if(campaign.endTime > today)
                {
                    var campaignPlayer = leaderboardCampaign.find(element => element.id === options.playerId);
                    
                    if(campaignPlayer === undefined)
                    campaignPlayer = new Object();
                    campaignPlayer.score = filteredPlayer.scores["c"+cID];
                    campaignPlayer.scores = filteredPlayer.scores;
                }
            }

            reorderLeaderboard();

            var doc = {
                "$set": {
                    score: filteredPlayer.score,
                    scores: filteredPlayer.scores,
                    lastScore: filteredPlayer.lastScore
                }
            };
            
            var query2 =
			{
                doc: doc,
                filter: { id: options.playerId },
                safe: true
            };

            db.bicaserver.players.update(query2, function(error2, players2)
			{
                if (error2)
				{
                    console.log(error2);
                    callback("error updating player from database (api.players.add_score:28)", errorcodes.GeneralError, false);
                    return;
                }

                var lbPlayer2 = {};
                lbPlayer2.id = filteredPlayer.id;
                lbPlayer2.regionName = filteredPlayer.regionName;
                lbPlayer2.districtName = filteredPlayer.districtName;
                lbPlayer2.name = filteredPlayer.name;
                lbPlayer2.debug_user = filteredPlayer.debug_user;
                
                for (var i in filteredPlayer.scores) {
                    lbPlayer2[i] = filteredPlayer.scores[i];
                }

                var doc = {
                    "$set": {
                        id: filteredPlayer.id,
                        regionName: filteredPlayer.regionName,
                        districtName: filteredPlayer.districtName,
                        name: filteredPlayer.name,
                        debug_user: filteredPlayer.debug_user
                    }
                };
                
                var lbQuery = {
                    filter: {
                        id: options.playerId
                    },
                    doc: doc,
                    safe: true
                };
                
                db.bicaserver.leaderboard.update(lbQuery, function(error3, players3) {
                
                    if (error3)
                    {
                        callback("error updating player from database (api.players.add_score:28)", errorcodes.GeneralError, false);
                        return;
                    }
                    
                    executeUpdate();
                    callback(null, errorcodes.NoError, true);
                
                });
            });
        });
    },

    add_score_mission: function(mission, callback) {

        mission.score = 0;
    
        if (mission.playerId === null || mission.playerId === undefined) {
            callback("error: no player id specified  (api.players.update_player:68)", errorcodes.GeneralError);
            return;
        }

        var query = {
            filter: { id: mission.playerId },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players) {

            if (error) {
                callback("error getting player from database (api.players.update_player:28)", errorcodes.GeneralError);
                return;
            }

            var filteredPlayer = players[0];
            filteredPlayer.score += mission.score;
            if (filteredPlayer.scores === undefined) {
                filteredPlayer.scores = {};
            }

            var dateMonthYearString = getDateMonthYearString();

            if (filteredPlayer.scores[dateMonthYearString] === undefined) {
                filteredPlayer.scores[dateMonthYearString] = 0;
            }

            filteredPlayer.scores[dateMonthYearString] += options.score;

            var lbPlayer;
            if (filteredPlayer.debug_user)
                lbPlayer = leaderboardDebug.find(element => element.id === options.playerId);
            else
                lbPlayer = leaderboard.find(element => element.id === options.playerId);
                
            lbPlayer.score = filteredPlayer.score;
            
            for (var i in filteredPlayer.scores) {
                lbPlayer[i] = filteredPlayer.scores[i];
            }
            
            reorderLeaderboard();

            var doc = {
                "$set": {
                    score: filteredPlayer.score,
                    scores: filteredPlayer.scores
                }
            };
            
            var query2 = {
                doc: doc,
                filter: { id: mission.playerId },
                safe: true
            };

            db.bicaserver.players.update(query2, function(error2, players2) {

                if (error2) {
                    callback("error updating player from database (api.players.update_player:28)", errorcodes.GeneralError);
                    return;
                }

                callback(null, errorcodes.NoError, mission);
                
                var lbPlayer2 = {};
                lbPlayer2.id = filteredPlayer.id;
                lbPlayer2.regionName = filteredPlayer.regionName;
                lbPlayer2.districtName = filteredPlayer.districtName;
                lbPlayer2.name = filteredPlayer.name;
                
                for (var i in filteredPlayer.scores) {
                    filteredPlayer.scores[i] = newPlayer.scores[i];
                }
                
                var doc = {
                    "$set": {
                        id: filteredPlayer.id,
                        regionName: filteredPlayer.regionName,
                        districtName: filteredPlayer.districtName,
                        name: filteredPlayer.name
                    }
                };

                var lbQuery = {
                    filter: {
                        id: options.id
                    },
                    doc: doc,
                    safe: true
                };
                
                db.bicaserver.leaderboard.update(lbQuery, function(error3, players3) {
                
                    if (error3)
                    {
                        callback("error updating player from database (api.players.add_score:28)", errorcodes.GeneralError, false);
                        return;
                    }
                    executeUpdate();
                    callback(null, errorcodes.NoError, true);
                
                });
            });
        });
    },

    add_card: function(card, callback) {

        if (card.playerId === null || card.playerId === undefined) {
            callback("error: no player id specified  (api.players.update_player:68)", errorcodes.GeneralError);
            return;
        }

        var query = {
            filter: { id: card.playerId },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players) {

            if (error) {
                callback("error getting player from database (api.players.update_player:28)", errorcodes.GeneralError);
                return;
            }

            var filteredPlayer = players[0];
            var cardFound = filteredPlayer.cards.find(element => element.cardId === card.cardId);
            if (cardFound === null || cardFound === undefined) {
                var c = { cardId: card.cardId, numberCollected: 1 };
                filteredPlayer.cards.push(c);
                // console.log(c.numberCollected);
            } else {
                var objIndex = filteredPlayer.cards.findIndex((obj => obj.cardId == card.cardId));
                filteredPlayer.cards[objIndex].numberCollected += 1;
                card.numberCollected = filteredPlayer.cards[objIndex].numberCollected;
                // console.log("2");
            }
            
            var doc = {
                "$set": {
                    cards: filteredPlayer.cards
                }
            };

            var query2 = {
                doc: doc,
                filter: { id: card.playerId },
                safe: true
            };

            db.bicaserver.players.update(query2, function(error, players2) {

                if (error) {
                    callback("error adding card to database (api.players.add_card)", errorcodes.GeneralError);
                    return;
                }

                callback(null, errorcodes.NoError, card);
            });
        });
    },

    get_leaderboard: function(player, callback) {

        /*if (leaderboard.length === 0 && leaderboardDebug.length === 0) {
            console.log("error 1");
            callback("error: no players in Leaderboard", errorcodes.GeneralError);
            return;
        }*/

        if (player.id === null || player.id === undefined) {
            console.log("error 2");
            callback("error: no player id specified", errorcodes.GeneralError);
            return;
        }

        var lb;
        console.log("debug_user: " + player.debug_user);
        
        if (player.use_last) {
            if (player.is_campaign)
            lb = leaderboardCampaign;
        else if (player.debug_user !== undefined && player.debug_user === true)
            lb = leaderboardDebugLast;
        else
            lb = leaderboardLast;
        }
        else {
            if (player.is_campaign)
            lb = leaderboardCampaign;
        else if (player.debug_user !== undefined && player.debug_user === true)
            lb = leaderboardDebug;
        else
            lb = leaderboard;
        }
        
        

        console.log("lb size: " + lb.length);
        console.log("Checking leaderboard " + (player.is_campaign ? " campaign " : (player.debug_user ? " debug " : " normal ")));
        
        var temp = new Array();

		var player = lb.find(element => element.id === player.id);
		
		if(player !== null && player !== undefined)
		{
			var playerPosition = player.position;

			lb.forEach(playerLB => {
				if (playerLB.position < 4 || playerLB.id === player.id ||
					(playerLB.position <= (playerPosition + leaderboardRange) && playerLB.position >= (playerPosition - leaderboardRange))) {
					var tempPlayer = new Object();
					tempPlayer.id = playerLB.id;
					tempPlayer.name = playerLB.name;
					tempPlayer.score = playerLB.score;
					tempPlayer.position = playerLB.position;

					temp.push(tempPlayer);
				}
			});
		}
        else
        {
            for (var i = 0; i < leaderboardRange; i++) {
                if (i == lb.length) {
                    break;
                }
                var playerLB = lb[i];
                var tempPlayer = new Object();
                tempPlayer.id = playerLB.id;
                tempPlayer.name = playerLB.name;
                tempPlayer.score = playerLB.score;
                tempPlayer.position = playerLB.position;

                temp.push(tempPlayer);
            }
        }

        //console.log(temp);
        
        while (temp.length < leaderboardBots.length) {
            var clonePlayer = leaderboardBots[temp.length];
            if (clonePlayer !== undefined) {
                if (temp[temp.length-1] !== undefined) {
                    if (temp[temp.length-1].score === 0) {
                        temp[temp.length-1].score = 100;
                    }
                    clonePlayer.score = Math.max(0, Math.min(1000, temp[temp.length-1].score) - 100);
                }
                temp.push(clonePlayer);
             }
            else
            {
                break;
            }
        }
        
        //console.log(temp);
        //console.log("temp size: " + temp.length);
        callback(null, errorcodes.NoError, temp);
    },

    get_leaderboard_district: function(player, callback) {

        /*if (leaderboard.length === 0 && leaderboardDebug.length === 0) {
            callback("error: no players in Leaderboard", errorcodes.GeneralError);
            return;
        }*/

        if (player.id === null || player.id === undefined) {
            callback("error: no player id specified", errorcodes.GeneralError);
            return;
        }

        var temp = new Array();
        var districtLB = new Array();

        var lb;
        if (player.debug_user)
            lb = leaderboardDebug;
        else
            lb = leaderboard;

        var position = 0;
        lb.forEach(playerLB => {
            if (playerLB.district === player.filter) {
                position++;
                var tempPlayer = new Object();
                tempPlayer.id = playerLB.id;
                tempPlayer.name = playerLB.name;
                tempPlayer.score = playerLB.score;
                tempPlayer.position = position;

                districtLB.push(tempPlayer);
            }
        });

		var player = districtLB.find(element => element.id === player.id);
		if(player !== null && player !== undefined)
		{        
			var playerPosition = player.position;
			districtLB.forEach(playerLB => {
				if (playerLB.position < 4 || playerLB.id === player.id ||
					(playerLB.position <= (playerPosition + leaderboardRange) && playerLB.position >= (playerPosition - leaderboardRange))) {
					temp.push(playerLB);
				}
			});
		}
        else
        {
            for (var i = 0; i < leaderboardRange; i++) {
                if (i == districtLB.length) {
                    break;
                }
                var playerLB = districtLB[i];
                var tempPlayer = new Object();
                tempPlayer.id = playerLB.id;
                tempPlayer.name = playerLB.name;
                tempPlayer.score = playerLB.score;
                tempPlayer.position = playerLB.position;

                temp.push(tempPlayer);
            }
        }

        while (temp.length < leaderboardBots.length) {
            var clonePlayer = leaderboardBots[temp.length];
             if (clonePlayer !== undefined) {
                if (temp[temp.length-1] !== undefined) {
                    if (temp[temp.length-1].score === 0) {
                        temp[temp.length-1].score = 100;
                    }
                    clonePlayer.score = Math.max(0, Math.min(1000, temp[temp.length-1].score) - 100);
                }
                temp.push(clonePlayer);
             }
        }
        
        //console.log(temp);
        callback(null, errorcodes.NoError, temp);
    },

    get_leaderboard_region: function(player, callback) {

        /*if (leaderboard.length === 0 && leaderboardDebug.length === 0) {
            callback("error: no players in Leaderboard", errorcodes.GeneralError);
            return;
        }*/

        if (player.id === null || player.id === undefined) {
            callback("error: no player id specified", errorcodes.GeneralError);
            return;
        }

        var temp = new Array();
        var regionLB = new Array();

        var lb;
        //overwritten for campaigns
        if(campaign == null)
        {
            console.log("campaign is null");
            if (player.debug_user)
                lb = leaderboardDebug;
            else
                lb = leaderboard;
        }
        else
            lb = leaderboardCampaign;

        var position = 0;
        lb.forEach(playerLB => {
            if (playerLB.region === player.filter || campaign != null) {
                position++;
                var tempPlayer = new Object();
                tempPlayer.id = playerLB.id;
                tempPlayer.name = playerLB.name;
                tempPlayer.score = playerLB.score;
                tempPlayer.position = position;

                regionLB.push(tempPlayer);
            }
        });

        var player = regionLB.find(element => element.id === player.id);
        if(player !== null && player !== undefined)
		{  
            var playerPosition = player === undefined ? -1 : player.position;
            regionLB.forEach(playerLB => {
                if (playerLB.position > 0 && playerLB.position < 4 || playerLB.id === player.id ||
                    (playerLB.position <= (playerPosition + leaderboardRange) && playerLB.position >= (playerPosition - leaderboardRange))) {
                    temp.push(playerLB);
                }
            });
        }
        else
        {
            for (var i = 0; i < leaderboardRange; i++) {
                if (i == regionLB.length) {
                    break;
                }
                var playerLB = regionLB[i];
                var tempPlayer = new Object();
                tempPlayer.id = playerLB.id;
                tempPlayer.name = playerLB.name;
                tempPlayer.score = playerLB.score;
                tempPlayer.position = playerLB.position;

                temp.push(tempPlayer);
            }
        }

        while (temp.length < leaderboardBots.length) {
            var clonePlayer = leaderboardBots[temp.length];
            if (clonePlayer !== undefined) {
               if (temp[temp.length-1] !== undefined) {
                    if (temp[temp.length-1].score === 0) {
                            temp[temp.length-1].score = 100;
                        }
                   clonePlayer.score = Math.max(0, Math.min(1000, temp[temp.length-1].score) - 100);
               }
               temp.push(clonePlayer);
            }
        }
        
        //console.log(temp);
        callback(null, errorcodes.NoError, temp);
    },

    get_leaderboard_lastmonth_top3: function(player, callback) {

        var utc = new Date(datetime.utcnow.getTime());
        utc.setMonth(utc.getMonth() - 1);
        var utcstring = (utc.getMonth() + 1) + "-" + utc.getFullYear();

        var scoreString = utcstring;
        var sortScore = {};
        sortScore[scoreString] = -1;

        var matchStr = {};
        matchStr[scoreString] = { $ne: null };
        
        var lbType = player.lbType; // 0 = national, 1 = district, 2 = regional
        
        if (lbType === 1) {
            matchStr["districtName"] = player.districtName;
        }
        else if (lbType === 2) {
            matchStr["regionName"] = player.regionName;
        }

        var query = {
            aggregate: [{
                    $sort: sortScore
                },
                {
                    $limit: 3
                },
                {
                    $match: matchStr
                }
            ]
        };

        db.bicaserver.leaderboard.aggregate(query, function(error, results) {

            if (error) {
                callback("error getting last month leaderboard (api.players.get_leaderboard_lastmonth_top3)", errorcodes.GeneralError);
                return;
            }

            callback(null, errorcodes.NoError, results);
        });
    },

    get_markers: function(player, callback) {

        if (markers.length === 0) {
            callback("error: no markers in list", errorcodes.GeneralError);
            return;
        }

        // var temp = new Array();
        // markers.forEach(marker => {
        //     temp.push(marker);
        // });
        console.log(markers[1]);

        callback(null, errorcodes.NoError, markers);
    },

    get_locations: function(options, callback) {

        if (c111.length === 0) {
            callback("error: no region locations in list", errorcodes.GeneralError);
            return;
        }

        if (aldi.length === 0) {
            callback("error: no aldi locations in list", errorcodes.GeneralError);
            return;
        }

        if (options.id === null || options.id === undefined) {
            callback("error: no player id specified  (api.players.get_player:68)", errorcodes.GeneralError);
            return;
        }

        var query = {
            filter: { id: options.id },
            safe: true
        };

        db.bicaserver.players.get(query, function(error, players) {

            if (error) {
                callback("error getting player from database (api.players.get_player:28)", errorcodes.GeneralError);
                return;
            }

            var player = players[0];
            // player.markers

            var utc = new Date(datetime.utcnow.getTime());
			
			// calculate week number:
			var oneJan = new Date(utc.getFullYear(), 0, 1);
			var numberOfDays = Math.floor((utc - oneJan) / (24 * 60 * 60 * 1000));
			var utcWeekNumber = Math.ceil((oneJan.getDay() + 1 + numberOfDays) / 7);
            
            // console.log(`The week number of the UTC date (${utc}) is ${utcWeekNumber}.`);
            var temp = new Array();
            var i = 0;
            c111.forEach(loc => {
                var position = {}

                position.location = {}
                position.location.x = loc.latitude;
                position.location.y = loc.longitude;
                var geoLocation = loc.latitude + ", " + loc.longitude;
                position.markerType = 1;

                position.isDone = false;
                
                player.markers.forEach(e =>
                {
                    if(geoLocation == e.geoLocation)
                    {
                        var e_date = new Date(e.date.year, e.date.month - 1, e.date.day);
                        var e_oneJan = new Date(e_date.getFullYear(), 0, 1);
                        var e_numberOfDays = Math.floor((e_date - e_oneJan) / (24 * 60 * 60 * 1000));
                        var e_weekNumber = Math.ceil((e_oneJan.getDay() + 1 + e_numberOfDays) / 7);
                        // console.log(`The week number of the marker date (${e_date}) is ${e_weekNumber}.`);
                        
                        if (e.date.year === utc.getFullYear() &&
                        e.date.month === utc.getMonth() + 1 &&
                        //e_weekNumber == utcWeekNumber)
                        e.date.day === utc.getDate())
                        {
                            
                            // console.log(i);
                            position.isDone = true;
                        }
                    }
                });

                if(loc.regionName == "TEST")
                {
                    // console.log(loc.regionName);
                    // console.log(player.debug_user);
                    if(options.debug_user != undefined && options.debug_user)
                        temp.push(position);
                }
                else
                    temp.push(position);

                i += 1;
            });

            i = 0;
            // aldi.forEach(loc => {
            //     var position = {}

            //     position.location = {}
            //     position.location.x = loc.latitude;
            //     position.location.y = loc.longitude;
            //     var geoLocation = loc.latitude + ";" + loc.longitude;
            //     position.markerType = 2;

            //     position.isDone = false;
                
            //     player.markers.forEach(e =>
            //     {
            //         if(geoLocation == e.geoLocation)
            //         {
            //             var e_date = new Date(e.date.year, e.date.month - 1, e.date.day);
            //             var e_oneJan = new Date(e_date.getFullYear(), 0, 1);
            //             var e_numberOfDays = Math.floor((e_date - e_oneJan) / (24 * 60 * 60 * 1000));
            //             var e_weekNumber = Math.ceil((e_oneJan.getDay() + 1 + e_numberOfDays) / 7);
            //             // console.log(`The week number of the marker date (${e_date}) is ${e_weekNumber}.`);
                        
            //             if (e.date.year === utc.getFullYear() &&
            //             e.date.month === utc.getMonth() + 1 &&
            //             e_weekNumber == utcWeekNumber)
            //             {
                            
            //                 // console.log(i);
            //                 position.isDone = true;
            //             }
            //         }
            //     });

            //     temp.push(position);
            //     i += 1;
            // });

            callback(null, errorcodes.NoError, temp);
        });
    },

    check_leaderboard_prizes: function(options, callback) {

        console.log("HERE _______________________");
        if (options.playerId === undefined) {
            callback("no player id found (api.players.check_leaderboard_prizes:25)", errorcodes.GeneralError, false);
            return;
        }

        if (options.debug_user === undefined) {
            callback("debug_user variable undefined (api.players.check_leaderboard_prizes:25)", errorcodes.GeneralError, false);
            return;
        }
        
        options.id = options.playerId;
        options.use_last = true;
        
        options.regionName = "";
        options.districtName = "";

        if(options.debug_user === true)
        {
            for (var i = 0; i < leaderboardDebug.length; i++) {
                if (leaderboardDebug[i].id === options.id){
                    options.regionName = leaderboardDebug[i].region;
                    options.districtName = leaderboardDebug[i].district;
                    break;
                }
            }
            
            players.get_leaderboard(options, (error, errorcode, results) => {
                if (error) {
                    callback("error getting last month leaderboard (api.players.check_leaderboard_prizes)", errorcodes.GeneralError);
                    return;
                }

                var missionConfirmValues = [];
                for (var i = 0; i < leaderboardMissionIds.length; i++)
                {
                    missionConfirmValues[leaderboardMissionIds[i]] = false;
                }
    
                var missionCheckId = -1;

                var months = getPreviousLeaderboardDateStrings();
                results.sort(function(a, b) {
                    if(a === undefined) a = {};
                    if(b === undefined) b = {};
                    
                    var aScore = 0;
                    var bScore = 0;
                    for (var i = 0; i < months.length; i++) {
                        if(a[months[i]] === undefined) a[months[i]] = 0;
                        if(b[months[i]] === undefined) b[months[i]] = 0;
                        aScore += a[months[i]];
                        bScore += b[months[i]];
                    }
                    
                    if(a.id == options.id) console.log(aScore);
                
                    if(a.lastScore === undefined) a.lastScore = 0;
                    if(b.lastScore === undefined) b.lastScore = 0;
                    if(a.totalScore === undefined) a.totalScore = aScore;
                    if(b.totalScore === undefined) b.totalScore = bScore;
                
                    // Sort by score
                    // If the first item has a higher number, move it down
                    // If the first item has a lower number, move it up
                    if (aScore < bScore) return 1;
                    if (aScore > bScore) return -1;
                
                    // If the score number is the same between both items, sort by timestamp
                    // If the first score was achieved first, move it up
                    // Otherwise move it down
                    if (a.lastScore < b.lastScore) return 1;
                    if (a.lastScore > b.lastScore) return -1;

                    // return bScore - aScore;
                    return 0;
                });
    
                // for (var i = 0; i < 100; i++) {
                //     missionConfirmValues[leaderboardMissionIds[i]] = filtered[i].id === options.playerId;
                //     if (filtered[i].id === options.playerId) {
                //         missionCheckId = leaderboardMissionIds[i];
                //     }
                // }
    
                var currentLbPrizeStartTime;
                for (var i = 0; i < leaderboardSettings.length; i++) {
                    if (leaderboardSettings[i].leaderboardId === leaderboardMissionIds[0]) {
                        currentLbPrizeStartTime = {... leaderboardSettings[i]}["startTime"];
                        currentLbPrizeStartTime = currentLbPrizeStartTime.setFullYear(currentLbPrizeStartTime.getFullYear() + 1);
                        break;
                    }
                }
                
                
                var currentLbSettings;
                
                //top3 mission national
                for (var i = 0; i < 3; i++) {
                    if (results.length <= i) {
                        break;
                    }
                    
                    if(results[i] !== undefined && results[i].id === options.playerId && results[i].score > 0 && (currentLbSettings === undefined || (currentLbPrizeStartTime !== undefined && datetime.utcnow > currentLbPrizeStartTime)))
                    {
                        missionConfirmValues[leaderboardMissionIds[0]] = true;
                        break;
                    }
                }
                
                currentLbPrizeStartTime = undefined;
                
                for (var i = 0; i < leaderboardSettings.length; i++) {
                    if (leaderboardSettings[i].leaderboardId === leaderboardMissionIds[1]) {
                        currentLbPrizeStartTime = {... leaderboardSettings[i]}["startTime"];
                        currentLbPrizeStartTime = currentLbPrizeStartTime.setFullYear(currentLbPrizeStartTime.getFullYear() + 1);
                        break;
                    }
                }
                
                //top3 mission district
                var districtResults = [];
                for (var i = 0; i < results.length; i++) {
                    
                    var dName = "";
                    for (var j = 0; j < leaderboardDebug.length; j++) {
                        if (leaderboardDebug[j].id === results[i].id){
                            dName = leaderboardDebug[j].district;
                            break;
                        }
                    }
                    
                    if (dName === options.districtName) {
                        districtResults.push(results[i]);
                    }
                }
                    
                
                for (var i = 0; i < 3; i++) {
                    if (districtResults.length <= i) {
                        break;
                    }
                    
                    if(districtResults[i] !== undefined && districtResults[i].id === options.playerId && districtResults[i].score > 0 && (currentLbSettings === undefined || (currentLbPrizeStartTime !== undefined && datetime.now > currentLbPrizeStartTime)))
                    {
                        missionConfirmValues[leaderboardMissionIds[1]] = true;
                        break;
                    }
                }
                
                currentLbPrizeStartTime = undefined;
                
                for (var i = 0; i < leaderboardSettings.length; i++) {
                    if (leaderboardSettings[i].leaderboardId === leaderboardMissionIds[2]) {
                        currentLbPrizeStartTime = {... leaderboardSettings[i]}["startTime"];
                        currentLbPrizeStartTime = currentLbPrizeStartTime.setFullYear(currentLbPrizeStartTime.getFullYear() + 1);
                        break;
                    }
                }
                
                //top3 mission regional
                var regionalResults = [];
                for (var i = 0; i < results.length; i++) {
                    
                    var rName = "";
                    for (var j = 0; j < leaderboardDebug.length; j++) {
                        if (leaderboardDebug[j].id === results[i].id){
                            rName = leaderboardDebug[j].region;
                            break;
                        }
                    }
                    
                    if (rName === options.regionName) {
                        regionalResults.push(results[i]);
                    }
                }
                    
                for (var i = 0; i < 3; i++) {
                    if (regionalResults.length <= i) {
                        break;
                    }
                    
                    if(regionalResults[i] !== undefined && regionalResults[i].id === options.playerId && regionalResults[i].score > 0 && (currentLbSettings === undefined || (currentLbPrizeStartTime !== undefined && datetime.now > currentLbPrizeStartTime)))
                    {
                        missionConfirmValues[leaderboardMissionIds[2]] = true;
                        break;
                    }
                }
    
                var missionsArray = [];
    
                // TODO: Make dictionaries valid in client side, not just turn this into an Array
                for (var key in missionConfirmValues) {
                    var newObject = {
                        "missionId": key,
                        "isAvailable": missionConfirmValues[key]
                    };
                    missionsArray.push(newObject);
                }
    
                // Just a quick skip
                if (missionCheckId === -2) {
                    callback(null, errorcodes.NoError, missionsArray);
                } else {
                    // check redeems
                    var query2 = {
                        filter: {
                            "missionId": {"$in": leaderboardMissionIds}
                        },
                        safe: true
                    };
    
                    db.bicaserver.redeems.get(query2, function(error2, results2) {
    
                        if (error2) {
                            console.log(error2);
                            callback("error getting redeems data from database (api.players.check_leaderboard_prizes:25)", errorcodes.GeneralError, false);
                            return;
                        }
    
                        if (results2.length === 0) {
                            callback(null, errorcodes.NoError, missionsArray);
                        } else {
                            for ( var k = 0; k < results2.length; k++) {
                                var redeems = results2[k];
                                console.log(results2[k]);
                                for (var i = 0; i < redeems.redeemData.length; i++) {
                                    var currentUser = redeems.redeemData[i];
        
                                    var lastLbMonths = getPreviousLeaderboardDateStrings();
                                    var lastLbMonth = lastLbMonths[lastLbMonths.length - 1];
                                    var month = parseInt(lastLbMonth.split("-")[0]);
                                    var year = parseInt(lastLbMonth.split("-")[1]);
        
                                    if (currentUser.playerId === options.playerId/* && currentUser.leaderboardMonth === month && currentUser.leaderboardYear === year*/) {
                                        
                                        for (var j = 0; j < missionsArray.length; j++) {
                                            if (missionsArray[j].missionId.toString() === redeems.missionId.toString()) {
                                                missionsArray[j].isAvailable = false;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
    
                            callback(null, errorcodes.NoError, missionsArray);
                        }
                    });
                }
            });
        }
        else
        {
            var currentLbSettings;
            
            for (var i = 0; i < leaderboard.length; i++) {
                if (leaderboard[i].id === options.id){
                    options.regionName = leaderboard[i].region;
                    options.districtName = leaderboard[i].district;
                    break;
                }
            }
            
            players.get_leaderboard(options, (error, errorcode, results) => {
                if (error) {
                    callback("error getting last month leaderboard (api.players.check_leaderboard_prizes)", errorcodes.GeneralError);
                    return;
                }

                var missionConfirmValues = [];
                for (var i = 0; i < leaderboardMissionIds.length; i++)
                {
                    missionConfirmValues[leaderboardMissionIds[i]] = false;
                }
    
                var missionCheckId = -1;

                var months = getPreviousLeaderboardDateStrings();
                    
                results.sort(function(a, b) {
                    if(a === undefined) a = {};
                    if(b === undefined) b = {};
                    
                    var aScore = 0;
                    var bScore = 0;
                    for (var i = 0; i < months.length; i++) {
                        if(a[months[i]] === undefined) a[months[i]] = 0;
                        if(b[months[i]] === undefined) b[months[i]] = 0;
                        aScore += a[months[i]];
                        bScore += b[months[i]];
                    }
                    
                    if(a.id == options.id) console.log(aScore);
                
                    if(a.lastScore === undefined) a.lastScore = 0;
                    if(b.lastScore === undefined) b.lastScore = 0;
                    if(a.totalScore === undefined) a.totalScore = aScore;
                    if(b.totalScore === undefined) b.totalScore = bScore;
                
                    // Sort by score
                    // If the first item has a higher number, move it down
                    // If the first item has a lower number, move it up
                    if (aScore < bScore) return 1;
                    if (aScore > bScore) return -1;
                
                    // If the score number is the same between both items, sort by timestamp
                    // If the first score was achieved first, move it up
                    // Otherwise move it down
                    if (a.lastScore < b.lastScore) return 1;
                    if (a.lastScore > b.lastScore) return -1;

                    // return bScore - aScore;
                    return 0;
                });
    
                // for (var i = 0; i < 100; i++) {
                //     missionConfirmValues[leaderboardMissionIds[i]] = filtered[i].id === options.playerId;
                //     if (filtered[i].id === options.playerId) {
                //         missionCheckId = leaderboardMissionIds[i];
                //     }
                // }

                // console.log("First Place ----------------------------------------------");
                // console.log(results[0].id);
                // console.log(results.length);
    
                var currentLbPrizeStartTime;
                for (var i = 0; i < leaderboardSettings.length; i++) {
                    if (leaderboardSettings[i].leaderboardId === leaderboardMissionIds[0]) {
                        currentLbPrizeStartTime = {... leaderboardSettings[i]}["startTime"];
                        currentLbPrizeStartTime = currentLbPrizeStartTime.setFullYear(currentLbPrizeStartTime.getFullYear() + 1);
                        break;
                    }
                }
    
                //top3 mission national
                for (var i = 0; i < 3; i++) {
                    if (results.length <= i) {
                        break;
                    }
                    
                    if(results[i] !== undefined && results[i].id === options.playerId && results[i].score > 0 && (currentLbSettings === undefined || (currentLbPrizeStartTime !== undefined && datetime.utcnow > currentLbPrizeStartTime)))
                    {
                        missionConfirmValues[leaderboardMissionIds[0]] = true;
                        break;
                    }
                }
                
                currentLbPrizeStartTime = undefined;
                
                for (var i = 0; i < leaderboardSettings.length; i++) {
                    if (leaderboardSettings[i].leaderboardId === leaderboardMissionIds[1]) {
                        currentLbPrizeStartTime = {... leaderboardSettings[i]}["startTime"];
                        currentLbPrizeStartTime = currentLbPrizeStartTime.setFullYear(currentLbPrizeStartTime.getFullYear() + 1);
                        break;
                    }
                }
                
                //top3 mission district
                var districtResults = [];
                for (var i = 0; i < results.length; i++) {
                    var dName = "";
                    for (var j = 0; j < leaderboard.length; j++) {
                        if (leaderboard[j].id === results[i].id){
                            dName = leaderboard[j].district;
                            break;
                        }
                    }
                    
                    if (dName === options.districtName) {
                        districtResults.push(results[i]);
                    }
                }
                    
                
                for (var i = 0; i < 3; i++) {
                    if (districtResults.length <= i) {
                        break;
                    }
                    
                    if(districtResults[i] !== undefined && districtResults[i].id === options.playerId && districtResults[i].score > 0 && (currentLbSettings === undefined || (currentLbPrizeStartTime !== undefined && datetime.now > currentLbPrizeStartTime)))
                    {
                        missionConfirmValues[leaderboardMissionIds[1]] = true;
                        break;
                    }
                }
                
                currentLbPrizeStartTime = undefined;
                
                for (var i = 0; i < leaderboardSettings.length; i++) {
                    if (leaderboardSettings[i].leaderboardId === leaderboardMissionIds[2]) {
                        currentLbPrizeStartTime = {... leaderboardSettings[i]}["startTime"];
                        currentLbPrizeStartTime = currentLbPrizeStartTime.setFullYear(currentLbPrizeStartTime.getFullYear() + 1);
                        break;
                    }
                }
                
                //top3 mission regional
                var regionalResults = [];
                for (var i = 0; i < results.length; i++) {
                    var rName = "";
                    for (var j = 0; j < leaderboard.length; j++) {
                        if (leaderboard[j].id === results[i].id){
                            rName = leaderboard[j].region;
                            break;
                        }
                    }
                    
                    if (rName === options.regionName) {
                        regionalResults.push(results[i]);
                    }
                }
                    
                for (var i = 0; i < 3; i++) {
                    if (regionalResults.length <= i) {
                        break;
                    }
                    
                    if(regionalResults[i] !== undefined && regionalResults[i].id === options.playerId && regionalResults[i].score > 0 && (currentLbSettings === undefined || (currentLbPrizeStartTime !== undefined && datetime.now > currentLbPrizeStartTime)))
                    {
                        missionConfirmValues[leaderboardMissionIds[2]] = true;
                        break;
                    }
                }
    
                var missionsArray = [];
    
                // TODO: Make dictionaries valid in client side, not just turn this into an Array
                for (var key in missionConfirmValues) {
                    var newObject = {
                        "missionId": key,
                        "isAvailable": missionConfirmValues[key]
                    };
                    missionsArray.push(newObject);
                }
    
                // Just a quick skip
                if (missionCheckId === -2) {
                    callback(null, errorcodes.NoError, missionsArray);
                } else {
                    // check redeems
                    var query2 = {
                        filter: {
                            "missionId": {"$in": leaderboardMissionIds}
                        },
                        safe: true
                    };
    
                    db.bicaserver.redeems.get(query2, function(error2, results2) {
    
                        if (error2) {
                            console.log(error2);
                            callback("error getting redeems data from database (api.players.check_leaderboard_prizes:25)", errorcodes.GeneralError, false);
                            return;
                        }
    
                        if (results2.length === 0) {
                            callback(null, errorcodes.NoError, missionsArray);
                        } else {
                            for ( var k = 0; k < results2.length; k++) {
                                var redeems = results2[k];
                                console.log(results2[k]);
                                for (var i = 0; i < redeems.redeemData.length; i++) {
                                    var currentUser = redeems.redeemData[i];
        
                                    var lastLbMonths = getPreviousLeaderboardDateStrings();
                                    var lastLbMonth = lastLbMonths[lastLbMonths.length - 1];
                                    var month = parseInt(lastLbMonth.split("-")[0]);
                                    var year = parseInt(lastLbMonth.split("-")[1]);
        
                                    if (currentUser.playerId === options.playerId/* && currentUser.leaderboardMonth === month && currentUser.leaderboardYear === year*/) {
                                        
                                        for (var j = 0; j < missionsArray.length; j++) {
                                            if (missionsArray[j].missionId.toString() === redeems.missionId.toString()) {
                                                missionsArray[j].isAvailable = false;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
    
                            callback(null, errorcodes.NoError, missionsArray);
                        }
                    });
                }
            });
        }

    },

    redeem_leaderboard_prize: function(options, callback) {

        if (options.playerId === undefined) {
            callback("no player id found (api.players.redeem_leaderboard_prize:25)", errorcodes.GeneralError, false);
            return;
        }

        players.get_leaderboard_lastmonth_top3(options, (error, errorcode, results) => {
            if (error) {
                callback("error getting last month leaderboard (api.players.redeem_leaderboard_prize)", errorcodes.GeneralError, false);
                return;
            }

            var isPositionValid = false;

            var index = leaderboardMissionIds.indexOf(options.missionId);
            if (index === -1) {
                callback("missionId is not one of the leaderboard mission ids (api.players.redeem_leaderboard_prize)", errorcodes.GeneralError, false);
                return;
            }

            var placementResult = results[index];
            if (placementResult.id !== options.playerId) {
                callback("this player is not on the " + (index + 1) + " position (api.players.redeem_leaderboard_prize)", errorcodes.GeneralError, false);
                return;
            }

            // check redeems
            var query2 = {
                filter: {
                    "missionId": options.missionId
                },
                safe: true
            };

            db.bicaserver.redeems.get(query2, function(error2, results2) {

                if (error2) {
                    console.log(error2);
                    callback("error getting redeems data from database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                    return;
                }

                var utctime = new Date(datetime.utcnow.getTime());
                utctime.setMonth(utctime.getMonth() - 1);

                var lastLbMonths = getPreviousLeaderboardDateStrings();
                var lastLbMonth = lastLbMonths[lastLbMonths.length - 1];
                var month = int.parse(lastLbMonth.split("-")[0]);
                var year = int.parse(lastLbMonth.split("-")[1]);
                
                if (results2.length === 0) {
                    var newRedeemData = {};
                    newRedeemData["missionId"] = options.missionId;

                    var redeemInnerData = [];
                    var currentRedeemData = {};

                    currentRedeemData["playerId"] = options.playerId;
                    currentRedeemData["geoLocation"] = options.geoLocation;
                    currentRedeemData["leaderboardMonth"] = month;
                    currentRedeemData["leaderboardYear"] = year;
                    currentRedeemData["promoterName"] = options.promoterName;
                    currentRedeemData["timestamp"] = datetime.now;

                    redeemInnerData.push(currentRedeemData);

                    newRedeemData["redeemData"] = redeemInnerData;

                    var insertQuery = {
                        doc: newRedeemData,
                        safe: true
                    };

                    db.bicaserver.redeems.insert(insertQuery, function(error3, items) {
                        if (error3) {
                            callback("error inserting redeems data on database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                            return;
                        }

                        callback(null, errorcodes.NoError, true);
                    });
                } else {
                    var codeRedeems = results2[0];
                    for (var i = 0; i < codeRedeems.redeemData.length; i++) {
                        var currentUser = codeRedeems.redeemData[i];
                        if (currentUser.playerId === options.playerId && currentUser.leaderboardMonth === month && currentUser.leaderboardYear === year) {
                            callback("error player already redeemed (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                            return;
                        }
                    }

                    var currentRedeemData = {};

                    currentRedeemData["playerId"] = options.playerId;
                    currentRedeemData["geoLocation"] = options.geoLocation;
                    currentRedeemData["leaderboardMonth"] = month;
                    currentRedeemData["leaderboardYear"] = year;
                    currentRedeemData["promoterName"] = options.promoterName;
                    currentRedeemData["timestamp"] = datetime.now;

                    codeRedeems["redeemData"].push(currentRedeemData);

                    var doc = {
                        "$set": {
                            playerId: options.playerId,
                            geoLocation: options.geoLocation,
                            leaderboardMonth: month,
                            leaderboardYear: year,
                            promoterName: options.promoterName,
                            timestamp: datetime.now
                        }
                    };
                    
                    var updateQuery = {
                        filter: {
                            "missionId": options.missionId
                        },
                        doc: doc,
                        safe: true
                    };

                    db.bicaserver.redeems.update(updateQuery, function(error4, items) {
                        if (error4) {
                            console.log(error4);
                            callback("error updating redeems data on database (api.prizeCodes.get_prize_missions:25)", errorcodes.GeneralError, false);
                            return;
                        }

                        callback(null, errorcodes.NoError, true);
                    });
                }
            });
        });
    },

    check_campaign_prizes: function(options, callback) {
    
        if (options.playerId === undefined) {
            callback("no player id found (api.players.check_campaign_prizes:25)", errorcodes.GeneralError, false);
            return;
        }

        var prizeCampaign = null;
        var today = new Date(datetime.utcnow);
        campaigns.forEach(c => {
            if(c.endTime < today && c.redeemTime > today)
            {
                prizeCampaign = c;
            }
        });

        if (prizeCampaign === null) {
            console.log("check_campaign_prizes: No prize campaign active");
            callback("No active campaign", errorcodes.NoActiveCampaign, false);
            return;
        }
        
        options.id = options.playerId;
        options.is_campaign = true;
        
        players.get_leaderboard(options, (error, errorcode, results) => {
            if (error) {
                console.log(error);
                callback("error getting campaign leaderboard (api.players.check_campaign_prizes)", errorcodes.GeneralError);
                return;
            }
            
            //TODO CHECK IF CAMPAIGN HAS ENDED
            //TODO CHECK IF CAMPAIGN REDEEM PERIOD IS OVER

            var missionConfirmValues = [];
            for (var i = 0; i < campaignMissionIds.length; i++)
            {
                missionConfirmValues[campaignMissionIds[i]] = false;
            }
            
            var cID = CAMPAIGN_PREFIX + prizeCampaign.campaignId;

            var missionCheckId = -1;

            console.log("c"+cID);
            console.log(options.playerId);
            results.sort(function(a, b) {
                if(a === undefined) a = {};
                if(a.score === undefined) a.score = -1;
                if(b === undefined) b = {};
                if(b.score === undefined) b.score = -1;
                
                var aScore = a.score;
                var bScore = b.score;
            
                if(a.lastScore === undefined) a.lastScore = 0;
                if(b.lastScore === undefined) b.lastScore = 0;
            
                // Sort by score
                // If the first item has a higher number, move it down
                // If the first item has a lower number, move it up
                if (aScore < bScore) return 1;
                if (aScore > bScore) return -1;
            
                // If the score number is the same between both items, sort by timestamp
                // If the first score was achieved first, move it up
                // Otherwise move it down
                if (a.lastScore < b.lastScore) return 1;
                if (a.lastScore > b.lastScore) return -1;

                // return bScore - aScore;
                return 0;
            });

            console.log("Results for lb: " + results.length);
            
             //console.log("First Place ----------------------------------------------");
             //console.log(results[0].id);
             //console.log(results[1].id);
             //console.log(results[2].id);
             //console.log(results.length);

            //Top 3 mission
            if(results[0].id === options.playerId || results[1].id === options.playerId || results[2].id === options.playerId)
            {
                if(results[0].score >= 0 || results[1].score >= 0 || results[2].score >= 0)
                {
                    missionConfirmValues[campaignMissionIds[0]] = true;
                    missionCheckId = campaignMissionIds[0];
                }

            }
            
            
            
            //top 4-200 mission
            for (var i = 3; i < results.length && i < 200; i++)
            {
                if(results[i].score >= 0)
                {
                    missionConfirmValues[campaignMissionIds[1]] = results[i].id === options.playerId;
                    if (results[i].id === options.playerId) {
                        missionCheckId = campaignMissionIds[1];
                        break;
                    }
                }
            }

            var missionsArray = [];

            // TODO: Make dictionaries valid in client side, not just turn this into an Array
            for (var key in missionConfirmValues) {
                var newObject = {
                    "missionId": key,
                    "Done": missionConfirmValues[key],
                    "isAvailable": true
                };
                missionsArray.push(newObject);
            }
            
            if (prizeCampaign.endTime > today)
            {
                console.log("Campaign not in redeem period");
                for (var j = 0; j < missionsArray.length; j++)
                {
                    missionsArray[j].Done = false;
                }
            }

            if (missionCheckId === -1) {
                callback(null, errorcodes.NoError, missionsArray);
            } else {
                // check redeems
                var query2 = {
                    filter: {
                        "missionId": missionCheckId
                    },
                    safe: true
                };

                db.bicaserver.redeems.get(query2, function(error2, results2) {

                    if (error2) {
                        console.log(error2);
                        callback("error getting redeems data from database (api.players.check_leaderboard_prizes:25)", errorcodes.GeneralError, false);
                        return;
                    }

                    if (results2.length === 0) {
                        callback(null, errorcodes.NoError, missionsArray);
                    } else {
                        var redeems = results2[0];
                        for (var i = 0; i < redeems.redeemData.length; i++) {
                            var currentUser = redeems.redeemData[i];
                            
                            for (var j = 0; j < missionsArray.length; j++) {
                                // console.log(missionsArray[j].isAvailable);
                            }

                            if (currentUser.playerId === options.playerId) {

                                for (var j = 0; j < missionsArray.length; j++) {
                                    missionsArray[j].isAvailable = false;
                                }
                                break;
                            }
                        }

                        console.log(missionsArray);
                        callback(null, errorcodes.NoError, missionsArray);
                    }
                });
            }
        });
    },

    get_daily_chest: function(options, callback) {

        if (options.id === undefined) {
            callback("no player id found (api.players.get_daily_chest:965)", errorcodes.GeneralError, false);
            return;
        }

        date = new Date(datetime.utcnow.getTime());

        var str = "" + options.id + date.getDate() + date.getMonth() + date.getFullYear();
        var seed = parseInt(str);
        var rng = seedrandom(seed);
        var sameRandom = rng();
        var randomChest = Math.floor(sameRandom * _NUM_CHESTS);

        var callbackInfo = {};
        callbackInfo["playerId"] = options.id;
        callbackInfo["chestIndex"] = randomChest;
        callback(null, errorcodes.NoError, callbackInfo);

        return;
    },

    get_campaign: function(options, callback) {
        
        var camp = {};
        console.log("Campaign: " + campaign);
        if(campaign != null)
        {
            camp.campaignName = campaign.campaignName;
            camp.campaignFullName = campaign.campaignFullName;
            camp.campaignId = campaign.campaignId;
            camp.isActive = true;
			camp.campaignRedeemTime = (campaign.redeemTime.getTime() / 1000);
        }
        else
        {
            camp.isActive = false;
            camp.campaignId = 0;
            camp.campaignName = "";
            camp.campaignFullName = "";
			
			var prizeCampaign = null;
			var today = new Date(datetime.utcnow);
			campaigns.forEach(c => {
				if(c.endTime < today && c.redeemTime > today)
				{
					prizeCampaign = c;
				}
			});
			camp.campaignRedeemTime = prizeCampaign === null ? 0 : (prizeCampaign.redeemTime.getTime() / 1000);
			
        }

        callback(null, errorcodes.NoError, camp);
        return;
    }
};

function executeUpdate() {
    players.update_leaderboard(null, updateLeaderboard);
}

function updateLeaderboard(errorMessage, errorCode, results) {
    
    console.log("STARTED UPDATE LEADERBOARD LOCAL results: " + results.length);
    
    var alreadyAddedPlayers = [];
    
    leaderboard = [];
    leaderboardDebug = [];
    leaderboardCampaign = [];
    leaderboardLast = [];
    leaderboardDebugLast = [];
    
    if (results === undefined) {
        console.log("Error getting leaderboard from database");
        return;
    }
    if (results[0] != null && results[0] != undefined) {
        var position = 0;
        var campaign_position = 0;
        var debug_position = 0;
        var last_position = 0;
        var last_debug_position = 0;
        
        var months = getCurrentLeaderboardDateStrings();
        var lastMonths = getPreviousLeaderboardDateStrings();

        var results1 = [...results];
        
        results1.sort(function(a, b) {
            if(a === undefined) a = {};
            if(b === undefined) b = {};
            
            var aScore = 0;
            var bScore = 0;
            for (var i = 0; i < months.length; i++) {
                if(a[months[i]] === undefined) a[months[i]] = 0;
                if(b[months[i]] === undefined) b[months[i]] = 0;
                aScore += a[months[i]];
                bScore += b[months[i]];
            }
            
            if(a.lastScore === undefined) a.lastScore = 0;
            if(b.lastScore === undefined) b.lastScore = 0;
            
            // Sort by score
            // If the first item has a higher number, move it up
            // If the first item has a lower number, move it down
            if (aScore < bScore) return 1;
	        if (aScore > bScore) return -1;

            // If the score number is the same between both items, sort by timestamp
            // If the first score was achieved first, move it up
            // Otherwise move it down
            if (a.lastScore < b.lastScore) return 1;
            if (a.lastScore > b.lastScore) return -1;

            // return bScore - aScore;
            return 0;
        });

        results1.forEach(player => {
            
            if (alreadyAddedPlayers.indexOf(player.id) >= 0) {
                return;
            }
            
            var playerLB = new Object();
            playerLB.id = player.id;
			if(player.name === "" || player.name === undefined)
			{
				var genderInt = Math.floor((Math.random() * 2) + 1); // get new random number
				 players.get_random_nickname({gender: genderInt}, function(error, errorcode, playernn)
				 {
					 if(error !== null)
					 {
						 playerLB.name = "";
					 }
					 else
					 {
						 playerLB.name = playernn.name;
					 }
				 });
			}
			else
			{
				playerLB.name = player.name;
			}
            playerLB.district = player.districtName;
            playerLB.region = player.regionName;

            if(playerLB.lastScore === undefined) playerLB.lastScore = 0;

            //score
            var months = getCurrentLeaderboardDateStrings();

            playerLB.score = 0;
            for (var i = 0; i < months.length; i++) {
                if(playerLB[months[i]] === undefined) playerLB[months[i]] = 0;
                playerLB.score += player[months[i]];
            }
            
            if ((player.debug_user !== undefined && player.debug_user === false) || player.debug_user === undefined)
            {
                if (playerLB.score > 0) {
                    position++;
                    playerLB.position = position;
                    leaderboard.push(playerLB);
                }
                
            }
            else
            {
                if (playerLB.score > 0) {
                    debug_position++;
                    playerLB.position = debug_position;
                    leaderboardDebug.push(playerLB);
                }
            }

            //console.log(playerLastLB);

            // console.log(playerLB);
            // console.log("--------------");
            
            alreadyAddedPlayers.push(player.id);
        });
        
        var results2 = [...results];
        
        results2.sort(function(a, b) {
            if(a === undefined) a = {};
            if(b === undefined) b = {};
            
            var aScore = 0;
            var bScore = 0;
            for (var i = 0; i < lastMonths.length; i++) {
                if(a[lastMonths[i]] === undefined) a[lastMonths[i]] = 0;
                if(b[lastMonths[i]] === undefined) b[lastMonths[i]] = 0;
                aScore += a[lastMonths[i]];
                bScore += b[lastMonths[i]];
            }
            
            if(a.lastScore === undefined) a.lastScore = 0;
            if(b.lastScore === undefined) b.lastScore = 0;
            
            // Sort by score
            // If the first item has a higher number, move it up
            // If the first item has a lower number, move it down
            if (aScore < bScore) return 1;
	        if (aScore > bScore) return -1;

            // If the score number is the same between both items, sort by timestamp
            // If the first score was achieved first, move it up
            // Otherwise move it down
            if (a.lastScore < b.lastScore) return 1;
            if (a.lastScore > b.lastScore) return -1;

            // return bScore - aScore;
            return 0;
        });
        
        alreadyAddedPlayers = [];
        
        results2.forEach(player => {
            
            if (alreadyAddedPlayers.indexOf(player.id) >= 0) {
                return;
            }
            
            var playerLB = new Object();
            playerLB.id = player.id;
			if(player.name === "" || player.name === undefined)
			{
				var genderInt = Math.floor((Math.random() * 2) + 1); // get new random number
				 players.get_random_nickname({gender: genderInt}, function(error, errorcode, playernn)
				 {
					 if(error !== null)
					 {
						 playerLB.name = "";
					 }
					 else
					 {
						 playerLB.name = playernn.name;
					 }
				 });
			}
			else
			{
				playerLB.name = player.name;
			}
            playerLB.district = player.districtName;
            playerLB.region = player.regionName;

            if(playerLB.lastScore === undefined) playerLB.lastScore = 0;
            
            var lastMonths = getPreviousLeaderboardDateStrings();
            var playerLastLB = playerLB;
            
            playerLastLB.score = 0;
            for (var i = 0; i < lastMonths.length; i++) {
                if(playerLastLB[lastMonths[i]] === undefined) playerLastLB[lastMonths[i]] = 0;
                playerLastLB.score += player[lastMonths[i]];
            }
            
            if ((player.debug_user !== undefined && player.debug_user === false) || player.debug_user === undefined)
            {
                if (playerLastLB.score > 0) {
                    last_position++;
                    playerLastLB.position = last_position;
                    leaderboardLast.push(playerLastLB);
                }
                
            }
            else
            {
                if (playerLastLB.score > 0) {
                    last_debug_position++;
                    playerLastLB.position = last_debug_position;
                    leaderboardDebugLast.push(playerLastLB);
                }
            }

            //console.log(playerLastLB);

            // console.log(playerLB);
            // console.log("--------------");
            
            alreadyAddedPlayers.push(player.id);
        });
        

        var currentCampaign = campaign;
        if (currentCampaign === null) {
            var today = new Date(datetime.utcnow.getTime());
            var wasNull = (currentCampaign === null);
            var foundCampaign = false;
            campaigns.forEach(c => {
                if(c.redeemTime > today)
                {
                    currentCampaign = c;
                    foundCampaign = true;
                }
            });
        
            if(!foundCampaign)
                currentCampaign = null;
        }
        
        
        
        // campaign leaderboard ---------------------------------------------------------
        if(currentCampaign != null)
        {
            var cID = CAMPAIGN_PREFIX + currentCampaign.campaignId;
    
            results.sort(function(a, b) {
                if(a === undefined) a = {};
                if(a["c"+cID] === undefined) a["c"+cID] = 0;
                if(b === undefined) b = {};
                if(b["c"+cID] === undefined) b["c"+cID] = 0;
                
                var aScore = a["c"+cID];
                var bScore = b["c"+cID];

                if(a.lastScore === undefined) a.lastScore = 0;
                if(b.lastScore === undefined) b.lastScore = 0;
    
                // Sort by score
                // If the first item has a higher number, move it down
                // If the first item has a lower number, move it up
                if (aScore < bScore) return 1;
	            if (aScore > bScore) return -1;

                // If the score number is the same between both items, sort by timestamp
                // If the first score was achieved first, move it up
                // Otherwise move it down
                if (a.lastScore < b.lastScore) return 1;
        if (a.lastScore > b.lastScore) return -1;

                // return bScore - aScore;
                return 0;
            });
            
            results.forEach(player => {
                
                if (player.debug_user) {
                    return;
                }
                
                var playerC = new Object();
                playerC.id = player.id;
                if(player.name === "" || player.name === undefined)
				{
					var genderInt = Math.floor((Math.random() * 2) + 1); // get new random number
					 players.get_random_nickname({gender: genderInt}, function(error, errorcode, playernn)
					 {
						 if(error !== null)
						 {
							 playerC.name = "";
						 }
						 else
						 {
							 playerC.name = playernn.name;
						 }
					 });
				}
				else
				{
					playerC.name = player.name;
				}
                playerC.district = player.districtName;
                playerC.region = player.regionName;
                
                if(player.lastScore === undefined) player.lastScore = 0;
                
                playerC.score = player["c"+cID];
                
                if (playerC.score > 0) {
                    campaign_position++;
                    playerC.position = campaign_position;
                    leaderboardCampaign.push(playerC);
                }
            });
        }
    }
    
    leaderboard = [...leaderboardLast];
    leaderboardDebug = [...leaderboardDebugLast];
    
    //console.log(leaderboardDebug);
    
    console.log("FINISHED: Updated leaderboard.");
    console.log("lb size: " + leaderboard.length);
    console.log("debug lb size: " + leaderboardDebug.length);
    console.log("lb last size: " + leaderboardLast.length);
    console.log("debug lb last size: " + leaderboardDebugLast.length);
    console.log("campaign lb size: " + leaderboardCampaign.length);
}

//30 mins * 60000
var interval = 60 * 60000;
var cr = setInterval(executeUpdate, interval);
executeUpdate();

function executeMarkersUpdate() {
    markers = new Array();

    fs.readFile(__dirname + '/../../../markers.json', 'utf8', (err, jsonString) => {
        if (err) {
            console.log("File read failed:", err);
            return;
        }

        try {
            const file = JSON.parse(jsonString);
            var json = file.markers;

            json.forEach(element => {
                if (markers.some(e => e.id === element.id)) {
                    // console.log("Marker ID:" + element.id + " already present");
                } else
                    markers.push(element);
            });

            markers.forEach(function(item, index, object) {
                if (json.some(e => e.id === item.id)) {
                    // console.log("Validated " + marker.id);
                } else
                    object.splice(index, 1);
            });

            // markers.forEach(marker => {
            //     console.log(marker.id);
            // });

        } catch (err) {
            console.log('Error parsing JSON string:', err)
        }

    });

    // players.update_markers(null, updateMarkers);
}

//24 hours * 60000
// var intervalMarkers = (24 * 60) * 60000;
var intervalMarkers = 30 * 60000;
var cr = setInterval(executeMarkersUpdate, intervalMarkers);

executeMarkersUpdate();

// Nicknames
function generateAllNicknames() {

    var prefixNicknames = new Array();
    var girlFunctionNicknames = new Array();
    var boyFunctionNicknames = new Array();
    var recipeNicknames = new Array();

    fs.readFile(__dirname + '/../../../nicknames.csv', 'utf8', (err, jsonString) => {
        if (err) {
            console.log("File read failed:", err);
            return;
        }

        try {

            var fileContentLines = jsonString.split(/r\n|\n/);
            // Ignore first line
            for (var i = 1; i < fileContentLines.length; i++) {
                var splitLine = fileContentLines[i].split(',');
                if (splitLine.length !== 4) {
                    console.log("WARNING: CSV file not well formatted");
                }

                if (splitLine[0] !== undefined && splitLine[0] !== "") {
                    prefixNicknames.push(splitLine[0]);
                }
                if (splitLine[1] !== undefined && splitLine[1] !== "") {
                    girlFunctionNicknames.push(splitLine[1]);
                }
                if (splitLine[2] !== undefined && splitLine[2] !== "") {
                    boyFunctionNicknames.push(splitLine[2]);
                }
                if (splitLine[3] !== undefined && splitLine[3] !== "") {
                    recipeNicknames.push(splitLine[3]);
                }
            }

            for (var i = 0; i < prefixNicknames.length; i++) {
                var currentPrefix = prefixNicknames[i];
                for (var j = 0; j < girlFunctionNicknames.length; j++) {
                    var currentFunction = girlFunctionNicknames[j];
                    for (var k = 0; k < recipeNicknames.length; k++) {
                        var currentRecipe = recipeNicknames[k];
                        girlNicknames.push(currentPrefix + " " + currentFunction + " " + currentRecipe);
                    }
                }

                for (var j = 0; j < boyFunctionNicknames.length; j++) {
                    var currentFunction = boyFunctionNicknames[j];
                    for (var k = 0; k < recipeNicknames.length; k++) {
                        var currentRecipe = recipeNicknames[k];
                        boyNicknames.push(currentPrefix + " " + currentFunction + " " + currentRecipe);
                    }
                }
            }

            totalBoyNicknamesCount = boyNicknames.length;
            totalGirlNicknamesCount = girlNicknames.length;

            var query = {
                aggregate: [{
                    $project: {
                        "name": "$name"
                    }
                }, ],
                safe: true
            };

            db.bicaserver.players.aggregate(query, function(error, results) {
                for (var i = 0; i < results.length; i++) {
                    var arrayIndex = boyNicknames.indexOf(results[i].name);
                    if (arrayIndex > -1) {
                        boyNicknames.splice(arrayIndex, 1);
                    }

                    arrayIndex = girlNicknames.indexOf(results[i].name);
                    if (arrayIndex > -1) {
                        girlNicknames.splice(arrayIndex, 1);
                    }
                }

                console.log("Girl nicknames count: " + girlNicknames.length);
                console.log("Boy nicknames count: " + boyNicknames.length);

                executeNicknamesUpdate();
            });

        } catch (err) {
            console.log('Error parsing CSV string:', err)
        }

    });
}



//24 hours = 86400 seconds
var intervalNicknames = 86400000;
var maxNoLoginTime = 86400000 * 30;

function executeNicknamesUpdate() {

    sentEmailToday = false;

    var lastMonthUtc = new Date(datetime.utcnow.getTime());
    lastMonthUtc.setMonth(lastMonthUtc.getMonth() - 3); //get the day -3 months which = 90 days

    var timestamp = lastMonthUtc.getTime() / 1000;

    var query = {
        filter: {
            "lastlogin": { "$lte": timestamp },
            "name": { "$ne": "" }
        },
        safe: true
    }
    db.bicaserver.players.get(query, function(error, results) {
        if (error) {
            console.log("Error getting players on executeNicknamesUpdate: " + error);
            return;
        }




        var allPlayerIds = new Array();
        for (var i = 0; i < results.length; i++) {
            allPlayerIds.push(results[i].id);
        }

        var query2 = {
            filter: {
                "id": { "$in": allPlayerIds }
            },
            doc: {
                "$set": { "name": "" }
            },
            multi: true,
            safe: true
        };

        db.bicaserver.players.update(query2, function(error2, results2) {
            if (error2) {
                console.log("Error setting player names on executeNicknamesUpdate: " + error2);
                return;
            }

            console.log("Player nicknames deleted successfully -- " + results.length);
            
            for (var i = 0; i < results.length; i++) {
                if (results[i].gender === 1) {
                    boyNicknames.push(results[i].name);
                } else {
                    girlNicknames.push(results[i].name);
                }
            }

            console.log("Updated local nicknames list");
        });
    });

}

function executeNewFailsafeNicknames() {

    var timestamp = 1663027200; // Edit this as needed

    var query = {
        filter: {
            "lastlogin": { "$gte": timestamp },
            "name": ""
        },
        safe: true
    }
    db.bicaserver.players.get(query, function(error, results) {
        if (error) {
            console.log("Error getting players on executeNewFailsafeNicknames: " + error);
            return;
        }
		
		var count = results.length;
		
		var innerUpdate = function(lastCount)
		{
			if(lastCount === count) return;
			
			var player = results[lastCount];
			players.get_random_nickname(player, function(error_inner, errorcode_inner, nicknameObj)
			{
				if (error_inner === null || error_inner === undefined)
				{
					player.name = nicknameObj.name;
					
					var query2 = {
						filter: {
							"id": player.id,
						},
						doc: {
							"$set": { "name": player.name }
						},
						safe: true
					};
					
					console.log("New name: " + nicknameObj.name);
					
					/*db.bicaserver.players.update(query2, function(error2, results2) {
						if (error2) {
							console.log("Error setting player names on executeNewFailsafeNicknames: " + error2);
							innerUpdate(lastCount + 1);
							return;
						}

						console.log("Player nickname updated successfully " + lastCount + " -- " + player.id + " -- " + player.name);
						
						var boyNamesIndex = boyNicknames.indexOf(player.name);
						if (boyNamesIndex > -1) {
							boyNicknames.splice(boyNamesIndex, 1);
						}

						var girlNamesIndex = girlNicknames.indexOf(player.name);
						if (girlNamesIndex > -1) {
							girlNicknames.splice(girlNamesIndex, 1);
						}

						innerUpdate(lastCount + 1);

					});*/
				}
				else
				{
					innerUpdate(lastCount + 1);
				}
			});
		};
		
		innerUpdate(0);
    });
}


var cn = setInterval(executeNicknamesUpdate, intervalNicknames);

generateAllNicknames();



// DELETE USERS
function deleteEmptyUsers() {
    var query = {
        filter: {
            "id": { "$exists": false }
        },
        safe: true
    };



    db.bicaserver.players.get(query, function(error, results) {
        if (error) {
            console.log("ERROR Getting players to delete: " + error);
            return;
        }

        console.log("Users to delete: " + results.length);

        for (var i = 0; i < results.length; i++) {
            console.log(results[i], "");


        }

        var query2 = {
            filter: {
                "id": { "$exists": false }
            },
            safe: true
        }

        db.bicaserver.players.remove(query2, function(error2, results2) {
            if (error2) {
                console.log("ERROR Deleting players: " + error2);
            }
        });
    });
}
//deleteEmptyUsers();

// Region Markers
function generateRegionMarkers()
{
	// Example
	// var cMarker = new Object();
	// cMarker.regionName = "Águeda";
	// cMarker.regionId = 1;
	// cMarker.latitude = 38.000;
	// cMarker.longitude = 7.000;

	c111 = [];
	
	 fs.readFile(__dirname + '/../../../regionMarkers.csv', 'utf8', (err, jsonString) =>
	 {
        if (err)
		{
            console.log("File read failed:", err);
            return;
        }

        try
		{
			var fileContentLines = jsonString.split(/r\n|\n/);
			// Ignore first line (i = 1)
			for(var i = 1; i < fileContentLines.length; i++)
			{	
				var cMarker = new Object();
				
				if(fileContentLines[i].indexOf('"') > -1)
				{
					var indices = [];
					for(var j = 0; j < fileContentLines[i].length; j++)
					{
						if(fileContentLines[i][j] === '"') indices.push(j);
					}
					
					var substring = fileContentLines[i].substring(indices[0], indices[1]);
					substring = substring.replace(/[,]+/g, ';');
					
					fileContentLines[i] = fileContentLines[i].substring(0, indices[0]) + substring + fileContentLines[i].substring(indices[1]);
					fileContentLines[i] = fileContentLines[i].replace(/['"]+/g, '');
				}
				
				var splitLine = fileContentLines[i].split(',');
				
				if(splitLine[0] !== undefined && splitLine[0] !== "")
					cMarker.regionName = splitLine[0];
				
				if(splitLine[1] !== undefined && splitLine[1] !== "")
                // 	cMarker.regionId = splitLine[1];
                {
            		var coordinates = splitLine[1].split(';');
            		cMarker.latitude = parseFloat(coordinates[0]);
            		cMarker.longitude = parseFloat(coordinates[1]);
            		const clone1 = Object.assign({}, cMarker);
                    if(!isNaN(cMarker.latitude))
                    {
                        c111.push(clone1);
                        // console.log(cMarker.latitude);
                    }
                }
				
				// for(var j = 2; j < splitLine.length; j++)
				// {
				// 	if(splitLine[j] !== undefined && splitLine[j] !== "")
				// 	{
				// 		var coordinates = splitLine[j].split(';');
				// 		cMarker.latitude = parseFloat(coordinates[0]);
				// 		cMarker.longitude = parseFloat(coordinates[1]);
				// 		const clone1 = Object.assign({}, cMarker);
                //         if(!isNaN(cMarker.latitude))
                //         {
                //             // console.log(cMarker.latitude);
                //             c111.push(clone1);
                //         }
				// 	}
				// }
			}
            console.log(c111);
        }
		catch (err)
		{
            console.log('Error parsing CSV string:', err)
        }
    });
}

// ALDI Markers
function generateAldiMarkers()
{
	// Example
	// var cMarker = new Object();
	// cMarker.storeName = "Águeda";
	// cMarker.storeId = 1;
	// cMarker.latitude = 38.000;
	// cMarker.longitude = 7.000;

	aldi = [];
	
    fs.readFile(__dirname + '/../../../aldi.csv', 'utf8', (err, jsonString) =>
    {
        if (err)
		{
            console.log("File read failed:", err);
            return;
        }

        try
		{
			var fileContentLines = jsonString.split(/r\n|\n/);
			// Ignore first line (i = 1)
			for(var i = 1; i < fileContentLines.length; i++)
			{	
				var cMarker = new Object();
				var splitLine = fileContentLines[i].split(',');
				
				if(splitLine[0] !== undefined && splitLine[0] !== "")
					cMarker.storeName = splitLine[0];
				
				if(splitLine[1] !== undefined && splitLine[1] !== "")
					cMarker.storeId = splitLine[1];
				
                if(splitLine[2] !== undefined && splitLine[2] !== "")
				{
                    var coordinates = splitLine[2].split(';');
						cMarker.latitude = parseFloat(coordinates[0]);
						cMarker.longitude = parseFloat(coordinates[1]);
						const clone1 = Object.assign({}, cMarker);
						aldi.push(clone1);
                }
				
			}
        }
		catch (err)
		{
            console.log('Error parsing CSV string:', err)
        }
    });
}

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:::                                                                         :::
//:::  This routine calculates the distance between two points (given the     :::
//:::  latitude/longitude of those points). It is being used to calculate     :::
//:::  the distance between two locations using GeoDataSource (TM) prodducts  :::
//:::                                                                         :::
//:::  Definitions:                                                           :::
//:::    South latitudes are negative, east longitudes are positive           :::
//:::                                                                         :::
//:::  Passed to function:                                                    :::
//:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
//:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
//:::    unit = the unit you desire for results                               :::
//:::           where: 'M' is statute miles (default)                         :::
//:::                  'K' is kilometers                                      :::
//:::                  'N' is nautical miles                                  :::
//:::                                                                         :::
//:::  Worldwide cities and other features databases with latitude longitude  :::
//:::  are available at https://www.geodatasource.com                         :::
//:::                                                                         :::
//:::  For enquiries, please contact sales@geodatasource.com                  :::
//:::                                                                         :::
//:::  Official Web site: https://www.geodatasource.com                       :::
//:::                                                                         :::
//:::               GeoDataSource.com (C) All Rights Reserved 2022            :::
//:::                                                                         :::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

function Distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    } else {
        var radlat1 = Math.PI * lat1 / 180;
        var radlat2 = Math.PI * lat2 / 180;
        var theta = lon1 - lon2;
        var radtheta = Math.PI * theta / 180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit == "K") { dist = dist * 1.609344 }
        if (unit == "N") { dist = dist * 0.8684 }
        return dist;
    }
}

const maxRadius = 0.1; //100 meters | 0.1 Km

function CheckMarkerRegionGPSDistance(id, latitude, longitude)
{
	//console.log("Check Distance ---------------------------------------");
	var success = false;
	//var regionId = 0;
	//var storeId = 0;
	var geoLocation = 0;
    var closestDistance = 10000000000;
	switch (id)
	{
		case 'c111':
			c111.every(element => {
				var distance = Distance(element.latitude, element.longitude, latitude, longitude, 'K');
				if(distance <= maxRadius)
				{
                    console.log('Found c111 in ', element.regionName);
					//regionId = element.regionId;
					// geoLocation = element.latitude + ";" + element.longitude;
                    if(closestDistance > distance)
                    {
                        geoLocation = element.latitude + ", " + element.longitude;
                        closestDistance = distance;
                        success = true;
                    }
					// Make sure you return true. If you don't return a value, `every()` will stop.
  					// return false;
  					return true;
				}
				//else console.log("latitude: " + latitude + "; longitude: " + longitude + " != " + "element.latitude: " + element.latitude + "; element.longitude: " + element.longitude);
				return true;
			});
			// return { success, regionId, geoLocation };
			return { success, geoLocation };
        case 'aldi':
            aldi.every(element => {
                var distance = Distance(element.latitude, element.longitude, latitude, longitude, 'K');
                if(distance <= maxRadius)
				{
                    console.log('Found c111 in ', element.regionName);
					//regionId = element.regionId;
					// geoLocation = element.latitude + ";" + element.longitude;
                    if(closestDistance > distance)
                    {
                        geoLocation = element.latitude + ", " + element.longitude;
                        closestDistance = distance;
                        success = true;
                    }
					// Make sure you return true. If you don't return a value, `every()` will stop.
  					// return false;
				}
				//else console.log("latitude: " + latitude + "; longitude: " + longitude + " != " + "element.latitude: " + element.latitude + "; element.longitude: " + element.longitude);
				return true;
            });
            // return { success, regionId, geoLocation };
            return { success, geoLocation };
			
		default:
			console.log('Not close to any location, ', id);
			// return { success, regionId, geoLocation };
			return { success, geoLocation };
	}
}

generateRegionMarkers();
generateAldiMarkers();

function clearScore()
{
    players.get_all_players(null, updatePlayerScore);
}

function updatePlayerScore(errorMessage, errorCode, results)
{
    if (results === undefined)
	{
        console.log("Error getting players from database");
        return;
    }

    //results = get_all_players

    if (results[0] != null && results[0] != undefined)
	{
        var dateMonthYearString = getDateMonthYearString();
        var prevDateMonthYearString = getPreviousDateMonthYearString();

        if (getDateMonthString() % 2 != 0)
        {

            var allPlayerIds = new Array();
            for (var i = 0; i < results.length; i++)
			{
                allPlayerIds.push(results[i].id);
            }

            var query2 =
			{
                filter: {
                    "id": { "$in": allPlayerIds }
                },
                doc: {
                    "$set": { "score": 0 }
                },
                multi: true,
                safe: true
            };
            //Update on the database
            db.bicaserver.players.update(query2, function(error2, players2)
			{
                if (error2)
				{
                    console.log("error updatating player login date (api.players.get_player:28)", errorcodes.GeneralError);
                }
                else
                {
                    // executeUpdate(); //leaderboard
                    console.log("UPDATED_______________________________");
                }
            });
        }

        
        console.log("UPDATE LEADERBOARD_______________________________");
        executeUpdate(); //leaderboard
        
    }
}

function clearScoreRelease()
{
    players.get_all_players(null, updatePlayerScoreRelease);
}

function updatePlayerScoreRelease(errorMessage, errorCode, results)
{
    if (results === undefined)
	{
        console.log("Error getting players from database");
        return;
    }

    //results = get_all_players

    if (results[0] != null && results[0] != undefined)
	{
        var dateMonthYearString = getDateMonthYearString();
        var prevDateMonthYearString = getPreviousDateMonthYearString();


        var allPlayerIds = new Array();
        for (var i = 0; i < results.length; i++)
        {
            allPlayerIds.push(results[i].id);
        }

        var query2 =
        {
            filter: {
                "id": { "$in": allPlayerIds }
            },
            doc: {
                "$set": { "score": 0 , "scores.2-2022" : 0, "scores.3-2022" : 0 }
            },
            multi: true,
            safe: true
        };

        //Update on the database
        db.bicaserver.players.update(query2, function(error2, players2)
        {
            if (error2)
            {
                console.log("error updatating player login date (api.players.get_player:28)", errorcodes.GeneralError);
            }
            else
            {
                // executeUpdate(); //leaderboard
                console.log("UPDATED_______________________________");
            }
        });

        executeUpdate(); //leaderboard
        
    }
}


// schedule.scheduleJob('* * * * *', function(){
schedule.scheduleJob('@monthly', function(){
    clearScore();
}); // run every month

//Clear for app release
schedule.scheduleJob('0 0 3 3 *', function(){
    var utc = new Date(datetime.utcnow.getTime());
    
    if(utc.getFullYear() == 2022)
    {
        clearScoreRelease();
    }
}); // run “At 00:00 on day-of-month 3 in March.” | 2022-03-03 00:00:00

campaign = null;
function activeCampaign()
{
    var today = new Date(datetime.utcnow.getTime());
    console.log("today " + today);
    var wasNull = (campaign == null);
    var foundCampaign = false;
    campaigns.forEach(c => {
        console.log("campaignName " + c.campaignName);
        console.log("startTime " + c.startTime);
        console.log("endTime " + c.endTime);
        console.log("redeemTime " + c.redeemTime);
        if(c.startTime < today && c.endTime > today)
        {
            campaign = c;
            foundCampaign = true;
        }
    });

    if(!foundCampaign)
        campaign = null;
	// campaign = campaigns.find(element => element.campaignId === ACTIVE_CAMPAIGN);

    console.log("--------------");
    console.log("Active Campaign: "+ (campaign == null ? "NONE": campaign.campaignName));
    console.log("Campaign info:",campaign);
    console.log("--------------");

    if(wasNull && foundCampaign && leaderboardCampaign.length < 1)
    { 
        executeUpdate();
    }
}

activeCampaign();
// campaign = null;

schedule.scheduleJob('*/1 * * * *', function(){
    console.log("checking campaign");
    activeCampaign();
}); // every minute.


function playersWithPoints(options)
{
    players.get_all_players_statistics(options, countPlayersWithPoints);
}

function countPlayersWithPoints(errorMessage, errorCode, results)
{
	if (results === undefined) {
		console.log("Error getting leaderboard from database");
        return;
    }
	
    var count = 0;
    var countTotal = 0;
    var count3 = 0;
    var count4 = 0;
    var count5 = 0;
    var count6 = 0;
	//Total
    results.forEach(player => {
        countTotal++;
        if(player.scores !== undefined && player.scores !== null)
        {
            var hasScore = false;
            var scores = player.scores;
            
            if(scores["3-2022"] > 0)
            {
                count3++;
                hasScore = true;
            }
            if(scores["4-2022"] > 0)
            {
                count4++;
                hasScore = true;
            }
            if(scores["5-2022"] > 0)
            {
                count5++;
                hasScore = true;
            }
            if(scores["6-2022"] > 0)
            {
                count6++;
                hasScore = true;
            }
            // scores.forEach(element => {
            //     if(element > 0)
            //     {
            //         hasScore = true;
            //         return true;
            //     }
            // });
            if(hasScore)
                count++;
        }
    });

    console.log("Total: " + countTotal);
    console.log("With Score: " + count);
    console.log("Month 3: " + count3);
    console.log("Month 4: " + count4);
    console.log("Month 5: " + count5);
    console.log("Month 6: " + count6);
}
// playersWithPoints()

/*
function campaign_players(options)
{
    players.get_all_players_statistics(options, countCampaignPlayers);
}

function countCampaignPlayers(errorMessage, errorCode, results)
{
	if (results === undefined) {
		console.log("Error getting leaderboard from database");
        return;
    }
	
    var count = 0;
	//Total
    results.forEach(player => {
        if(player.scores !== undefined && player.scores !== null)
        {
            var hasScore = false;
            var scores = player.scores;
            
            if(scores["c2001"] != null && scores["c2001"] >= 0)
            {
                count++;
            }
        }
    });

    console.log("Count: " + count);
}
campaign_players()
*/

function deleteCampaignScores()
{
    var query2 = {
        filter: {
            'scores.cNaN' : {$exists: true}//, id : {$nin : [183,369]}
        },
        doc: {	
            $unset: {
                'scores.cNaN': 0,
            }
        },
        safe : true,
        multi: true
    };
    
    db.bicaserver.players.update(query2, function(error) {
        
        if(error)
        {
            callback("error getting scores (api.playersessions.register_session_score:147)", errorcodes.GeneralError);
            return;
        }

        console.log("finished");
    });
}
// deleteCampaignScores()

// for testing slow server responses
/* Usage
    Make function async

    call:
    console.log(1);
    await sleep(5000); // time in ms
    console.log(2);
*/
function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

// Debug Elements, do not activate these unless necessary!

function generateLeaderboardCollection() {
    var query = {
        safe: true,
        filter: {"id": {"$exists":true}},
        sort: { score: 1, lastlogin: 1 }
    };

    console.log("BEFORE QUERY GET LEADERBOARD FOR GENERATE");
    
    db.bicaserver.players.get(query, function(error, results) {
        if (error) {
            console.log(error);
            //callback("error aggregating players from database", errorcodes.GeneralError);
            return;
        }

        console.log("COMPLETED QUERY GET LEADERBOARD FOR GENERTATE");
        
        //callback(null, errorcodes.NoError, results);
        
        var currentIndex = 0;
        
        function addLeaderboardEntry() {
            if (currentIndex >= results.length) {
                console.log("All done adding players to leaderboard!");
                return;
            }
            
            var dbPlayer = results[currentIndex];
            var lbPlayer = {};
            lbPlayer.id = dbPlayer.id;
            lbPlayer.name = dbPlayer.name;
            lbPlayer.regionName = dbPlayer.regionName;
            lbPlayer.districtName = dbPlayer.districtName;
            
            for (var i in dbPlayer.scores) {
                lbPlayer[i] = dbPlayer.scores[i];
            }
            
            var doc = {
                "$set": {
                    id: dbPlayer.id,
                    name: dbPlayer.name,
                    regionName: dbPlayer.regionName,
                    districtName: dbPlayer.districtName
                }
            };
            
            var updateQuery = {
                filter: {
                    id: lbPlayer.id
                },
                doc: lbPlayer,
                upsert: true,
                safe: true
            };
            
            db.bicaserver.leaderboard.update(updateQuery, function(error2, resultUpd)
            {
                if (error2) {
                    console.log(error2);
                    return;
                }
                
                console.log("Finished adding: ");
                console.log(lbPlayer, "");
                
                currentIndex++;
                addLeaderboardEntry();
            });
        }
        
        addLeaderboardEntry();
    });
}
//generateLeaderboardCollection();

function addIsDebugToLeaderboard() {
    var query = {
        safe: true,
        filter: {"id": {"$exists":true}},
        sort: { score: 1, lastlogin: 1 }
    };
    
    db.bicaserver.players.get(query, function(error, results) {
        if (error) {
            console.log(error);
            //callback("error aggregating players from database", errorcodes.GeneralError);
            return;
        }
        
        //callback(null, errorcodes.NoError, results);
        
        var currentIndex = 0;
        
        function setNextLeaderboardEntryDebug() {
            if (currentIndex >= results.length) {
                console.log("All done setting debug players to leaderboard!");
                return;
            }
            
            var dbPlayer = results[currentIndex];
            if (dbPlayer.debug_user === undefined || dbPlayer.debug_user === false) {
                currentIndex++;
                setNextLeaderboardEntryDebug();
                return;
            }
            
            var updateQuery = {
                filter: {
                    id: dbPlayer.id
                },
                doc: {
                    "$set": { "debug_user": dbPlayer.debug_user }
                },
                safe: true
            };
            
            db.bicaserver.leaderboard.update(updateQuery, function(error2, resultUpd)
            {
                if (error2) {
                    console.log(error2);
                    return;
                }
                
                console.log("Finished setting debug_user on " + dbPlayer.name);
                
                currentIndex++;
                setNextLeaderboardEntryDebug();
            });
        }
        
        setNextLeaderboardEntryDebug();
    });
}
//addIsDebugToLeaderboard();
