var errors = module.exports = {
    
    errorcodes: {

        NoError: 0,
        GeneralError: 1,
        NoUsername: 2,
		MarkerAlreadyScanned: 3,
		MarkerScannedWrongDate: 4,
		MarkerScannedWrongTime: 5,
		MarkerScannedWrongLocation: 6,
		MarkerScannedRepeatRegion: 7,
		MarkerScannedUnique: 8,
		MarkerScannedMonthly: 9,
		WrongAppVersion: 10,
		NoActiveCampaign: 11,
		MarkerScannedWeekly: 12,

	},

    descriptions: {
        // General Errors
        "0": "No error",
        "1": "General error, this typically means the player is unable to connect",
        "2": "Username not defined",
        "3": "Marker was already scanned today",
        "4": "Marker isn't active today",
        "5": "Marker isn't active today at this time",
        "6": "User isn't near a valid location",
        "7": "User has already scanned this marker in this location today",
        "8": "User has already scanned this marker that can only be scanned once",
        "9": "Marker was already scanned this month",
        "10": "Wrong App version, update app",
        "11": "No active campaign",
        "12": "Marker was already scanned this week",
    }
};